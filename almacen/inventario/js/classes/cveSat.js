export class CveSat {
    

    getClaves() {
        
        return fetch('php/select-cat-prodserv.php')
        .then(data => data.json())
        .then(data => {
            return data; 
        })

    }

    async setClaves() {

        const DATA = await this.getClaves();
        let options = `<option value="" selected disabled>Selecciona una clave</option>`
        DATA.forEach(unidad => {
            options += `<option value="${unidad.ClaveSAT}">${unidad.Descrip}</option>`;
        });
        document.querySelector('[name=ClaveSAT]').innerHTML = options;

    }

  
}