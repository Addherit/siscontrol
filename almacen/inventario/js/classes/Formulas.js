export class Formula {
    

    getFormula() {
        
        return fetch('php/select-formulas.php')
        .then(data => data.json())
        .then(data => {
            return data; 
        })

    }

    async setFormulas() {

        const DATA = await this.getFormula();
        let options = `<option value="0" selected>Buscar Nuevos</option>`;
        DATA.forEach(formula => {
            options += `<option value="${formula.Formula}">${formula.Formula}</option>`;
        });
        document.querySelector('[name=Formula]').innerHTML = options;

    }

    async ChangeDesc(formu) {
        console.log("entro");
        let descformula = '';
        console.log(formu);
	//description
      switch (formu) {

        case 'azul':
            descformula = 'DIAMETRO X DIAMETRO X 4 X LARGO';
        break;
        case 'naranja':
            descformula = 'ESPESOR  X ANCHO X  5  X (LARGO)';

        break;
        case 'pistache':
            descformula = 'DIAMETRO X DIAMETRO X 5  X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 5 (VARIANTE FIJA) X LARGO (EN METROS)

        break;
        case 'negro':
            descformula = 'DIAMETRO X DIAMETRO X 1.4 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 1.4 (VARIANTE FIJA) X LARGO (EN METROS)	

        break;
        case 'blanco':
            descformula = 'PESO X METRO X LARGO X CANTIDAD';
          
        break;   
        case 'amarillo':
            descformula = 'ESPESOR X ANCHO X 1.8 X (LARGO)';
      // ESPESOR(PULGADAS) X ANCHO(PULGADAS) X 1.8 (VARIANTE FIJA) X (LARGO EN METROS)					
          
        break;   
        case 'morado':
            descformula = ' DIAMETRO  X DIAMETRO X  1.8 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  1.8 (VARIANTE FIJA) X (LARGO EN METROS)
        break;   
        case 'rey':
            descformula = 'DIAMETRO X DIAMETRO X 4.5 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 4.5 (VARIANTE FIJA) X LARGO (EN METROS)	
          
        break;   
        case 'gris':
            descformula = ' DIAMETRO  X DIAMETRO X  5.5 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  5.5 (VARIANTE FIJA) X (LARGO EN METROS)	
          
        break; 
        case 'verde':
            descformula = 'DIAMETRO EXTERIOR AL CUADRADO -  DIAMETRO INTERIOR AL CUADRADO X 5 X LARGO';
      // DIAMETRO EXTERIOR (PULGADAS) AL CUADRADO -  DIAMETRO INTERIOR (PULGADAS) AL CUADRADO X 5 (VARIANTE FIJA) X LARGO (EN METROS)
          
        break;   
        case 'placany':
            descformula = 'PLACA';

        break;
    }
        document.querySelector('[name=fdesc]').value = descformula;

    }
  
}