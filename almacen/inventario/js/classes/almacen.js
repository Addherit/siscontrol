import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();


export class Almacen {

    constructor () {
        this.idProduct = ``;
    }

    getAlmacen() {

        
        return fetch('php/select-inventario.php')
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data;
        })

    }

    async setAlmacen() {

        SPINNER.showSpinner();
        const DATA = await this.getAlmacen();
        const ID_type = localStorage.getItem('id_user_type');
        let lineas = ``;
        let BTN_ACTIVATE_DISABLED = "";

        DATA.forEach(producto => {

            const STATUS = this.getColorStatus(producto.Estatus);
            if(ID_type =="1" || ID_type == "4"){
                BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(producto.Estatus);
            }else if(ID_type =="2" ){
                BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled("Desactivado");
                document.querySelector("#btn-edit-product").disabled = true;
            }
            lineas += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-warning btn-edit" title="Editar Producto" data-toggle="modal" data-target="#modal-edit-product">
                            <span class="material-icons align-middle">edit</span>
                            Editar
                        </button>
                    </td>
                    <td>${producto.ID_Producto}</td>
                    <td>${producto.Clave_Prod}</td>
                    <td>${producto.Descripcion}</td>
                    <td>${producto.Unidad}</td>
                    <td>${producto.Stock}</td>
                    <td>${producto.Matriz}</td>
                    <td>${producto.Polanco}</td>
                    <td>${producto.Bodega}</td>
                    ${STATUS}
                </tr>
            `
        });

        document.querySelector('#list-inventario tbody').innerHTML = lineas;
        const dataTable = new DataTable('list-inventario');
        this.addEventClickToBtnDeleteActivate();
        this.addEventClickToBtnEditProduct();
        this.addEventClickToBtnDelProduct();
        dataTable.initDataTable();

    }

    addEventClickToBtnDeleteActivate() {

        document.querySelectorAll('.btn-delete-activate').forEach(btn => {
            btn.addEventListener('click', (event) => {
       
                const NAME_PRODUCT = event.currentTarget.parentElement.parentElement.children[3].textContent;
                document.querySelector('#disabled-name').innerHTML = `el producto`;
                document.querySelector('#id-to-disabled').innerHTML = NAME_PRODUCT;
                document.querySelector('#confirm-name').innerHTML = `el producto`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                this.idProduct = event.currentTarget.parentElement.parentElement.children[1].textContent;
                
            })
        })

    }

    addEventClickToBtnEditProduct() {

        document.querySelectorAll('.btn-edit').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoProductById(ID_PRODUCT);
                
            })
        })        

    }

    addEventClickToBtnDelProduct() {

        document.querySelectorAll('.btn-delete').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.DeleteProduct(ID_PRODUCT);
                
            })
        })        

    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Inactivo: `<td>${status}</td>`,
            Moroso: `<td class="text-danger">${status}</td>`
        }

        return COLOR_STATUS[status];

    }

    getBtnActivateDisabled(status) {

        const ID_type = localStorage.getItem('id_user_type');
        let Eliminar =  `
            <button type="button" class="btn btn-sm btn-danger btn-delete" title="Eliminar Prodcuto"  style="width: 121px">
                <span class="material-icons align-middle">cancel</span>
                Eliminar
            </button>  `;
        if(ID_type != 1){
            Eliminar = ` `;
        }

        if (status == 'Inactivo') {
            return `
            ${Eliminar}
            <button type="button" class="btn btn-sm btn-success btn-delete-activate" title="Activar Prodcuto" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else if(status =='Activo'){
            return `
            <button type="button" class="btn btn-sm btn-danger btn-delete-activate" title="Desactivar producto" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Desactivar
            </button>`
        }else{
            return `
            <button type="button" disabled class="btn btn-sm btn-danger btn-delete-activate" title="Desactivar producto" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Desactivar
            </button>`

        }
       
    }

    disabledProduct() {
        
        const ID_PRODUCT = this.idProduct        
        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', ID_PRODUCT);
        fetch('php/desactivar-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-disabled-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    activateProduct() {

        const ID_PRODUCT = this.idProduct
        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', ID_PRODUCT);
        fetch('php/activar-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }


    updateProduct() {

        const FORM_DATA = new FormData(document.querySelector("#form-edit-product"))        
        fetch('php/update-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            $('#modal-edit-product').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
            
        })

    }

    DeleteProduct(Id) {

        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', Id);
        fetch('php/delete-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    getInfoProductById(id_product) {

        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', id_product)        
        return fetch('php/select-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
           return data;  
        })

    }

    async setInfoProductById(id_product) {

        const PRODUCTO = await this.getInfoProductById(id_product);
        document.querySelector('[name=id]').value = PRODUCTO[0].ID_Producto
        document.querySelector('[name=codigo]').value = PRODUCTO[0].Clave_Prod
        document.querySelector('[name=descripcion]').value = PRODUCTO[0].Descripcion
        document.querySelector('[name=unidad]').value = PRODUCTO[0].Unidad
        document.querySelector('[name=stock]').value = PRODUCTO[0].Stock
        document.querySelector('[name=ClaveSAT]').value = PRODUCTO[0].Catprodserv
        document.querySelector('[name=status]').value = PRODUCTO[0].Estatus            
        document.querySelector('[name=unidadp]').value = PRODUCTO[0].MedidaPulg
        document.querySelector('[name=medida]').value = PRODUCTO[0].Medida
//        document.querySelector('[name=Formula]').value = PRODUCTO[0].Formula            
        //selects
        let lineas = `<option value="0" selected>Buscar Nuevos</option>`;
        lineas += `<option value="${PRODUCTO[0].Formula}" selected>${PRODUCTO[0].Formula}</option>`;
        document.querySelector('[name=Formula]').innerHTML = lineas
        this.ChangeDesc(PRODUCTO[0].Formula);
        //SAT
        let lineas2 = `<option value="0" selected>Buscar Nuevos</option>`;
        lineas2 += `<option value="${PRODUCTO[0].CatProdServ}" selected>${PRODUCTO[0].CatProdServ}</option>`;
        document.querySelector('[name=ClaveSAT]').innerHTML = lineas2

        const descstock = document.getElementById("descstock");
        descstock.innerHTML = " ";
    }

    async ChangeDesc(formu) {
        console.log("entro");
        let descformula = '';
        console.log(formu);
	//description
      switch (formu) {

        case 'azul':
            descformula = 'DIAMETRO X DIAMETRO X 4 X LARGO';
        break;
        case 'naranja':
            descformula = 'ESPESOR  X ANCHO X  5  X (LARGO)';

        break;
        case 'pistache':
            descformula = 'DIAMETRO X DIAMETRO X 5  X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 5 (VARIANTE FIJA) X LARGO (EN METROS)

        break;
        case 'negro':
            descformula = 'DIAMETRO X DIAMETRO X 1.4 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 1.4 (VARIANTE FIJA) X LARGO (EN METROS)	

        break;
        case 'blanco':
            descformula = 'PESO X METRO X LARGO X CANTIDAD';
          
        break;   
        case 'amarillo':
            descformula = 'ESPESOR X ANCHO X 1.8 X (LARGO)';
      // ESPESOR(PULGADAS) X ANCHO(PULGADAS) X 1.8 (VARIANTE FIJA) X (LARGO EN METROS)					
          
        break;   
        case 'morado':
            descformula = ' DIAMETRO  X DIAMETRO X  1.8 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  1.8 (VARIANTE FIJA) X (LARGO EN METROS)
        break;   
        case 'rey':
            descformula = 'DIAMETRO X DIAMETRO X 4.5 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 4.5 (VARIANTE FIJA) X LARGO (EN METROS)	
          
        break;   
        case 'gris':
            descformula = ' DIAMETRO  X DIAMETRO X  5.5 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  5.5 (VARIANTE FIJA) X (LARGO EN METROS)	
          
        break; 
        case 'verde':
            descformula = 'DIAMETRO EXTERIOR AL CUADRADO -  DIAMETRO INTERIOR AL CUADRADO X 5 X LARGO';
      // DIAMETRO EXTERIOR (PULGADAS) AL CUADRADO -  DIAMETRO INTERIOR (PULGADAS) AL CUADRADO X 5 (VARIANTE FIJA) X LARGO (EN METROS)
          
        break;   
        case 'placany':
            descformula = 'PLACA';

        break;
    }
        document.querySelector('[name=fdesc]').value = descformula;
    }

    async getStockByAlmacen() {

        const FORM_DATA = new FormData()
        const SELECT_ALMACEN = document.querySelector('[name=almacen-to-edit]');
        const INPUT_STOCK = document.querySelector('[name=stock]');
        const ID_PRODUCT = document.querySelector('[name=id]');
        FORM_DATA.append('id-almacen',  SELECT_ALMACEN.value);
        FORM_DATA.append('id-producto',  ID_PRODUCT.value);
        const DATA = await fetch('php/select-stock.php', { method: 'POST', body: FORM_DATA });
        const STOCK = await DATA.json()
        STOCK.length > 0 ? INPUT_STOCK.value = STOCK[0].Stock : INPUT_STOCK.value = 0;
        //set descripcion
        const descstock = document.getElementById("descstock");
        let text = STOCK[0].Stock;
        let lineas = `<option value="0" selected>Ningun Stock</option>`;
        descstock.innerHTML = " ";
        if(text.length > 0){
            const array01 = text.split("-");
            const array02 = array01[1].split(",");
            text = array01[0] +"<br>";
            //lineas
            array02.forEach( item =>{
                let item01 = item.split("+");
                let item02id = item01[1];
                text += item01[0] +"<br>"
                lineas += `<option value="${item02id}">${item01[0]}</option>`;
            });
        }
        descstock.innerHTML = text;
        document.querySelector('[name=stocks-almacen]').innerHTML = lineas

    }

    getAllAlmacenes() {

        fetch('php/select-almacenes.php')
        .then(data => data.json())
        .then(data => {                       
            let lineas = `<option value="0" selected>Todos los almacenes</option>`;
            data.forEach(almacen => {
                lineas += `<option value="${almacen.Id}">${almacen.Nombre}</option>`;
            })
            document.querySelector('[name=almacen-to-edit]').innerHTML = lineas
            document.querySelector('[name=nuevo-almacen]').innerHTML = lineas
            
        })

    }

    async applyTraslado() {

        const NUEVO_ALMACEN = document.querySelector('[name=nuevo-almacen]');
        const OLD_ALMACEN = document.querySelector('[name=almacen-to-edit]');
        const ID_user = localStorage.getItem('user_id');

        if(NUEVO_ALMACEN.value != OLD_ALMACEN.value) {

            const FORM_DATA = new FormData();
            FORM_DATA.append('id-producto', document.querySelector('[name=id]').value)
            FORM_DATA.append('nuevo-almacen', NUEVO_ALMACEN.value)
            FORM_DATA.append('old-almacen', OLD_ALMACEN.value)
            FORM_DATA.append('traslado', document.querySelector('[name=traslado]').value)
            FORM_DATA.append('id-stocks', document.querySelector('[name=stocks-almacen]').value)
            FORM_DATA.append('piezas', document.querySelector('[name=Piezas]').value)
            const DATA = await fetch('php/translado-almacenes.php', {method: 'POST', body: FORM_DATA });
            const TRASLADO = await DATA.json();
            const MSN = new Message(TRASLADO.type, TRASLADO.message);
            MSN.showMessage();
            //agregar el pdf

            window.open(`/siscontrol/almacen/inventario/php/get_translado_pdf.php?Ido=${OLD_ALMACEN.value}&Idn=${NUEVO_ALMACEN.value}&user=${ID_user}&idp=${document.querySelector('[name=id]').value}&pieza=${document.querySelector('[name=Piezas]').value}&stock=${document.querySelector('[name=traslado]').value}`);
        } else {

            const MSN = new Message('warning', 'No se puede aplicar el traslado al mismo almacén');
            MSN.showMessage();

        }


    }

    isMinThanStock() {

        const STOCK = parseInt(document.querySelector('[name=stock]').value);
        const TRASLADO = parseInt(document.querySelector('[name=traslado]').value);
        const BTN_TRASLADO = document.querySelector('#btn-traslado');
        console.log(STOCK, TRASLADO);
        if(STOCK > TRASLADO) {
            BTN_TRASLADO.disabled = false;
        } else {
            const MSN = new Message('warning', 'La cantidad de traslado es mayor al stock de este amlmacén');
            MSN.showMessage();
            BTN_TRASLADO.disabled = true;
        }

    }

   
}