import { Almacen } from "./classes/almacen.js";
import { Unidad } from "./classes/unidad.js";
import { CveSat } from "./classes/cveSat.js";
import { Formula } from "./classes/Formulas.js";


const UNIDAD = new Unidad();
const ALMACEN = new Almacen();
const CLAVE = new CveSat();
const FORMU = new Formula();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-disabled').addEventListener('click', () => { ALMACEN.disabledProduct() });
document.querySelector('#btn-activate').addEventListener('click', () => { ALMACEN.activateProduct() });
document.querySelector('#btn-edit-product').addEventListener('click', () => { document.querySelector('#completing-msg').innerHTML = `editar la información del producto` });
document.querySelector('#btn-confirm').addEventListener('click', () => confirm());
document.querySelector('[name=almacen-to-edit]').addEventListener('change', () => ALMACEN.getStockByAlmacen());
document.querySelector('#btn-traslado').addEventListener('click', () => ALMACEN.applyTraslado());
document.querySelector('[name=traslado]').addEventListener('keyup', () => ALMACEN.isMinThanStock());

const confirm = () => {

    ALMACEN.updateProduct();
    $('#modal-confirmation').modal('hide');

}

const init = () => {
    
    ALMACEN.setAlmacen();
    UNIDAD.setUnidades();
    ALMACEN.getAllAlmacenes();
    CLAVE.setClaves();
}

//revisar
const select = document.querySelector('[name=Formula]');
select.addEventListener('change', function handleChange(event) {

  if(event.target.value == 0){
    FORMU.setFormulas();
  }else{
    console.log(event.target.value); // 👉️ get selected VALUE
    FORMU.ChangeDesc(event.target.value);
  }
});
//clave prod
const sat = document.querySelector('[name=ClaveSAT]');
sat.addEventListener('change', function handleChange(event) {
  if(event.target.value == 0){
    //FORMU.setFormulas();
    console.log(event.target.value);
  }else{
    console.log(event.target.value); // 👉️ get selected VALUE
    //FORMU.ChangeDesc(event.target.value);
  }
});

