<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {

    //$sql = "SELECT p.`ID_Producto`,p.`Clave_Prod`,p.`Descripcion`,p.`Unidad`,p.Estatus,  IFNULL(TRUNCATE((Select Sum(s.Stock) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo'),2),0) as Stock FROM `productos` p where p.`Estatus` <> 'Eliminado' ORDER BY ID_Producto ASC";
    //$sql = "SELECT p.`ID_Producto`,p.`Clave_Prod`,p.`Descripcion`,p.`Unidad`,p.Estatus, IFNULL(TRUNCATE((Select sum(s.`Stock`- IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo'),2),0) as Stock, IFNULL(TRUNCATE((Select sum(s.`Stock`- IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and e.Almacen = 1),2),0) as Matriz, IFNULL(TRUNCATE((Select sum(s.`Stock`- IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and e.Almacen = 2),2),0) as Polanco,IFNULL(TRUNCATE((Select sum(s.`Stock`- IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and e.Almacen = 3),2),0) as Bodega FROM `productos` p where p.`Estatus` <> 'Eliminado' ORDER BY ID_Producto ASC";
    $sql = "SELECT p.`ID_Producto`,p.`Clave_Prod`,p.`Descripcion`,p.`Unidad`,p.Estatus, IFNULL(TRUNCATE((Select sum(s.`Stock`- (IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0)  + IFNULL(s.Traslado,0)) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo'),2),0) as Stock, IFNULL(TRUNCATE((Select sum(s.`Stock`- (IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0)  + IFNULL(s.Traslado,0)) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and s.`Id_almacen`= 1),2),0) as Matriz,IFNULL(TRUNCATE((Select sum(s.`Stock`- (IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0)  + IFNULL(s.Traslado,0)) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and s.`Id_almacen` = 2),2),0) as Polanco,IFNULL(TRUNCATE((Select sum(s.`Stock`- (IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id` and sl.Estatus ='Usado'),0)  + IFNULL(s.Traslado,0)) ) from `Stocks` s left join Entradas_Salidas e on s.`Entrada` = e.Id where s.Id_product = p.`ID_Producto` and e.Estatus = 'Activo' and s.`Id_almacen` = 3),2),0) as Bodega FROM `productos` p where p.`Estatus` <> 'Eliminado' ORDER BY ID_Producto ASC";
    $result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );

} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($result);
    //comentario de revision
//}
?>