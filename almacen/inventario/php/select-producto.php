<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {
    $clave= $_POST['ID_Producto'];
    //$sql = "SELECT p.`ID_Producto`,p.`Clave_Prod`,p.`Descripcion`,p.`Unidad`,p.`Estatus`,p.`CatProdServ`, IFNULL((Select Sum(s.Stock) from `Stocks` s where s.Id_product = p.`ID_Producto`),0) as Stock  FROM `productos` p where p.ID_Producto =$clave ";
    $sql = "SELECT  p.`MedidaPulg`,p.`Medida`,c.Formula,p.`ID_Producto`,p.`Clave_Prod`,p.`Descripcion`,p.`Unidad`,p.`Estatus`,p.`CatProdServ`, (SELECT IFNULL(TRUNCATE(sum((s.Stock - IFNULL( (select sum(sp.Cantidad) from StockPedido sp where sp.Id_stocks = s.Id and sp.Estatus = 'Usado') ,0)) ),2),0)as sr from Stocks s left join Entradas_Salidas e on s.Entrada = e.Id where e.Estatus = 'Activo' and s.Id_product =p.`ID_Producto`) as Stock FROM `productos` p inner join categoria c on c.Categoria = p.`Categoria` where p.ID_Producto =$clave";
    $result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
    //$result =["type"=>'success',"message"=>'Se Activo el producto correctamente'];

} catch (PDOException  $e) {
    $result = ["type"=>'danger',"message" => "Error al Activar el producto"];
}

echo json_encode($result);
    //comentario de revision
//}
?>