<?php
include_once('../../../assets/db/conexion.php');
require('../../../assets/CFDI33_SIFEI/fpdf/fpdf.php');

$id_p = $_GET["idp"];
$id_o = $_GET["Ido"];
$id_n = $_GET["Idn"];
$id_user = $_GET["user"];
$pieza = $_GET["pieza"];
$traslado = $_GET["stock"];
//get header info
$sql = "SELECT * FROM `productos` where ID_Producto = $id_p";
$result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
$clave = $result[0]['Clave_Prod'];
$des = $result[0]['Descripcion'];
$uni = $result[0]['Unidad'];
//user
$sql = "SELECT * FROM `Users` where `userId` = $id_user";
$result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
$name = $result[0]['name'];
//busqueda de los nombres de almacen
$sql = "SELECT * FROM `Almacen` where Id = $id_n";
$result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
$alman = $result[0]['Nombre'];
//almacen viejo
$sql = "SELECT * FROM `Almacen` where Id = $id_o";
$result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
$almao = $result[0]['Nombre'];
//datos del header
// Column headings
$headerH = array('Viejo Almacen', 'Nuevo Almacen', 'Agente', 'Producto');
// Data loading
$dataH = array (
  array($almao,$alman,$name,$clave)
);

//datos del la transferencia
// Column headings
$header = array('Id', 'Descripcion', 'UD', 'Piezas','Stock');
// Data loading
$data = array (
  array("0",$des,$uni,$pieza,$traslado)
);



class PDF extends FPDF {

	public $estatus;
	public function __construct($estado){
		parent::__construct();
		$this->estatus = $estado;
	}

	// Page header
	function Header() {
		
		// Add logo to page
		$this->Image('../../../assets/img/logo.png',10,8,33);
		
		// Set font family to Arial bold
		$this->SetFont('Arial','B',20);
		
		// Move to the right
		$this->Cell(80);
		
		// Header
		$this->Cell(50,10,"$this->estatus",1,0,'C');
		
		// Line break
		$this->Ln(40);
	}

	// Page footer
	function Footer() {
		
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		
		// Page number
		$this->Cell(0,10,'Page ' .
			$this->PageNo() . '/{nb}',0,0,'C');
	}

	// Cotizacion table
	function CotizacionTable($header, $data){
		// Column widths
		$w = array(10,110, 10, 20,20);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Cell($w[4],6,$row[4],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	// Abierto Coinfirmado table
	function NoCotizacionTable($header, $data){
		// Column widths
		$w = array(30,120, 10, 30);
		// Header
		for($i=0;$i<count($header)-2;$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}
	// Comentarios table
	function ObvTable($header, $data){
		// Column widths
		$w = array(20,170);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Cell($w[4],6,$row[4],'LR');
			$this->Cell($w[5],6,$row[5],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	function HeaderTable($header, $data){
		// Colors, line width and bold font
		$this->SetFillColor(38,198,218);
		$this->SetTextColor(0);
		$this->SetDrawColor(57,73,171);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		// Header
		$w = array(40,40,90,20);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = false;
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
			$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[2],6,$row[2],'LR',0,'R',$fill);
			$this->Cell($w[3],6,$row[3],'LR',0,'R',$fill);
			$this->Ln();
			$fill = !$fill;
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	// Sat table
	function SatTable($header, $data){
		// Column widths
		$w = array(60, 60, 60);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}
}

// Instantiation of FPDF class
$pdf = new PDF("Translado");

// Define alias for number of pages
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',14);



//construccion del pdf
$pdf->HeaderTable($headerH,$dataH);
$pdf->Ln(15);
$pdf->CotizacionTable($header,$data);
$pdf->Ln(15);
$pdf->line(50, 130, 120, 130);

/*
if($estados == "COTIZACION"){
	$pdf->Ln(15);
	$pdf->CotizacionTable($header,$data);
	$pdf->Ln(15);
	$pdf->ObvTable($headero,$datao);
	$pdf->Ln(15);
	$pdf->SatTable($headerS,$dataS);
}else{
	$pdf->Ln(15);
	$pdf->NoCotizacionTable($header,$data);
	$pdf->Ln(15);
	$pdf->ObvTable($headero,$datao);
}
*/
//salida	
$pdf->Output();

?>
