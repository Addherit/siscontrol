<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "./modales/editar-producto.html" ?>
        <?php include "../../assets/modales/modal-disabled-confirmation.html" ?>
        <?php include "../../assets/modales/modal-activate-confirmation.html" ?>
        <?php include "../../assets/modales/modal-confirmation.html" ?>
        <?php include "../../assets/modales/modal-spinner.html" ?>
        
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                                <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">INVENTARIO</span>
                        </div>                        
                    </div>
                    <hr>
                       
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table id="list-inventario" class="table table-hover table-dataTable table-sm" width="100%">
                                <thead>
                                <tr>
                                    <th class="th-sm"></th>
                                    <th class="th-sm">Id</th>
                                    <th class="th-sm">Codigo</th>
                                    <th class="th-sm">Descripción</th>
                                    <th class="th-sm">Unidad</th>
                                    <th class="th-sm">Stock</th>
                                    <th class="th-sm">Matriz</th>
                                    <th class="th-sm">Polanco</th>
                                    <th class="th-sm">Bodega</th>
                                    <th class="th-sm">Estatus</th>
                                </tr>
                                </thead>                            
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>