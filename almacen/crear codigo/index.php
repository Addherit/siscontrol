<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "../../assets/modales/modal-delete-confirmation.html" ?>
        <?php include "../../assets/modales/modal-confirmation.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">CREAR CODIGO</span>
                        </div>                        
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-7">                           
                            <form id="form-add-codigo">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="clave-producto">Clave de producto</label>
                                            <input type="text" class="form-control form-control-sm" name="clave-producto">

                                        </div>
                                        <div class="form-group">
                                            <label for="descripcion">Descripción</label>
                                            <input type="text" class="form-control form-control-sm" name="descripcion">

                                        </div>
                                        <div class="form-group">
                                            <label for="unidad">Unidad</label>
                                            <select name="unidad" class="form-control form-control-sm" id="slt-unidades"></select>

                                        </div>
                                        <div class="form-group">
                                            <label for="unidad-pulgadas">Unidad en pulgadas</label>
                                            <input type="text" class="form-control form-control-sm" name="unidad-pulgadas" placeholder="0.00">

                                        </div>
                                        <div class="form-group">
                                            <label for="medida">Medida</label>
                                            <input type="text" class="form-control form-control-sm" name="medida" placeholder="32 X 20">

                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlInput1" class="form-label">Clave Sat</label>
                                            <input class="form-control form-control-sm" list="datalistCodes" id="list-Codes" autocomplete="off" placeholder="Escribe una descripcion" >
                                            <input type="text" class="form-control form-control-sm" id="id-Code" hidden>
                                            <datalist id="datalistCodes"></datalist>
                                        </div>

                                        <div class="form-group">
                                            <label for="formula">Formula</label>
                                            <select name="formula" class="form-control form-control-sm" id="slt-formulas"></select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-sm" name="desc" disabled>

                                        </div>

                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#modal-confirmation">
                                            <span class="material-icons align-middle">done</span>
                                            crear código
                                        </button>
                                    </div>
                                </div>
                            </form>
                             
                        </div>
                        
                    </div>                                        
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>


      
     
</html>