export class Clave {
    

    getClaves() {
        
        return fetch('php/select-cat-prodserv.php')
        .then(data => data.json())
        .then(data => {
            return data; 
        })

    }

    async setClaves() {

        const DATA = await this.getClaves();
        let options = `<option value="-1" selected disabled>Selecciona una clave</option>`
        DATA.forEach(clave => {
            options += `<option value="${clave.ClaveSAT}">${clave.Descrip}</option>`;
        });
        document.querySelector('#slt-claves').innerHTML = options;


    }

    async setListICodeItems(value) {

      //funcion para llenar el datalist del modal de code 
      console.log(value);
      if(value == '-1'){
        var DATA = await this.getItems('general', value);
      }else{
        var DATA = await this.getItems('description', value);
      }
      let options = ``
      DATA.forEach(item => {
        options += `<option value="${item.Descrip}">`
      });
      document.querySelector('#datalistCodes').innerHTML = options;

    }

    getItems(token, value) {

      // esto obtendrá la info para rellenar los datalist del modal add new item
      const FORM_DATA = new FormData()
      const DATA = {
        code: {item_code: value, description: ''},
        description: {item_code: '', description: value},
        general: {item_code: '', description: ''}
      }

      FORM_DATA.append('item_code', DATA[token].item_code);
      FORM_DATA.append('desc', DATA[token].description);
      
      return fetch('php/select-cat-prodserv.php', { method: 'POST', body: FORM_DATA })
      .then(data => data.json())
      .then(data => {
        return data;
      })

    }
  
}