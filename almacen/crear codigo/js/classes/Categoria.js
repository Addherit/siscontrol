export class Categoria {
    

    getCategorias() {
        
        return fetch('php/select-formulas.php')
        .then(data => data.json())
        .then(data => {
            return data; 
        })

    }

    async setCategorias() {

        const DATA = await this.getCategorias();
        let options = `<option value="-1" selected disabled>Selecciona una Formula</option>`
        DATA.forEach(unidad => {
            options += `<option value="${unidad.Formula}">${unidad.Formula}</option>`;
        });
        document.querySelector('#slt-formulas').innerHTML = options;


    }

    async ChangeDesc() {
        console.log("entro");
        let v = document.querySelector('[name=formula]').value;
        let descformula = '';
        console.log(v);
        //let v = document.querySelector('#slt-formulas');
        //console.log(v);
	//description

      switch (v) {

        case 'azul':
            descformula = 'DIAMETRO X DIAMETRO X 4 X LARGO';
        break;
        case 'naranja':
            descformula = 'ESPESOR  X ANCHO X  5  X (LARGO)';

        break;
        case 'pistache':
            descformula = 'DIAMETRO X DIAMETRO X 5  X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 5 (VARIANTE FIJA) X LARGO (EN METROS)

        break;
        case 'negro':
            descformula = 'DIAMETRO X DIAMETRO X 1.4 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 1.4 (VARIANTE FIJA) X LARGO (EN METROS)	

        break;
        case 'blanco':
            descformula = 'PESO X METRO X LARGO X CANTIDAD';
          
        break;   
        case 'amarillo':
            descformula = 'ESPESOR X ANCHO X 1.8 X (LARGO)';
      // ESPESOR(PULGADAS) X ANCHO(PULGADAS) X 1.8 (VARIANTE FIJA) X (LARGO EN METROS)					
          
        break;   
        case 'morado':
            descformula = ' DIAMETRO  X DIAMETRO X  1.8 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  1.8 (VARIANTE FIJA) X (LARGO EN METROS)
        break;   
        case 'rey':
            descformula = 'DIAMETRO X DIAMETRO X 4.5 X LARGO';
      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 4.5 (VARIANTE FIJA) X LARGO (EN METROS)	
          
        break;   
        case 'gris':
            descformula = ' DIAMETRO  X DIAMETRO X  5.5 X (LARGO)';
      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  5.5 (VARIANTE FIJA) X (LARGO EN METROS)	
          
        break; 
        case 'verde':
            descformula = 'DIAMETRO EXTERIOR AL CUADRADO -  DIAMETRO INTERIOR AL CUADRADO X 5 X LARGO';
      // DIAMETRO EXTERIOR (PULGADAS) AL CUADRADO -  DIAMETRO INTERIOR (PULGADAS) AL CUADRADO X 5 (VARIANTE FIJA) X LARGO (EN METROS)
          
        break;   
        case 'placany':
            descformula = 'PLACA';

        break;
        case 'rojo':
            descformula = 'PLACA';

        break;
    }
        document.querySelector('[name=desc]').value = descformula;

    }
  
}