import { Unidad } from "./classes/Unidad.js";
import { Categoria } from "./classes/Categoria.js";
import { Clave } from "./classes/ClaveSat.js";

const UNIDAD = new Unidad();
const CATEGORIA = new Categoria();
const CLAVE = new Clave();
//Inicio
document.addEventListener('DOMContentLoaded', () => init());
// Acciones
document.querySelector('#form-add-codigo').addEventListener('submit', (e) => {
    //modal
    document.querySelector('#completing-msg').innerHTML = ` De  Guardar`;
    //prevent

    e.preventDefault();
    //putCodigo();
})

document.querySelector('#btn-confirm').addEventListener('click', () => putCodigo());
document.querySelector('#slt-formulas').addEventListener('change', () =>CATEGORIA.ChangeDesc()); 
document.querySelector('#list-Codes').addEventListener('keyup', function(e){
    // funcion de cada character
    var v = document.querySelector('#list-Codes').value;
    console.log(v);
    CLAVE.setListICodeItems(v);
}); 


//funciones
const init = () => {
    UNIDAD.setUnidades();
    CLAVE.setListICodeItems('-1');
    CATEGORIA.setCategorias();

}

function putCodigo() {
    
    // tomar info de el formulario
    let codigo = document.querySelector('[name=clave-producto]').value;
    let unidad = document.querySelector('[name=unidad-pulgadas]').value;
    let formula = document.querySelector('[name=formula]').value;

    //revision de codigo
    console.log(codigo.indexOf('"'));
    console.log(codigo);
    if(codigo.indexOf('"') == -1 && codigo != ''){
        //revision de unidad
        if(!unidad){
            const MSN = new Message("danger", "No se ingreso Unidad en Pulgadas ");
            MSN.showMessage();
        }else{
            //revision de la formula
            if(formula == -1){
                const MSN = new Message("danger", "No se ingreso Una Formula");
                MSN.showMessage();
            }else{
                //ingreso del codigo
                const FORMDATA = new FormData(document.querySelector('#form-add-codigo'))
                fetch('php/insert-codigo.php', { method: 'POST', body: FORMDATA })
                .then(data => data.json())
                .then(data => {                
                    const MSN = new Message(data.type, data.message);
                    MSN.showMessage();
                })
            }
        }
    }else{
        const MSN = new Message("danger", "La Clave del Producto Tiene algun caracter Invalido o Esta Vacio");
        MSN.showMessage();
    }

}
