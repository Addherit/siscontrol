import { Unidad } from "./classes/unidad.js";
import { Producto } from "./classes/producto.js";
import { Almacen } from "./classes/Almacen.js";

const UNIDAD = new Unidad();
const ALMACEN = new Almacen();

document.addEventListener('DOMContentLoaded', () => {
    
    
    const PRODUCTO = new Producto();
    UNIDAD.setUnidades();
    setInterval(() => getFechaActual(), 1000);  
    ALMACEN.getAlmacenes();
    PRODUCTO.setListICodeItems('');
    PRODUCTO.setListIDescriptionItems('');    
    

});

document.querySelector('#btn-registroEntrada').addEventListener('click', () => document.querySelector('#completing-msg').innerHTML = ` De  Guardar`);
document.querySelector('#btn-confirm').addEventListener('click', () => registroEntrada());
// document.querySelector('#btn-upload-pdf').addEventListener('change', () => uploadCertificado() );
document.querySelector('#list-Codes').addEventListener('change', () => getInfoProducto('code') );
document.querySelector('#descripcion-Codes').addEventListener('change', () => getInfoProducto('description') );
document.querySelector('[name=almacen-reg]').addEventListener('change', () => ALMACEN.setListMeidasPlacas());

function getFechaActual() {

    const FECHA = moment();
    FECHA.locale('es')
    document.querySelector('[name=fecha]').value = FECHA.format('MMMM Do YYYY, h:mm:ss a'); 

}

function registroEntrada() {

    var INPUT = document.querySelector('#medidas-placas');
    const INPUTI = document.querySelector('.medidas-placas');
    console.log(INPUTI);
    console.log(INPUT.value);

    if(INPUTI.hidden == false){ // si tiene placa
        if( INPUT.value != ''){
            const FORM_DATA = new FormData(document.querySelector('#form-entradas-salidas'));    
            fetch('php/insert-entrada.php', { method: 'POST', body: FORM_DATA })
            .then(data => data.json())
            .then(data => {
                uploadCertificado(data.id)
                const MSN = new Message(data.type, data.message);
                MSN.showMessage();
            })
        }else{
            const MSN = new Message("danger", "La Medida de Placa no esta ingresada");
            MSN.showMessage();
        }
    }else{ // si no tiene placa
        const FORM_DATA = new FormData(document.querySelector('#form-entradas-salidas'));    
        fetch('php/insert-entrada.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            uploadCertificado(data.id)
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }
}

async function getInfoProducto(event) {

    const PRODUCTO = new Producto();
    event === 'code' ? PRODUCTO.setDescriptionItem(document.querySelector('#list-Codes').value) : PRODUCTO.setCodeItem(document.querySelector('#descripcion-Codes').value);
    
}

async function uploadCertificado(id_entrada) {
    
    for (let key = 0; key < document.querySelector('#btn-upload-pdf').files.length; key++) {

        const FILE = document.querySelector('#btn-upload-pdf').files[key];
        const TMP_NAME = await copyPDFTmp(FILE, id_entrada);
        let message = ``;
        let type =``;
        if (TMP_NAME) {
            message = `Se subió correctamente el archivo PDF`;
            type = 'success';
        } else {
            message = `Error al subir el archivo PDF`;
            type = 'danger';
        }
        
        const MSN = new Message(type, message);
        MSN.showMessage();
    }

}

function copyPDFTmp(file, id_entrada) {

    const FORM_DATA = new FormData()
    FORM_DATA.append('file', file);
    FORM_DATA.append('id_entrada', id_entrada)

    //Create file tmp in dir tmpxml and return the tmp name
    return fetch('php/copy-tmp-pdf.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => { 
       return data.tmp_name;
    })

}  