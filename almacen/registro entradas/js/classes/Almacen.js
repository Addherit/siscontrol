export class Almacen {

    async getAlmacenes() {

        const DATA = await fetch('php/select-almacenes.php');
        const ALMACENES = await DATA.json();
        let option = '';
        ALMACENES.forEach(almacen => {
            option += `<option value="${almacen.Id}">${almacen.Nombre}</option>`;        
        });
        document.querySelector('[name=almacen-reg]').innerHTML = option;
        
    }

    async setListMeidasPlacas() {
        
        const FORM_DATA = new FormData();
        FORM_DATA.append('id-almacen', document.querySelector('[name=almacen-reg]').value);
        FORM_DATA.append('id-producto', document.querySelector('[name=id-producto]').value);

        fetch('php/select-stock.php', {method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            let options = ``;
            data.forEach(item => {
                options += `<option value='${item.MedidaPlaca}'>`;
            });
            document.querySelector('#list-medidas-placas').innerHTML = options;
        
        })        
  
    }
}