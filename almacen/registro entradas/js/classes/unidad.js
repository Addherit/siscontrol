export class Unidad {
    

    getUnidades() {
        
        return fetch('php/select-unidades.php')
        .then(data => data.json())
        .then(data => {
            return data; 
        })

    }

    async setUnidades() {

        const DATA = await this.getUnidades();
        let options = `<option value="" selected disabled>Selecciona unidad</option>`
        DATA.forEach(unidad => {
            options += `<option value="${unidad.Unidad}">${unidad.Unidad}</option>`;
        });
        document.querySelector('[name=unidad]').innerHTML = options;
    }
}