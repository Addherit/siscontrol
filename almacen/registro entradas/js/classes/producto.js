import { Almacen } from "./Almacen.js";

export class Producto {

    getProducts(value, token) {

        // esto obtendrá la info para rellenar los datalist del modal add new item
        const FORM_DATA = new FormData()
        const DATA = {
            code: {item_code: value, description: ''},
            description: {item_code: '', description: value},
            general: {item_code: '', description: ''}
        }

        FORM_DATA.append('item_code', DATA[token].item_code);
        FORM_DATA.append('description', DATA[token].description);
        
        return fetch('php/select-items.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            return data;
        })


    }

    async setListICodeItems(value) {

        //funcion para llenar el datalist del modal de code 
        const DATA = await this.getProducts(value, 'general');
        let options = ``
        DATA.forEach(item => {
          options += `<option value="${item.Clave_Prod}">`
        });
        document.querySelector('#datalistCodes').innerHTML = options;
  
      }
  
      async setListIDescriptionItems(value) {
  
        //funcion para llenar el datalist del modal de descripcion
        const DATA = await this.getProducts(value, 'general');
        let options = ``
        DATA.forEach(item => {
          options += `<option value="${item.Descripcion}">`
        });
        document.querySelector('#datalistDescription').innerHTML = options;
  
      }

      async setDescriptionItem(code_item) {

        //Function to show description if was found itme by code
        const DATA = await this.getProducts(code_item, 'code');
        document.querySelector('#descripcion-Codes').value = DATA[0].Descripcion;
        document.querySelector('[name=unidad]').value = DATA[0].Unidad;
        document.querySelector('[name=id-producto]').value = DATA[0].ID_Producto;
        // vaidar si es una placa
        this.isPlaca(DATA[0].Categoria);
        const ALMACEN = new Almacen();
        ALMACEN.setListMeidasPlacas();


      }
  
      async setCodeItem(description_item) {
  
        //Function to show description if was found itme by description
        const DATA = await this.getProducts(description_item, 'description');
        document.querySelector('#list-Codes').value = DATA[0].Clave_Prod;
        document.querySelector('[name=unidad]').value = DATA[0].Unidad;
        document.querySelector('[name=id-producto]').value = DATA[0].ID_Producto;
        // vaidar si es una placa
        this.isPlaca(DATA[0].Categoria);
        const ALMACEN = new Almacen();
        ALMACEN.setListMeidasPlacas();
  
      }

      isPlaca(categoria) {

        const INPUT = document.querySelector('.medidas-placas');
        if (categoria === 'M-1' || categoria === 'P' || categoria === 'IP' || categoria === 'AP') {
          INPUT.hidden = false;
        } else {
          INPUT.hidden = true;
        }
        
      }


}