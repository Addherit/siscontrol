<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "../../assets/modales/modal-delete-confirmation.html" ?>
        <?php include "../../assets/modales/modal-confirmation.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                                <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">REGISTRO DE ENTRADA</span>
                        </div>                        
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <!-- <img src="/siscontrol/assets/img/en-construccion.png" alt="" srcset="" > -->
                            <form id="form-entradas-salidas">

                                <div class="row mb-3">
                                <div class="col-4">
                                <label for="exampleFormControlInput1" class="form-label">Código</label>
                                <input class="form-control form-control-sm" list="datalistCodes" id="list-Codes" name="codigo" placeholder="Escribe un código">
                                <input type="text" name="id-producto" hidden >
                                <datalist id="datalistCodes"></datalist>
                            </div>
                            <div class="col-8">
                                <label for="exampleFormControlInput1" class="form-label">Descripción</label>
                                <input class="form-control form-control-sm" list="datalistDescription" id="descripcion-Codes" name="descripcion" placeholder="Escribe una descripción">
                                <datalist id="datalistDescription"></datalist>                                
                            </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-2">
                                        <label for="unidad" class="form-label">Unidad</label>
                                        <select class="form-control form-control-sm" name="unidad"></select>
                                    </div>
                                    <div class="col-2">
                                        <label for="entrada" class="form-label">Entrada</label>
                                        <input class="form-control form-control-sm" name="entrada-salida">
                                    </div>
                                    <div class="col-2">
                                        <label for="largo" class="form-label">Largo 
                                            <select name="ulargo" id="unidad-medida">
                                                <option value="m">m</option>
                                                <option value="cm">cm</option>
                                                <option value="mm">mm</option>
                                                <option value="in">in</option>
                                            </select>
                                        </label>                            
                                       <!-- <label for="largo" class="form-label">Largo</label>-->
                                        <input class="form-control form-control-sm" name="largo">
                                    </div>
                                    <div class="col-1">
                                        <label for="pieza" class="form-label">Pieza</label>
                                        <input class="form-control form-control-sm" name="pieza">
                                    </div>
                                    <div class="col-3">
                                        <label for="proveedor" class="form-label">Proveedor</label>                            
                                        <input class="form-control form-control-sm" name="proveedor">
                                    </div>
                                    <div class="col-2">
                                        <label for="n-colada" class="form-label">N° De Colada</label>                            
                                        <input class="form-control form-control-sm" name="n-colada">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-3">
                                        <label for="fecha" class="form-label">Fecha</label>                            
                                        <input class="form-control form-control-sm" name="fecha">
                                    </div>
                                    <div class="col-3">
                                        <label for="n-certificado" class="form-label">N° De Certificado</label>                            
                                        <input class="form-control form-control-sm" name="n-certificado">
                                    </div>
                                    <div class="col-3">
                                        <label for="almacen-reg" class="form-label">Almacen</label>                            
                                        <select class="form-control form-control-sm" name="almacen-reg"></select>
                                    </div>
                                    <div class="col-3">
                                        <label for="numero-pedimento" class="form-label">Numero pedimento</label>       
                                        <input class="form-control form-control-sm" name="numero-pedimento">                     
                                    </div>
                                    <div class="col-3 medidas-placas" hidden>
                                        <label for="medidas-placas" class="form-label">Medidas placas</label>
                                        <input class="form-control form-control-sm" list="list-medidas-placas" name="medida-placa" id="medidas-placas" placeholder="Escribe una medida">
                                        <datalist id="list-medidas-placas"></datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 text-right">
                                        <button class="btn btn-success" type="button" id="btn-registroEntrada" data-toggle="modal" data-target="#modal-confirmation">
                                            <span class="material-icons align-middle">input</span>
                                            Registrar de Entrada
                                        </button>                                                                                
                                    </div>                                    
                                    <div class="col-6 text-left">
                                        <input type="file" name="btn-upload-pdf" id="btn-upload-pdf" class="inputfile" multiple>
                                        <label for="btn-upload-pdf"><span class="material-icons align-middle">picture_as_pdf</span> Cargar certificado</label>                         
                                    </div>  
                                </div>
                                
                            </form>
                        </div>
                    </div>                                        
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>