import { Entradas } from "./classes/entradas.js";

const ENTRADA = new Entradas();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-update-entrada').addEventListener('click', () => ENTRADA.UpdateEntrada());
document.querySelector('#btn-download-cert').addEventListener('click', () => ENTRADA.DownloadCert());
document.querySelector('#btn-disabled').addEventListener('click', () => ENTRADA.disabledEntrada());

const init = () => {
    ENTRADA.setEntrada();

}



