import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();


export class Entradas {

    constructor () {
        this.idProduct = ``;
    }

    getEntrada() {

        
        return fetch('php/select-entradas.php')
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data;
        })

    }

    async setEntrada() {

        SPINNER.showSpinner();
        const DATA = await this.getEntrada();
        let lineas = ``;

        DATA.forEach(producto => {

            const STATUS = this.getColorStatus(producto.Estatus);
            const BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(producto.Estatus);
            lineas += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-info btn-edit" title="Detalles" data-toggle="modal" data-target="#modal-info-entrada">
                            <span class="material-icons align-middle">info</span>
                            Detalles
                        </button>
                    </td>
                    <td>${producto.Id}</td>
                    <td>${producto.Codigo}</td>
                    <td>${producto.Descripcion}</td>
                    <td>${producto.Unidad}</td>
                    <td>${producto.EnSal}</td>
                    <td>${producto.Pieza}</td>
                    <td>${producto.MedidaL}</td>
                    <td>${producto.DateCreated}</td>
                    ${STATUS}
                </tr>
            `
        });

        document.querySelector('#list-inventario tbody').innerHTML = lineas;
        const dataTable = new DataTable('list-inventario');
        this.addEventClickToBtnDeleteActivate();
        this.addEventClickToBtnEditProduct();
        dataTable.initDataTable();

    }
    addEventClickToBtnDeleteActivate() {
        document.querySelectorAll('.btn-delete-activate').forEach(btn => {
            btn.addEventListener('click', (event) => {
       
                const NAME_PRODUCT = event.currentTarget.parentElement.parentElement.children[2].textContent;
                console.log(NAME_PRODUCT);
                document.querySelector('#disabled-name').innerHTML = `la entrada`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                document.querySelector('#confirm-name').innerHTML = `la entrada`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                this.idProduct = event.currentTarget.parentElement.parentElement.children[1].textContent;
                
            })
        })

    }

    addEventClickToBtnEditProduct() {
        document.querySelectorAll('.btn-edit').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoEntradaById(ID_PRODUCT);
                
            })
        })        

    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Terminada: `<td>${status}</td>`,
            Eliminada: `<td class="text-danger">${status}</td>`
        }

        return COLOR_STATUS[status];

    }

    getBtnActivateDisabled(status) {


        if (status == 'Terminada') {
            return `
            <button type="button" class="btn btn-sm btn-success btn-delete-activate" title="Activar Prodcuto" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else {
            return `
            <button type="button" class="btn btn-sm btn-danger btn-delete-activate" title="Desactivar producto" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Eliminar
            </button>`
        }
       
    }

    disabledEntrada() {
        const ID_PRODUCT = this.idProduct        
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', ID_PRODUCT);
        fetch('php/eliminar-entrada.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-disabled-confirmation').modal('hide');
            this.setEntrada();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    activateProduct() {
        /*
        const ID_PRODUCT = this.idProduct
        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', ID_PRODUCT);
        fetch('php/activar-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })
        */

    }

    UpdateEntrada() {
        const FORM_DATA = new FormData(document.querySelector("#form-edit-entrada"))        
        fetch('php/update-entrada.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            $('#modal-info-entrada').modal('hide');
            this.setEntrada();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
            
        })

    }

    DownloadCert() {

        const NAME_CERTIFICADO = localStorage.getItem('cert_name');

        NAME_CERTIFICADO.replace(' ','').split(',').forEach(async certificado => {
            const URL = `https://farguaceros.com/siscontrol/TempFiles/Certificados/${certificado}`;        
            const FILE_EXIST = await fetch(URL);

            if (FILE_EXIST.status == 200) {
                window.open(URL, '_blank');   
            } else {
                const MSN = new Message('danger', 'No se Encontro el Certificado');
                MSN.showMessage();
            }
        })
    }

    getInfoEntradaById(id_product) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', id_product)        
        return fetch('php/select-entrada.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
           return data;  
        })
    }

    async setInfoEntradaById(id_product) {
        const Entrada = await this.getInfoEntradaById(id_product);
        document.querySelector('[name=id]').value = Entrada[0].Id
        document.querySelector('[name=codigo]').value = Entrada[0].Codigo
        document.querySelector('[name=descripcion]').value = Entrada[0].Descripcion
        document.querySelector('[name=unidad]').value = Entrada[0].Unidad
        document.querySelector('[name=stock]').value = Entrada[0].EnSal
        document.querySelector('[name=Fecha]').value = Entrada[0].DateCreated
        document.querySelector('[name=almacen]').value = Entrada[0].NombreA
        document.querySelector('[name=status]').value = Entrada[0].Estatus            
        document.querySelector('[name=piezas]').value = Entrada[0].Pieza
        document.querySelector('[name=largo]').value = Entrada[0].LargoFormat
        document.getElementById('largou').value = Entrada[0].MedidaL

        localStorage.setItem('cert_name', Entrada[0].FIlecertificado);

        switch (Entrada[0].U_Largo) {
            case 'in':
                document.getElementById("unidad-medidau").selectedIndex = 3;
                break;
        
            case 'cm':
                
                document.getElementById("unidad-medidau").selectedIndex = 1;
                break;
            case 'mm':
                
                document.getElementById("unidad-medidau").selectedIndex = 2;
                break;
            case 'm':
                
                document.getElementById("unidad-medidau").selectedIndex = 0;
                break;
            default:
                document.getElementById("unidad-medidau").selectedIndex = 0;
                break;
        }
        document.querySelector('[name=provedor]').value = Entrada[0].Proveedor
        document.querySelector('[name=colada]').value = Entrada[0].NColada            
    }
   
}