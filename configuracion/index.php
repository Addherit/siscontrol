<!DOCTYPE html>
<html lang="en">
    <?php include "../includes/header.html" ?>
    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg1 toggled">
            <?php include "../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <h2>Configuración</h2>
                        </div>
                        <div class="form-group col-md-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#">
                                <span>Toggle Sidebar</span>
                            </a>
                            <a id="pin-sidebar" class="btn btn-outline-secondary rounded-0" href="#">
                                <span>Pin Sidebar</span>
                            </a>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <h3>Tema</h3>
                            <p>Puedes seleccionar el color de fondo para el sidebar</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <a href="#" data-theme="default-theme" title="default-theme" class="theme default-theme selected"></a>
                            <a href="#" data-theme="chiller-theme" title="chiller-theme" class="theme chiller-theme"></a>
                            <a href="#" data-theme="legacy-theme" title="legacy-theme" class="theme legacy-theme"></a>
                            <a href="#" data-theme="ice-theme" title="ice-theme" class="theme ice-theme"></a>
                            <a href="#" data-theme="cool-theme" title="cool-theme" class="theme cool-theme"></a>
                            <a href="#" data-theme="light-theme" title="light-theme" class="theme light-theme"></a>
                        </div>
                        <div class="form-group col-md-12">
                            <p>También puedes usar una imagen de fondo</p>
                        </div>
                        <div class="form-group col-md-12">
                            <a href="#" data-bg="bg1" class="theme theme-bg"></a>
                            <a href="#" data-bg="bg2" class="theme theme-bg selected"></a>
                            <a href="#" data-bg="bg3" class="theme theme-bg"></a>
                            <a href="#" data-bg="bg4" class="theme theme-bg"></a>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="toggle-bg" checked>
                                <label class="custom-control-label" for="toggle-bg">Imgen de fondo</label>
                            </div>
                        </div>                        
                    </div>                               
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>
    <?php include "../includes/footer.html" ?>
</html>