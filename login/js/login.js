`use strict`;

// this class it's located in crm/asssets/js/classes
const NEW_SESSION = new UserSession();

document.addEventListener('DOMContentLoaded', () => { NEW_SESSION.validateStartedSession() })
document.querySelector("#form-login").addEventListener('submit', (event) => { event.preventDefault(); NEW_SESSION.logIn() })
document.querySelector("#btn-recover-password").addEventListener('click', () => {NEW_SESSION.recoverPassword() })