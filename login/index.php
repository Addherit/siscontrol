<!DOCTYPE html>
<html lang="en">

<?php include "../includes/header.html"?>
<link rel="stylesheet" href="/siscontrol/login/css/login.css">

<body>

    <div class="wrapper fadeInDown">
        <div id="formContent">                

            <!-- Icon -->
            <div class="fadeIn first icon">
                <img class="mt-2 mb-3" src="/siscontrol/assets/img/logo.png" alt="Fargu Logo" width="40%">
                <!-- <span class="material-icons" style=" font-size: 10rem; color: #ebebeb">account_circle</span> -->
                <br>
                <h4 style="color: #cfcece">INICIO DE SESIÓN</h4>
            </div>         

            <!-- Login Form -->            
            <form id="form-login" name="form-login">
                <input type="text" id="login" class="fadeIn second" name="login" placeholder="User">
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
                <input type="submit" class="fadeIn fourth" value="Log In">
            </form>

            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover" id="btn-recover-password" href="#">Olvidaste tu Password?</a>
            </div>
        </div>
    </div>    

    
    
</body>
<?php include "../includes/footer.html"?>
<script src="/siscontrol/login/js/login.js"></script>
</html>