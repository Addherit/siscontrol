class UserSession {

    validateStartedSession() {
        
        const url = window.location.pathname;
        const userID = typeof(localStorage.getItem('user_id'));
        if (userID == 'string' && url == '/siscontrol/') {            
            switch (localStorage.getItem('id_user_type')) {
                
                case '1':
                    window.location = '/siscontrol/dashboard/index.php';
                    break;
                case '2':
                    window.location = '/siscontrol/administrativo/pedidos/index.php';
                    break;
                case '3':
                    window.location = '/siscontrol/administrativo/pedidos/index.php';
                    break;
                case '4':
                    window.location = '/siscontrol/almacen/inventario/index.php';
                    break;
                case '5':
                    window.location = '/siscontrol/administrativo/pedidos/index.php';
                    break;
                case '6':
                    window.location = '/siscontrol/administrativo/comprobante de nomina/index.php';
                    break;
                default:
                    window.location = '/siscontrol/dashboard/index.php';
                    break;
            }
        } else {            
            if (userID != 'string' && url != '/siscontrol/') {
                window.location = '/siscontrol/';
            }
        }

    }

    logIn() {

        const form = new FormData(document.querySelector("#form-login"));    
        fetch("/siscontrol/login/php/secure_login.php", {method: 'POST', body: form })
        .then(data => data.json())
        .then(data => {
            if (!data.mensaje) {                           
                if (data.status.toLowerCase() === 'inactivo') {
                    const msn = new Message('danger', 'Usuario con estatus Inactivo, intenta con otro');
                    msn.showMessage(); 
                } else {
                    localStorage.clear();              
                    this.createLocalVariables(data);
                    this.validateStartedSession();
                }
            } else {
                const msn = new Message(data.type, data.mensaje);
                msn.showMessage(); 
            }
        })

    }

    createLocalVariables(data) {

        // limpias Localstorage para crear las variables con la información del usuario logueado    
        localStorage.setItem('user_id', data.userId);
        localStorage.setItem('id_user_type', data.Tipo_usuario);
        localStorage.setItem('user_type', data.Tipo);
        localStorage.setItem('all_name', `${data.Nombre}`);
        localStorage.setItem('user', data.name);
        localStorage.setItem('email', data.correo);
                
    }

    logOut() {
        localStorage.clear();
        location.reload();
    }

    recoverPassword() {
        const msn = new Message('info', 'Esta función no está disponible');
        msn.showMessage(); 
    }

    UserPermissions() {

        const ID_USER_TYPE = localStorage.getItem('id_user_type');
        let menu = [];
        switch (ID_USER_TYPE) {
                
            case '1':
              //admin
              menu.push('ventas', 'dashboard', 'administrativo', 'almacen', 'nuevo-pedido', 'pedidos', 'comprobante-nomina', 'usuarios', 'inventario', 'crear-codigo', 'registro-entrada', 'tipo-cambio', 'clientes', 'calculadora-comisiones', 'entradas','Listas','factura-publico','globales')

                break;
            case '2':
                //agente
                menu.push('administrativo', 'nuevo-pedido', 'pedidos','almacen', 'inventario','ventas', 'clientes')
                break;
            case '3':
                //facturas 
                menu.push('administrativo', 'pedidos','ventas', 'clientes', 'factura-publico', 'globales')
                break;
            case '4':
                //almacen
                menu.push('administrativo','pedidos', 'almacen', 'inventario','registro-entrada','entradas','crear-codigo')
                break;
            case '5':
                //cobranza
                menu.push('administrativo','pedidos','ventas', 'clientes')
                break;
            case '6':
                //contabilidad
                menu.push('administrativo','comprobante-nomina')
                break;
        }
        menu.forEach(element => document.querySelector(`.${element}`).hidden = false);

    }
    
}



