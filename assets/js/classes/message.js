class Message {
    
    constructor(type, message) {
                
        this.message = message;        
        this.type = type; // success/danger/info/warning

    }

    showMessage() {

        const ICON = this.selectIconMessage();
        
        const options = {                  
            message: `${ICON} <span style="vertical-align: super;">${this.message}</span>`,            
        }
        const settings = {
            type: this.type,  
            z_index: 10000000, 
            // position: 'absolute',
            // top: 100px,
            // placement: {
            //     from: "top",
            //     align: "right"
            // },
            animate: {
                enter: 'animate__animated animate__fadeInRight',
                exit: 'animate__animated animate__fadeOutRight'
            },      
            template: 
                `<div data-notify="container" class="col-11 col-md-4 alert alert-{0}" role="alert" >
                    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">
                        <span class="material-icons" style="font-size: 1rem">close</span>
                    </button>                    
                    <span data-notify="message">{2}</span>
                </div>`
        }

        $.notify(options,settings);

    }

    selectIconMessage() {
        
        switch (this.type) {
            case 'success':
                return '<span class="font-weight-bold"><span class="material-icons">check_circle_outline</span><span style="vertical-align: super; padding-left: 4px">Éxito:</span></span>';
                break;
            case 'danger':
                return '<span class="font-weight-bold"><span class="material-icons">dangerous</span><span style="vertical-align: super; padding-left: 4px">Error:</span></span>';                
                break;        
            case 'info':
                return '<span class="font-weight-bold"><span class="material-icons">help_outline</span><span style="vertical-align: super; padding-left: 4px">Info:</span></span>';
                break;
            case 'warning':
                return '<span class="font-weight-bold"><span class="material-icons">warning_amber</span><span style="vertical-align: super; padding-left: 4px">Alerta:</span></span>';
                break;
        }
    }

}


