export class DataTable {

    constructor(tableId) {
        this.tableId = tableId
    }

    initDataTable() {
        
        $(`#${this.tableId}`).DataTable();        
        
    }

}


