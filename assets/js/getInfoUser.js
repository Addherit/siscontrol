`use strict`



if (window.location.pathname != '/siscontrol/') {
    //First validate if view is diferent of logion page
    document.addEventListener('DOMContentLoaded', () => {

        document.querySelector('#user-name').innerHTML = localStorage.getItem('all_name').split(' ')[0]
        document.querySelector('#user-role').innerHTML = localStorage.getItem('user_type')
        
    })
    // this class it's located in crm/asssets/js/classes
    const NEW_SESSION = new UserSession();
    NEW_SESSION.validateStartedSession();
    NEW_SESSION.UserPermissions();
    document.querySelector("#log-out").addEventListener('click', () => { NEW_SESSION.logOut() })
}