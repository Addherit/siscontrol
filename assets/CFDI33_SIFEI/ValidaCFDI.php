<?php
// Consulta el estado de una factura en el SAT
// @ToRo 2016 https://tar.mx/tema/facturacion-electronica.html
//

$rfcemisor = "CHP12110757A";
$rfcreceptor = "SAF980202D99";
$uuid = "81CFDFAB-F7D2-4042-91E3-6EDAC617C950";
$total = "1740000.00";

$options=array('trace'=>true, 'stream_context'=>stream_context_create( ['http' => ['timeout'=>1] ] ));
$client = new SoapClient("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl",$options);
//uso: asumimos que tenemos el RFC del emisor, el RFC del receptor
// el total y el uuid (folio fiscal).
$factura = "?re=".$rfcemisor."&rr=".$rfcreceptor."&tt=".str_pad(number_format($total,6,".",""),17,0,STR_PAD_LEFT)."&id=".strtoupper($uuid);
$resultado = $client->Consulta(['expresionImpresa'=>$factura]);
//print_r(json_decode($resultado));

$Consult = $resultado->ConsultaResult;
echo "||CodigoEstatus: ".$Consult->CodigoEstatus;
echo " ||EsCancelable: ".$Consult->EsCancelable;
echo " ||Estado: ".$Consult->Estado;
echo " ||EstatusCancelacion: ".$Consult->EstatusCancelacion;
echo " ||ValidacionEFOS: ".$Consult->ValidacionEFOS;
//en caso que el SAT responda, dará una respuesta (objeto) parecida a:

/*stdClass Object
(
    [ConsultaResult] => stdClass Object
    (
        [CodigoEstatus] => S - Comprobante obtenido satisfactoriamente.
        [Estado] => Vigente
    )
)*/
