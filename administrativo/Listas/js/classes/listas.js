import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();


export class Listas {

    constructor () {
        this.idList = ``;
    }

    getListas() {

        
        return fetch('php/select-listas.php')
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data;
        })

    }

    async setListas() {

        SPINNER.showSpinner();
        const DATA = await this.getListas();
        let lineas = ``;

        DATA.forEach(producto => {

            const STATUS = this.getColorStatus(producto.Estado);
            const BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(producto.Estado);
            lineas += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-info btn-info" title="Detalles" >
                            <span class="material-icons align-middle">info</span>
                            Detalles
                        </button>
                        <button type="button" class="btn btn-sm btn-warning btn-edit" title="Edit" data-toggle="modal" data-target="#modal-edit-lista">
                            <span class="material-icons align-middle">edit</span>
                            Editar
                        </button>
                    </td>
                    <td>${producto.ID}</td>
                    <td>${producto.Nombre}</td>
                    ${STATUS}
                </tr>
            `
        });

        document.querySelector('#list-inventario tbody').innerHTML = lineas;
        const dataTable = new DataTable('list-inventario');
        this.addEventClickToBtnDeleteActivate();
        this.addEventClickToBtnEditProduct();
        this.addEventClickToBtnInfoLista();
        dataTable.initDataTable();

    }

    addEventClickToBtnDeleteActivate() {
        document.querySelectorAll('.btn-delete-activate').forEach(btn => {
            btn.addEventListener('click', (event) => {
       
                const NAME_PRODUCT = event.currentTarget.parentElement.parentElement.children[3].textContent;
                document.querySelector('#disabled-name').innerHTML = `la Lista`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                document.querySelector('#confirm-name').innerHTML = `la Lista`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                this.idList = event.currentTarget.parentElement.parentElement.children[1].textContent;
                
            })
        })

    }

    addEventClickToBtnEditProduct() {
        document.querySelectorAll('.btn-edit').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoListaById(ID_PRODUCT);
                
            })
        })        

    }

    addEventClickToBtnInfoLista() {
        document.querySelectorAll('.btn-info').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                console.log(ID_PRODUCT);
                this.SendToDetalles(ID_PRODUCT);
                
            })
        })        

    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Terminada: `<td>${status}</td>`,
            Eliminado: `<td class="text-danger">${status}</td>`
        }

        return COLOR_STATUS[status];

    }

    getBtnActivateDisabled(status) {


        if (status == 'Eliminado') {
            return `
            <button type="button" class="btn btn-sm btn-success btn-delete-activate" title="Activar Prodcuto" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else {
            return `
            <button type="button" class="btn btn-sm btn-danger btn-delete-activate" title="Desactivar producto" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Eliminar
            </button>`
        }
       
    }

    disabledLista() {
        const ID_PRODUCT = this.idList        
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', ID_PRODUCT);
        fetch('php/eliminar-lista.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-disabled-confirmation').modal('hide');
            this.setListas();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    activateProduct() {
        /*
        const ID_PRODUCT = this.idProduct
        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', ID_PRODUCT);
        fetch('php/activar-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })
        */

    }

    UpdateLista() {
        const FORM_DATA = new FormData(document.querySelector("#form-save-lista"))        
        fetch('php/update-lista.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            $('#modal-edit-lista').modal('hide');
            this.setListas();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
            
        })

    }

    getInfoListaById(id_product) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('id', id_product)        
        return fetch('php/select-lista.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
           return data;  
        })
    }

    async setInfoListaById(id_product) {
        const List = await this.getInfoListaById(id_product);
        document.querySelector('[name=nombrelist]').value = List[0].Nombre
        document.querySelector('[name=idli]').value = List[0].ID
    }

    ADDLista() {

        let nombre = document.querySelector('[name=nombre]').value; 
        const FORM_DATA = new FormData()
        FORM_DATA.append('nombre', nombre)        
        return fetch('php/add-lista.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-add-lista').modal('hide');
            this.setListas();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })
    }

    SendToDetalles(Id) {
        window.location.href = `/siscontrol/administrativo/ListasDetalle/index.php?id=${Id}`;
    }
   
}