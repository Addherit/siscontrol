import { Listas } from "./classes/listas.js";

const Lista = new Listas();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-new-lista').addEventListener('click', () => Lista.ADDLista());
document.querySelector('#btn-save-lista').addEventListener('click', () => Lista.UpdateLista());
document.querySelector('#btn-disabled').addEventListener('click', () => Lista.disabledLista());

const init = () => {
    Lista.setListas();

}



