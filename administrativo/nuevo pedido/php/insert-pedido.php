<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {
    $js = $_POST['pedido'];

     $exa = json_decode($js);
     $header =$exa->{'header'} ;
     $body =$exa->{'body'} ;
     //header
     $cliente =  $header->{'cliente'};
     $agente=  $header->{'agente'};
     $estatus=  $header->{'estatus'};
     $sub=  $header->{'subtotal'};
     $iva=  $header->{'iva'};
     $total=  $header->{'total'};

    $sql = "INSERT INTO `venta`(  `Cve_Cte`, `Cve_Agente`, `Subtotal`,`IVA`, `Total`, `Estatus`) VALUES ('$cliente','$agente','$sub','$iva','$total','$estatus')";
    $res = $con->query($sql);
    $id = $con->lastInsertId();
 //   print_r($id);
// body inserts
 foreach ($body as $row) {
     	$clave =  $row->{'clave'};
     	$desc =  $row->{'descripcion'};
     	$unidad =  $row->{'unidad'};
     	$cantidad =  $row->{'cantidad'};
     	$precio =  $row->{'precio'};
     	$importe =  $row->{'importe'};
     	$observaciones =  $row->{'observaciones'};
        //nuevas variables
     	$lista_precio =  $row->{'lista_precio'};
     	$precio_corte =  $row->{'precio_corte'};
     	$ltsPz =  $row->{'ltsPzas'};
     	$cortes =  $row->{'cortes'};
     	$almacen =  $row->{'almacen'};
        $peso_teorico =  $row->{'peso_teorico'};
     	$largo =  $row->{'largo'};
     	$ancho =  $row->{'ancho'};
        $espesor =  $row->{'espesor'};
     	$variante_fija =  $row->{'varianteFija'};
     	$diametro =  $row->{'diametro'};
        $diametro_ext =  $row->{'diametro_ext'};
        $diametro_int =  $row->{'diametro_int'};
     	$medida_placa =  $row->{'medida_placa'};
     	$u_medida =  $row->{'u_medida'};
        $formula =  $row->{'formula'};
        $Ncorteid =  $row->{'ncorte'};
      
        if($clave == 's0'){
	      $sqli = "INSERT INTO `ventad`( `ID_Venta`, `Clave_Prod`,`Cantidad`, `Unidad`,`Descripcion`,`Precio`, `Importe`, `Observaciones`, `Corte`, `PesoReal`, `PesoTeorico`,`Lista_precio`, `Precio_corte`, `ltsPz`, `Almacen`, `Largo`, `Ancho`,`Espesor`, `Variante_fija`, `Diametro`, `Diametro_ext`, `Diametro_int`,`Medida_placa`,`U_medida`, `Formula`, `Ncorte`)  VALUES ('$id','$clave','$cantidad','$unidad','$desc','$precio','$importe','$observaciones',0,'$peso_real','$peso_teorico','$lista_precio',0,0,'$almacen',0,0,0,0,0,0,0,'$medida_placa','$u_medida','$formula','$Ncorteid')";
        }else{
	      $sqli = "INSERT INTO `ventad`( `ID_Venta`, `Clave_Prod`,`Cantidad`, `Unidad`,`Descripcion`, `Precio`, `Importe`, `Observaciones`, `Corte`, `PesoReal`, `PesoTeorico`,`Lista_precio`, `Precio_corte`, `ltsPz`, `Almacen`, `Largo`, `Ancho`,`Espesor`, `Variante_fija`, `Diametro`, `Diametro_ext`, `Diametro_int`,`Medida_placa`,`U_medida`, `Formula`, `Ncorte`)  VALUES ('$id','$clave','$cantidad','$unidad','$desc','$precio','$importe','$observaciones',0,'$peso_real','$peso_teorico','$lista_precio',$precio_corte,$ltsPz,'$almacen',$largo,$ancho,$espesor,$variante_fija,$diametro,$diametro_ext,$diametro_int,'$medida_placa','$u_medida','$formula','$Ncorteid')";
        }
	    $con->query($sqli);
        $idvd = $con->lastInsertId();

        //status merma
        $status =  $row->{'status'};
        $merma  =  $row->{'merma'};
        //cuando el status es true existe merma y se guarda merma con stastus cotizacion
        //cuando el status de laventa cambia a abierto o confirmado, la merma se cambia a abierto
        //cuando se cancela una venta se elmimina la merma 
        if($status == true){
            $sqlmerma="INSERT INTO `Merma`( `merma`, `ID_Venta`, `ID_VtaD`, `Producto`, `Almacen`, `Estatus`) VALUES ('$merma',$id,$idvd,'$clave','$almacen','COTIZACION')";
	        $con->query($sqlmerma);
        }

 }

    $result =["type"=>'success',"message"=>'Se Ingreso el pedido correctamente',"sql"=> $sqli,"id"=>$id];
} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($result);
    //comentario de revision
//}
?>