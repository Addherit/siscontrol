`use strict`;

import {Productos} from "./classes/productos.js";
import {Precios} from "./classes/precios.js";
import {Clientes} from "./classes/clientes.js";
import {Pedidos} from "./classes/pedidos.js";
import {Agente} from "./classes/agentes.js";
import {Almacen} from "./classes/almacenes.js";
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();
const ITEM = new Productos();
const ALMACEN =new Almacen();
const PRICESLIST = new Precios();
const PEDIDOS = new Pedidos();
const CLIENTES = new Clientes();
const AGENTE = new Agente();

document.addEventListener('DOMContentLoaded', () => { init() });
document.querySelector('#btn-delete').addEventListener('click', () => {   

    // This is delete confirmated in the modal and remove row of the table
    PEDIDOS.deleteItem();
    PEDIDOS.getSubTotal();

});
document.querySelector('#list-Codes').addEventListener('change', () => getInfoProducto('code'));
document.querySelector('#descripcion-Codes').addEventListener('change', () => getInfoProducto('description'));
document.querySelector('#list-prices').addEventListener('change', () => PRICESLIST.setPrice());
document.querySelector('#client').addEventListener('change', () => CLIENTES.handleCliente());
document.querySelector('#add-new-partida').addEventListener('click', () => PEDIDOS.addPartida());
document.querySelector('#precio-producto').addEventListener('focusout', () => PRICESLIST.formatterPrecioCorrectly('precio-producto'));
document.querySelector('#precio-corte').addEventListener('focusout', () => PRICESLIST.formatterPrecioCorrectly('precio-corte'));
document.querySelector('#add-new-pedido').addEventListener('click', () => PEDIDOS.createPedido());

document.querySelector('[name=almacen]').addEventListener('change', () => ALMACEN.validarStock());
document.querySelector('#medidas-placas').addEventListener('change', () => PRICESLIST.getPriceByPlaca());

async function init() {
 
    // All things that needs to load at the beginning
    SPINNER.showSpinner();
    ITEM.init();
    AGENTE.getAgentes();
    CLIENTES.setClientes('');
    PRICESLIST.setPriceLists();
    ALMACEN.getAlmacen();
    PEDIDOS.getNewIdPedido();
    document.querySelector('[name="fecha"]').value = moment().locale('es').format('MMMM Do YYYY');  
    setTimeout(() => SPINNER.hideSpinner(), 1000);
}



async function getInfoProducto(token) {
    
    const CODE_SELECTED = document.querySelector('#list-Codes').value;
    const DESCRIPTION = document.querySelector('#descripcion-Codes').value;
    let value = DESCRIPTION;
    token === 'code' ?  value = CODE_SELECTED : value;

    const DATA = await ITEM.getItems(token, value);
    if (DATA.length) {
        ITEM.setInfoItem(token, DATA[0]);
        PRICESLIST.setPrice();
        PRICESLIST.setCutPrice();
        ITEM.choseCategory(DATA[0]);
    }
    
    
}


