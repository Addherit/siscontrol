
`use strict`

import {Formatter} from "../../../../assets/js/plugins/currency-format.js";

export class Precios {

    getListasPrecios() {

        // esto obtendrá la info para rellenar los datalist del modal add new item
        return fetch('php/select-lista-precios.php')
        .then(data => data.json())
        .then(data => {
          return data;
        })

    }
    async setPriceLists(){

        //funcion para llenar el datalist del modal de lista precios
        const DATA = await this.getListasPrecios(); 
        let options = '';       
        DATA.forEach(list => {
        options += `<option value="${list.ID}" ${list.ID == 1 ? 'selected' : ''}>Lista de precios: "${list.Nombre}"</option>`
        });
        document.querySelector('#list-prices').innerHTML = options;
                
    }

    getPrecio(list_price, item_code) {

        // esto obtendrá el precio del producto con base a la lista de precios seleccionada
        const FORM_DATA = new FormData()
        FORM_DATA.append('list_price', list_price);
        FORM_DATA.append('item_code', item_code);

        return fetch('php/select-precio.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
          return data;
        })

    }

  async setPrice() {

      const FORMATTER = new Formatter();
      
      const LISTA_PRECIOS_SELECTED = document.querySelector('#list-prices').value;
      const CODE_SELECTED = document.querySelector('#list-Codes').value;
      const INPUT_PRECIO = document.querySelector('#precio-producto');

      if (LISTA_PRECIOS_SELECTED) {

          const PRICE = await this.getPrecio(LISTA_PRECIOS_SELECTED, CODE_SELECTED);
          if (PRICE.length) {
              INPUT_PRECIO.value = `$ ${FORMATTER.currencyFormat(PRICE[0].Precio).toFixed(2)}`;
          } else {
              INPUT_PRECIO.value = `$ 0.00`;
              const msn = new Message('warning', 'No hay precio para este producto en esta lista');
              msn.showMessage();
          }

      } else {
          const msn = new Message('info', 'Elige una lista de precios');
          msn.showMessage();
      }
  
  }

    getPrecioCorte(item_code) {

        // esto obtendrá el precio de corte según el producto que se haya seleccionado
        const FORM_DATA = new FormData()        
        FORM_DATA.append('item_code', item_code);
        return fetch('php/select-precio-corte.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
          return data;
        })

    }

    async setCutPrice() {

      const FORMATTER = new Formatter();
      const CODE_SELECTED = document.querySelector('#list-Codes').value;
      const PRICE_CUT_DATA = await this.getPrecioCorte(CODE_SELECTED);
      let precio_corte = `$ 0.00`;
      PRICE_CUT_DATA[0] ? precio_corte = `$ ${FORMATTER.currencyFormat(PRICE_CUT_DATA[0].Precio).toFixed(2)}` : precio_corte;
      document.querySelector('#precio-corte').value = precio_corte;
  
  }

    getPriceByPlaca() {

       // esto obtendrá el precio del producto con base a la lista de precios seleccionada
       const FORM_DATA = new FormData()
       FORM_DATA.append('list_price', document.querySelector('#list-prices').value);
       FORM_DATA.append('item_code', document.querySelector('#list-Codes').value);
       FORM_DATA.append('placa', document.querySelector('#medidas-placas').value);

       return fetch('php/select-precio.php', { method: 'POST', body: FORM_DATA })
       .then(data => data.json())
       .then(data => {
          const FORMATTER = new Formatter();
          document.querySelector('#precio-producto').value = `$ ${FORMATTER.currencyFormat(data[0].Precio)}`;
       })

    }

    formatterPrecioCorrectly(id_name) {
        
        const ELEMENT_DOM = document.querySelector(`#${id_name}`);
        const FORMATTER = new Formatter();
        const PRECIO = ELEMENT_DOM.value;
        const REGEX = /[+-]?\d+(\.\d+)?/g;
        let precioFormater = PRECIO.match(REGEX)[0];    
    
        ELEMENT_DOM.value = `$ ${FORMATTER.currencyFormat(precioFormater)}`;
    
    }
}



// para poder saber que precio de corte tiene este Producto, como matchear el id con el precio de corte?
// como saber si ese producto se obtiene peso teoirico ?



// validar flujo del pedido 

//modulo de ventas.


