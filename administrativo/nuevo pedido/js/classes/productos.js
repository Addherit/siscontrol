`use strict`

import {Formatter} from "../../../../assets/js/plugins/currency-format.js";

export class Productos {

    

    init() {

      this.setListICodeItems('');
      this.setListIDescriptionItems('');

    }
    
    getItems(token, value) {

      // esto obtendrá la info para rellenar los datalist del modal add new item
      const FORM_DATA = new FormData()
      const DATA = {
        code: {item_code: value, description: ''},
        description: {item_code: '', description: value},
        general: {item_code: '', description: ''}
      }

      FORM_DATA.append('item_code', DATA[token].item_code);
      FORM_DATA.append('description', DATA[token].description);
      
      return fetch('php/select-items.php', { method: 'POST', body: FORM_DATA })
      .then(data => data.json())
      .then(data => {
        return data;
      })

    }
   
    async setListICodeItems(value) {

      //funcion para llenar el datalist del modal de code 
      const DATA = await this.getItems('general', value);
      let options = ``
      DATA.forEach(item => {
        options += `<option value="${item.Clave_Prod}">`
      });
      document.querySelector('#datalistCodes').innerHTML = options;

    }

    async setListIDescriptionItems(value) {

      //funcion para llenar el datalist del modal de descripcion
      const DATA = await this.getItems('general', value);
      let options = ``
      DATA.forEach(item => {
        options += `<option value="${item.Descripcion}">`
      });
      document.querySelector('#datalistDescription').innerHTML = options;

    }

   

    async setInfoItem(token, data) {

        const VALUES = {
          code: {element: "#descripcion-Codes", value: data.Descripcion},
          description: {element: "#list-Codes", value: data.Clave_Prod}
        }
               
        document.querySelector(VALUES[token].element).value = VALUES[token].value;        
        document.querySelector('#unidad').value = data.Unidad;
        document.querySelector('#id-Code').value = data.ID_Producto;  
        //unidad
        document.querySelector('#unidad').disabled = true;
    }

    seetInfoCategory(data, category) {

      switch (category) {

        case 'azul':

        break;
        case 'naranja':

        break;
        case 'pistache':

        break;
        case 'negro':

        break;
        case 'blanco':

          localStorage.setItem('pesoxmetro', data.Peso ? data.Peso : false);
          if (data.Medida) {

            document.querySelector('#diametro-externo').value = this.convertFraccionToDecimal(data.Medida.split(' X ')[0]);
            document.querySelector('#diametro-interno').value = this.convertFraccionToDecimal(data.Medida.split(' X ')[1]);

          }          
          
        break;   
        case 'amarillo':
          
        break;   
        case 'morado':
          
        break;   
        case 'rey':
          
        break;   
        case 'gris':
          
        break; 
        case 'verde':
          
        break;   
        case 'placany':
          document.querySelector('#espesor').value = data.MedidaPulg;
        break;
      }      

    }

    convertFraccionToDecimal(fraccion) {
      
      let FRACCION = fraccion;
      fraccion.indexOf('"') > 0 ? FRACCION = fraccion.replace('"', '') : FRACCION;    

      if (FRACCION.indexOf('/') > 0) {

        const NUMERADOR = FRACCION.split('/')[0]
        const DENOMINADOR = FRACCION.split('/')[1]
        if (NUMERADOR.indexOf(' ') > 0) {

          const ENTERO = NUMERADOR.split(' ')[0];
          const NUME = NUMERADOR.split(' ')[1];
          return parseInt(NUME)/parseInt(DENOMINADOR) + parseInt(ENTERO);
        } else {
          return parseInt(NUMERADOR)/parseInt(DENOMINADOR);
        }

      } else {

        return FRACCION

      }

    }

    async getCategoria(item) {

      const FORM_DATA = new FormData()
      FORM_DATA.append('item_code', item);
      const DATA = await fetch('php/select-categoria.php', { method: 'POST', body: FORM_DATA });
      return DATA.json();

    }

    async choseCategory(data) {
      
      this.cleanInputsCategory();
      let formula = 'default';
      const CATEGORIA = await this.getCategoria(data.Clave_Prod);
      CATEGORIA.length ? formula = CATEGORIA[0].Formula : formula;

      const CATEGORIAS = {
        azul: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        naranja: ['espesor', 'ancho', 'variante-fija', 'largo', 'peso-teorico'],
        pistache: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        negro: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        blanco: ['diametro-externo', 'diametro-interno', 'largo', 'peso-teorico'],
        amarillo: ['espesor', 'ancho', 'variante-fija', 'largo', 'peso-teorico'],
        morado: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        rey: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        gris: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        verde: ['diametro-externo', 'diametro-interno', 'variante-fija', 'largo', 'peso-teorico'],
        //nuevas
        cafe: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        rosa: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
        aqua: ['espesor', 'ancho', 'variante-fija', 'largo', 'peso-teorico'],
        //placas
        placany: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
        rojo: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
        aquaplaca: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
        amarilloplaca: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
        default: []
      };        
      this.showInputsCategory(CATEGORIAS[formula], formula);
      this.seetInfoCategory(data, formula);

    }   

    showInputsCategory(formula, formula_name) {

      if (formula.length) {

        localStorage.setItem('formula_categoria', formula_name);
        document.querySelector('#cantidad').addEventListener('keyup', () => this.applyFormule());

        formula.forEach(input => {
          const ELEMENT_DOM = document.querySelector(`.${input}`);
          ELEMENT_DOM.hidden = false;
          ELEMENT_DOM.addEventListener('keyup', () => this.applyFormule());
        });
        
      } else {
        const msn = new Message('info', 'Producto sin categoria');
        msn.showMessage();
      }   

    }

    cleanInputsCategory() {
            
      const INPUTS = ['diametro', 'variante-fija', 'largo', 'espesor', 'ancho', 'diametro-externo', 'diametro-interno', 'peso-teorico']
      INPUTS.forEach(input => {
        document.querySelector(`.${input}`).hidden = true;
      })

    }

    applyFormule() {
      
      //afeccion de los nuevos largos en las formulas 
      const NAME_FORM = localStorage.getItem('formula_categoria');    

      switch (NAME_FORM) {       
        case 'azul':
          this.methodAzul();
        break;
        case 'naranja':
          this.methodNaranja();
        break;
        case 'pistache':
          this.methodPistache();
        break;
        case 'negro':
          this.methodNegro();
        break;
        case 'blanco':
          this.methodBlanco();          
        break;   
        case 'amarillo':
          this.methodAmarillo();
        break;   
        case 'morado':
          this.methodMorado();
        break;   
        case 'rey':
          this.methodRey();
        break;   
        case 'gris':
          this.methodGris();
        break; 
        case 'verde':
          this.methodVerde();
        case 'cafe':
          this.methodCafe();
        break;                      
        case 'rosa':
          this.methodRosa();
        break;                      
        case 'aqua':
          this.methodAqua();
        break;                      
        case 'placany':
          this.methodny();
        break;                      
      }            

    }

    methodAzul() {

      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 4 (VARIANTE FIJA) X LARGO (EN METROS)	
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 4;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }
    
    methodNaranja() {

      // ESPESOR(PULGADAS)  X ANCHO(PULGADAS) X  5 (VARIANTE FIJA) X (LARGO EN METROS)		
      const ESPESOR = document.querySelector("[name=espesor]").value;
      const ANCHO = document.querySelector("[name=ancho]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 5;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = ESPESOR * ANCHO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodPistache() {

      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 5 (VARIANTE FIJA) X LARGO (EN METROS)
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 5;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodNegro() {

      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 1.4 (VARIANTE FIJA) X LARGO (EN METROS)	
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 1.4;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodBlanco() {

      // Se utilizará la formula de pesoxmetro * largo(metros) * CANTIDAD  
      const LARGO = parseFloat(document.querySelector("[name=largo]").value);
      const CANTIDAD = parseInt(document.querySelector('#cantidad').value);
      const RESULTADO = this.pulgadasAmetros(LARGO) * parseFloat(localStorage.getItem('pesoxmetro')) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodAmarillo() {

      // ESPESOR(PULGADAS) X ANCHO(PULGADAS) X 1.8 (VARIANTE FIJA) X (LARGO EN METROS)					
      const ESPESOR = document.querySelector("[name=espesor]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 1.8;
      const ANCHO = document.querySelector("[name=ancho]").value;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = ESPESOR * ANCHO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodMorado() {

      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  1.8 (VARIANTE FIJA) X (LARGO EN METROS)
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 1.8;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);	

    }

    methodRey() {

      // DIAMETRO(PULGADAS) X DIAMETRO(PULGADAS) X 4.5 (VARIANTE FIJA) X LARGO (EN METROS)	
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 4.5;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);

    }

    methodGris() {

      // DIAMETRO(PULGADAS)  X DIAMETRO(PULGADAS) X  5.5 (VARIANTE FIJA) X (LARGO EN METROS)	
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 5.5;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = DIAMETRO * DIAMETRO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);					
    }

    methodVerde() {

      // DIAMETRO EXTERIOR (PULGADAS) AL CUADRADO -  DIAMETRO INTERIOR (PULGADAS) AL CUADRADO X 5 (VARIANTE FIJA) X LARGO (EN METROS)
      const DIAMETRO_INTERIOR = document.querySelector("[name=diametro-interno]").value;
      const DIAMETRO_EXTERNO = document.querySelector("[name=diametro-externo]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 5;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = Math.pow(DIAMETRO_INTERIOR, 2) * Math.pow(DIAMETRO_EXTERNO, 2) * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);
      
    }
    
    methodCafe() {

      //(diámetro al cuadrado * 4.2)(largo en metros)
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 4.2;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = (Math.pow(DIAMETRO, 2) * Math.pow(DIAMETRO, 2)) * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);
      
    }

    methodRosa() {

      //(medida * 48)(1.8 variante fija)(largo que entrada)// puede que este mal
      const DIAMETRO = document.querySelector("[name=diametro]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 1.8;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = (Math.pow(DIAMETRO, 2) * 48) * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);
      
    }
    methodAqua() {

      //ESPESOR(PULGADAS)  X ANCHO(PULGADAS) X  5.2 (VARIANTE FIJA) X (LARGO EN METROS)
      const ESPESOR = document.querySelector("[name=espesor]").value;
      const VARIANTE_FIJA = document.querySelector("[name=variante-fija]").value = 5.2;
      const ANCHO = document.querySelector("[name=ancho]").value;
      const LARGO = document.querySelector("[name=largo]").value;
      const CANTIDAD = document.querySelector('#cantidad').value;
      const RESULTADO = ESPESOR * ANCHO * VARIANTE_FIJA * this.pulgadasAmetros(LARGO) * CANTIDAD;
      document.querySelector('#peso-teorico').value = RESULTADO.toFixed(2);
      
    }
    methodny() {

      // Ancho * Largo * Espesor * Precio
      //obtencion de las variables
      const ESPESOR = document.querySelector("[name=espesor]").value;
      const ANCHO = document.querySelector("[name=ancho]").value;
      const LARGO = document.querySelector("[name=largo]").value;
      const VOLUMEN = LARGO * ANCHO * ESPESOR;

      //calculado de variante
      //const VARIANTE_FIJA = 0.497685185; 
      const VARIANTE_FIJA = this.Varianteny(ESPESOR);
      console.log(VARIANTE_FIJA);

      //Calculado total del precio
      const RESULTADO = VOLUMEN * VARIANTE_FIJA;

      //formatear
      var v = RESULTADO.toFixed(2);
      if(v == 0.0){
        v = i;
      }else{
        v = '$ '+ v; 
      }
      //ingreso
      document.querySelector('#precio-producto').value = v;

      
    }

    pulgadasAmetros(largo) {

      // agregar el nuevo esquema
      if(document.querySelector('#unidad-medida').value == 'in') {
        const UNIDAD = document.querySelector('#unidad-medida').value;
        let response = 0;
        UNIDAD == 'in' ? response = largo * 0.0254 : response = largo;
        return response;

      } else if(document.querySelector('#unidad-medida').value == 'mm'){
        const UNIDAD = document.querySelector('#unidad-medida').value;
        let response = 0;
        UNIDAD == 'mm' ? response = largo / 1000.00 : response = largo;
        return response;

      }else if(document.querySelector('#unidad-medida').value == 'cm'){
        const UNIDAD = document.querySelector('#unidad-medida').value;
        let response = 0;
        UNIDAD == 'cm' ? response = largo / 100.00 : response = largo;
        return response;

      }else{
        return largo;
      }
      
    }
    Varianteny(Espesor) {
      console.log("e:"+Espesor);
      var r = 0.0;
      switch (Espesor) {
        case '0.25':
          r = 0.600694444; 
          break;
        case '0.375':
          r = 0.497685185; 
          break;
        case '0.5':
          r = 0.469444444; 
          break;
        case '0.625':
          r = 0.450555556; 
          break;
        case '0.75':
          r = 0.437847222; 
          break;

        case '1':
          r = 0.422916666; 
          break;
        case '1.25':
          r = 0.428402777; 
          break;
        case '1.5':
          r = 0.419444444; 
          break;
        case '1.75':
          r = 0.433134920; 
          break;
        case '2':
          r = 0.40907118; 
          break;
        case '2.25':
          r = 0.4549768; 
          break;
        case '2.5':
          r = 0.445590277; 
          break;
        case '3':
          r = 0.44016203; 
          break;
        case '3.25':
          r = 0.43568376; 
          break;
        case '3.5':
          r = 0.4339781; 
          break;
        case '4':
          r = 0.4495876; 
          break;
      }
      return r;
    }

}