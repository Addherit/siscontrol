import { Formatter } from "../../../../assets/js/plugins/currency-format.js";

export class Pedidos {

    constructor () {
        this.rowToDelete = ``;
        this.increCorte = 0;
    }


    async getNewIdPedido() {

        // genera un id consecutivo relativo al ultimo encontrado
        const FORM_DATA = new FormData()
        FORM_DATA.append('client', client);
        const DATA = await fetch('php/select-pedidos.php', { method: 'POST', body: FORM_DATA });
        const LAST_PEDIDO = await DATA.json();        
        const CONSECUTIVO = parseInt(LAST_PEDIDO[0].ID_Venta) + 1;
        document.querySelector('[name="pedido-id"]').value = CONSECUTIVO;

    }

    putPedidos(pedido) {

        const FORM_DATA = new FormData();
        FORM_DATA.append('pedido', JSON.stringify(pedido));
        return fetch('php/insert-pedido.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            const msn = new Message(data.type, data.message);
            msn.showMessage();
            if (data.type === 'success') {
                setTimeout(() => {
                    window.location = `/siscontrol/administrativo/detalle pedido/index.php?idPedido=${data.id}`;
                }, 2000);
            }        
        })
    }

    getBodyPedido() {

        let body = [];
        const FORMATTER = new Formatter();
        const TRS = document.querySelectorAll('#tbl-new-sale tbody tr');
        TRS.forEach(tr => {
            let concept = {};
            const TDS = tr.children
            for (const key in TDS) {                               
                const CLASES = TDS[key].classList;
                for (const x in CLASES) {
                                                            
                    // Get all concept's values by className from td cells
                    if (CLASES[x] == 'td-code') concept = {...concept, clave: TDS[key].textContent, almacen: TDS[key].dataset.almacen};
                    if (CLASES[x] == 'td-description') concept = {...concept, descripcion: TDS[key].textContent, lista_precio: TDS[key].dataset.pricelist};
                    if (CLASES[x] == 'td-unidad') concept = {...concept, unidad: TDS[key].textContent, peso_teorico: TDS[key].dataset.pesoteorico, ltsPzas: TDS[key].dataset.ltspzas};
                    if (CLASES[x] == 'td-cant') concept = {...concept, cantidad: TDS[key].textContent, cortes: TDS[key].dataset.cortes};
                    if (CLASES[x] == 'td-price') concept = {...concept, precio: FORMATTER.numberFormat(TDS[key].textContent.split(' ')[1]), precio_corte: TDS[key].dataset.preciocorte};
                    if (CLASES[x] == 'importe') concept = {...concept, importe: FORMATTER.numberFormat(TDS[key].textContent.split(' ')[1])};                
                    if (CLASES[x] == 'td-cant') concept = {...concept, peso_real: TDS[key].id};
                    if (CLASES[x] == 'td-comments') {                         

                        concept = {...concept, 
                            observaciones: TDS[key].textContent, 
                            ancho: TDS[key].dataset.ancho,  
                            largo: TDS[key].dataset.largo,  
                            espesor: TDS[key].dataset.espesor,  
                            varianteFija: TDS[key].dataset.variantefija,  
                            diametro: TDS[key].dataset.diametro, 
                            diametro_int: TDS[key].dataset.diint,  
                            diametro_ext: TDS[key].dataset.diext,  
                            medida_placa: TDS[key].dataset.medidaplaca, 
                            u_medida: TDS[key].dataset.unidadmedia,
                            formula: TDS[key].dataset.formula,
                            ncorte: TDS[key].dataset.ncorte,
                        };
                    }
                    if (CLASES[x] == 'observaciones') concept = {...concept, ncorte: TDS[key].dataset.ncorte};
                }
            }
            body.push(concept);
        });
        return body;
    }

    createPedido() {
    
        const FORMATTER = new Formatter();        
        const CLIENT = document.querySelector('#client').value;
        const AGENT = document.querySelector('[name=agente]').value;
        const BODY = this.getBodyPedido();
        if (CLIENT && AGENT && BODY.length > 0) {
            let pedido = {
                header: {
                    cliente: CLIENT,
                    email: document.querySelector('[name=email]').value,
                    agente: AGENT,
                    pedido: document.querySelector('[name=pedido-id]').value,
                    estatus: document.querySelector('[name=estatus').value,
                    subtotal: FORMATTER.numberFormat(document.querySelector('[name=subtotal]').value.split(' ')[1]),
                    iva: FORMATTER.numberFormat(document.querySelector('[name=iva]').value.split(' ')[1]),
                    total: FORMATTER.numberFormat(document.querySelector('[name=total]').value.split(' ')[1]),
                },
                body: BODY
            }
            this.putPedidos(pedido);
        } else {
            const MSN = new Message('danger', 'Falta agregar un cliente, agente y partidas al pedido')
            MSN.showMessage()
        }
        
    }

    addPartida() {        

        const FORMATTER = new Formatter();
        const CODE = document.querySelector('#list-Codes').value;
        const DESCRIPTION = document.querySelector('#descripcion-Codes').value;
        const UNIDAD = document.querySelector('#unidad').value.toLowerCase();
        const PESO_TEORICO = document.querySelector('#peso-teorico').value;
        const LTSPZAS = document.querySelector('#cantidad').value;
        let ALMACEN = document.querySelector('#almcen').value;

        if (!ALMACEN){
            console.log("323");
            ALMACEN = 323;
        }    
        // could be both ----------
        const CANTIDAD = UNIDAD.toLowerCase() === 'kg' ? PESO_TEORICO : LTSPZAS;
        // ------------------------
     
        const PRICE_LIST = document.querySelector('#list-prices').options[document.querySelector('#list-prices').value].textContent;
        const PRECIO_PRODUCTO = FORMATTER.numberFormat(document.querySelector('#precio-producto').value.split(' ')[1]);
        const IMPORTE = PRECIO_PRODUCTO * CANTIDAD;
        const OBSERVACIONES = document.querySelector('#exampleFormControlTextarea1').value;
    
        // Only if has cut services ---------------------
        const CORTES = document.querySelector('#cortes').value ? document.querySelector('#cortes').value : 0;
        const PRECIO_CORTE = document.querySelector('#precio-corte').value ? FORMATTER.numberFormat(document.querySelector('#precio-corte').value.split(' ')[1]) : 0.00;
        const IMPORTE_CORTE = PRECIO_CORTE * CORTES;
        // ---------------------------
    
        // INPUTS FORMULA --------------------------------       
        
        const VALUES = {
            largo: document.querySelector('#largo').value ? document.querySelector('#largo').value : 0,
            ancho: document.querySelector('#ancho').value ? document.querySelector('#ancho').value : 0,
            espesor: document.querySelector('#espesor').value ? document.querySelector('#espesor').value : 0,
            variante_fija: document.querySelector('#variante-fija').value ? document.querySelector('#variante-fija').value : 0,
            diametro: document.querySelector('#diametro').value ? document.querySelector('#diametro').value : 0,
            diametro_ext: document.querySelector('#diametro-externo').value ? document.querySelector('#diametro-externo').value : 0,
            diametro_int: document.querySelector('#diametro-interno').value ? document.querySelector('#diametro-interno').value : 0,
            medida_placa: document.querySelector('#medidas-placas').value ? document.querySelector('#medidas-placas').value : 0,
            unidad_medida: document.querySelector('#unidad-medida').value ? document.querySelector('#unidad-medida').value : 0,
            
        }
    
        const FORMULA = localStorage.getItem('formula_categoria');
        const DESCRIPCION_EXTRA = this.getDescriptionExtra(FORMULA, VALUES, LTSPZAS);
        // -----------------------------------------------
    
        if (PRECIO_PRODUCTO) {
            console.log(ALMACEN);
            if (ALMACEN) {
                $('#modal-add-item').modal('hide');
                if(CORTES){ // new id of cortes
                    this.increCorte++;
                }
                $('#tbl-new-sale tbody').append(
                    `<tr>
                        <td class="text-nowrap align-middle text-center">
                            <button type="button" class="btn btn-sm btn-danger btn-delete" title="Eliminar partida" data-toggle="modal" data-target="#modal-delete-confirmation">
                                <span class="material-icons align-middle">delete</span>
                                Eliminar
                            </button>
                        </td> 
                        <td class="text-nowrap align-middle td-code" data-almacen="${ALMACEN}">${CODE}</td>
                        <td class="text-nowrap align-middle td-description" data-pricelist="${PRICE_LIST}">${DESCRIPTION} ${DESCRIPCION_EXTRA}</td>
                        <td class="text-nowrap align-middle td-unidad" data-pesoteorico="${PESO_TEORICO}" data-ltspzas="${LTSPZAS}">${UNIDAD}</td>
                        <td class="text-nowrap align-middle td-cant" data-cortes="${CORTES}">${CANTIDAD}</td>                          
                        <td class="text-nowrap align-middle td-price" data-preciocorte="${PRECIO_CORTE}">$ ${FORMATTER.currencyFormat(PRECIO_PRODUCTO)}</td>
                        <td class="text-nowrap align-middle importe">$ ${FORMATTER.currencyFormat(IMPORTE)}</td>
                        <td class="text-nowrap align-middle td-comments" 
                            data-ancho="${VALUES.ancho}" 
                            data-largo="${VALUES.largo}" 
                            data-espesor="${VALUES.espesor}" 
                            data-variantefija="${VALUES.variante_fija}" data-diametro="${VALUES.diametro}"
                            data-diint="${VALUES.diametro_int}" 
                            data-diext="${VALUES.diametro_ext}" 
                            data-medidaplaca='${VALUES.medida_placa}' 
                            data-unidadmedida="${VALUES.medida_placa}" 
                            data-formula="${FORMULA}"                             
                            data-ncorte="${((CORTES) ? this.increCorte : 0)}"                             
                        >${OBSERVACIONES}</td>
                    </tr>`
                )
                if(CORTES) {
                    $('#tbl-new-sale tbody').append( 
                        `<tr>               
                            <td></td>        
                            <td class="td-code">s0</td>
                            <td class="text-nowrap align-middle td-description">Servicio de corte</td>
                            <td class="td-unidad"></td>
                            <td class="text-nowrap align-middle td-cant" id="">${CORTES}</td>                          
                            <td class="text-nowrap align-middle td-price">$ ${FORMATTER.currencyFormat(PRECIO_CORTE)}</td>
                            <td class="text-nowrap align-middle importe">$ ${FORMATTER.currencyFormat(IMPORTE_CORTE)}</td>
                            <td class="observaciones" data-ncorte="${this.increCorte}"></td>
                        </tr>`
                    )
                }
                document.querySelectorAll('.btn-delete').forEach(button => this.activateDeleteConfirm(button));
                document.getElementById("form-add-new-item").reset();
                this.getSubTotal();
    
            } else {
                const msn = new Message('warning', 'Falta seleccionar un almacén');
                msn.showMessage();
            }
        } else {
            const msn = new Message('warning', 'Falta agregar un precio');
            msn.showMessage();
        }
        
    }

    activateDeleteConfirm(button) {

        //This is ti activate event click over each buttom delete
        button.addEventListener('click', (event) => {
            let claveItem = ``;
            const ROW = event.currentTarget.parentElement.parentElement;
            const TDS = ROW.children;
            for (const key in TDS) {
                const CLASES = TDS[key].classList;
                for (const x in CLASES) {
                    if (CLASES[x] == 'td-description') {
                        claveItem = TDS[key].textContent
                    }
                }        
            }                            
            
            document.querySelector("#delete-name").innerHTML = "el producto";
            document.querySelector("#id-to-delete").innerHTML = claveItem;
            this.rowToDelete = ROW;
        })
        
    }

    getDescriptionExtra(formula, values, cantidad) {
        
        let description = '';
        switch (formula) {
           
            case 'azul':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;
            case 'naranja':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;
            case 'pistache':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;
            case 'negro':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;
            case 'blanco':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;   
            case 'amarillo':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;   
            case 'morado':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;   
            case 'rey':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;   
            case 'gris':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break; 
            case 'verde':
                description = `L: ${values.largo}${values.unidad_medida} Pza:${cantidad}`;
                break;                
            case 'rojo':
                description = `x ${values.largo}" x ${values.ancho}"  Pza: ${cantidad} `;
                break; 
            case 'acuaplaca':
                description = `x ${values.largo}" x ${values.ancho}"  Pza: ${cantidad} `;
                break; 
            case 'amarilloplaca':
                description = `x ${values.largo}" x ${values.ancho}"  Pza: ${cantidad} `;
                break; 
            case 'rosa':
                description = `L: ${values.largo}${values.unidad_medida} Pza: ${cantidad} Ancho: ${values.ancho} Espesor: ${values.espesor}`;
                break; 
            case 'placany':
                description = `x ${values.largo}" x ${values.ancho}"  Pza: ${cantidad} `;
                break;

            default:
                description;
                break;

        }

        return description;

    }

    deleteItem() {
        
        // validate if has a rows of cut services 
        const SIBLING = this.rowToDelete.nextSibling
        if (SIBLING != null) {
            const TDS = SIBLING.children
            for (const key in TDS) {
                const CLASES = TDS[key].classList;
                for (const x in CLASES) {
                    if (CLASES[x] == 'td-description') {
                        if (TDS[key].textContent === 'Servicio de corte') SIBLING.remove();   
                    }
                }        
            }
        }
        this.rowToDelete.remove();
        $('#modal-delete-confirmation').modal('toggle');
              
      }
    
    getSubTotal() {

    const FORMATTER = new Formatter();
    const IMPORTES = document.querySelectorAll('#tbl-new-sale tbody tr .importe');    
    let subtotal = 0;

    if (IMPORTES)
        IMPORTES.forEach(importe => subtotal = subtotal + parseFloat(FORMATTER.numberFormat(importe.textContent.split(' ')[1])));

    document.querySelector('[name=subtotal]').value = `$ ${FORMATTER.currencyFormat(subtotal.toFixed(2))}`;
    this.getIVA(subtotal);

}

    getIVA(subtotal) {

    // get IVA by each importe concept
    const FORMATTER = new Formatter();
    const IMPORTES = document.querySelectorAll('#tbl-new-sale tbody tr .importe');    
    let iva = 0;

    if (IMPORTES)
        IMPORTES.forEach(importe => iva = iva + (parseFloat(FORMATTER.numberFormat(importe.textContent.split(' ')[1])) * .16 ));

    document.querySelector('[name=iva]').value = `$ ${FORMATTER.currencyFormat(iva.toFixed(2))}`;
    this.getTotal(subtotal, iva);

}

    getTotal(subtotal, iva) {
    
    const FORMATTER = new Formatter();
    let total = parseFloat(subtotal) + parseFloat(iva);
    document.querySelector('[name=total]').value = `$ ${FORMATTER.currencyFormat(total.toFixed(2))}`;

}


}