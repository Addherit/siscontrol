export class Almacen {

    async getAlmacen() {

        const DATA = await fetch('php/select-almacenes.php');
        const ALMACENES = await DATA.json();
        let option = `<option value=""  selected>Selecciona uno</option>`;
        ALMACENES.forEach(almacen => {
            option += `<option value="${almacen.Id}">${almacen.Nombre}</option>`;
        })
    
        document.querySelector('[name=almacen]').innerHTML = option;

    }

    async validarStock() {
        
        const FORM_DATA = new FormData();
        FORM_DATA.append('id-producto', document.querySelector('#id-Code').value);
        FORM_DATA.append('id-almacen', document.querySelector('[name=almacen]').value)
        const DATA = await fetch('php/select-stock.php',{ method: 'POST', body: FORM_DATA});
        const STOCK = await DATA.json();
        const btn_add_producto = document.querySelector('#add-new-partida');
        
        if(!STOCK.length) {
            btn_add_producto.disabled = true;
            document.querySelector('#medidas-placas').value = '';
            const msn = new Message('warning', 'Almacen sin stock, intente con otro');
            msn.showMessage();
        } else {               
            btn_add_producto.disabled = false;
        } 
        this.setMedidasPlacas(STOCK);

    }

    setMedidasPlacas(data) {

        let option = `<option value="" selected disabled>Selecciona una</option>`;
        data.forEach(medida => {
            option += `<option value='${medida.MedidaPlaca}'>${medida.MedidaPlaca}</option>`;
        })
        document.querySelector('#medidas-placas').innerHTML = option

    }
}