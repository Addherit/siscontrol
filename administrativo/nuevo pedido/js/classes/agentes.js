
export class Agente {



    getAgentes() {

        //get info of agente
        const ID_type = localStorage.getItem('id_user_type');
        const FORM_DATA = new FormData();
        if(ID_type != 1){
            let id = localStorage.getItem('user_id');
            FORM_DATA.append('id_agente', id);
        }else{
            FORM_DATA.append('id_agente', 0);
        }
        fetch('php/select-agentes.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            let options = `<option value="" disabled selected>Selecciona un agente</option>`;
            data.forEach(agente => {
                options += `<option value="${agente.ID_Agente}">${agente.Nom_Agente}</option>`;
            });

            document.querySelector('[name=agente]').innerHTML = options;
        })
    }
}