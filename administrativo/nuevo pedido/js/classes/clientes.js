export class Clientes {

    getClientes(client) {

        // esto obtendrá la info para rellenar los datalist del modal add new item
        const ID_type = localStorage.getItem('id_user_type');
        const FORM_DATA = new FormData();
        if(ID_type != 1){
            let id = localStorage.getItem('user_id');
            FORM_DATA.append('id_agente', id);
        }else{
            FORM_DATA.append('id_agente', 0);
        }
        FORM_DATA.append('client', client);
        return fetch('php/select-clientes.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
        return data;
        })
        
    }

    async setClientes(client) {

        //funcion para llenar el datalist de clientes
        const DATA = await this.getClientes(client);
        let options = ``
        DATA.forEach(item => {
            options += `<option value="${item.Nombre_Cte}">`
        });
        document.querySelector('#list-clients').innerHTML = options;

    }

    async handleCliente() {

        let CLIENTE = document.querySelector('#client').value;
        let DATA = await this.getClientes(CLIENTE);
        console.log(DATA);
        if(DATA.length != 0){
            if (CLIENTE) {
                //activar pedido cuando tengan cliente que exista
                document.querySelector('#add-new-pedido').disabled = false;
                this.setEmailClient(CLIENTE);
                this.validateLineaDeCredito(CLIENTE);
            }
        }else{
            //si sale el cliente no exite no dejar ingresar el pedido
            document.querySelector('#add-new-pedido').disabled = true;
            const msn = new Message('info', 'El Cliente No Existe');
            msn.showMessage();
        }
        
    }
    
    async setEmailClient(client) {

       
        const DATA = await this.getClientes(client);
        let email = DATA[0].Email1;
        email ? email : email = 'Sin correo electrónico';
        document.querySelector('[name=email]').value = email;
             
    
    }

    async validateLineaDeCredito(client) {

        const FORM_DATA = new FormData()        
        FORM_DATA.append('client', client);
        const DATA = await fetch('php/validador-credito.php', { method: 'POST', body: FORM_DATA });
        const CREDITO = await DATA.json();
    
        switch (CREDITO[0].State) {
            case 'Moroso':
                const msn = new Message('warning', 'Cliente en estatus Moroso, no se le puede vender');
                msn.showMessage();
                document.querySelector('#add-new-pedido').disabled = true;
                break;
            case 'Excepcion':
                const msn2 = new Message('info', 'Cliente en estatus Excepción');
                msn2.showMessage();
                document.querySelector('#add-new-pedido').disabled = false;
                break;
            default:
                document.querySelector('#add-new-pedido').disabled = false;
                break;
        }
        
    }
}