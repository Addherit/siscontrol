<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled"> 
            <?php include "../../assets/modales/modal-delete-confirmation.html" ?>       
            <?php include "./modales/modal-add-item.html"?> 
            <?php include "./modales/modal-detalle-partida.html"?> 
            <?php include "../../sidebar/index.html"?> 
            <?php include "../../assets/modales/modal-spinner.html" ?>
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">NUEVO PEDIDO</span>
                        </div>                        
                    </div>
                    <hr>                   
                    <form id="new-sale-note"> 
                        <div class="row">
                            <div class="col-md-8">                    
                                <div class="row">
                                    <label for="cliente" class="col-md-5 col-lg-2 col-form-label">Cliente</label>
                                    <div class="col-md-7 col-lg-9">                                        
                                        <input class="form-control form-control-sm" list="list-clients" id="client" name="client" placeholder="Elige un cliente">
                                        <datalist id="list-clients"></datalist>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-md-5 col-lg-2 col-form-label">E-mail</label>
                                    <div class="col-md-7 col-lg-9">
                                    <input type="text" class="form-control form-control-sm" name="email">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="agente" class="col-md-5 col-lg-2 col-form-label">Agente</label>
                                    <div class="col-md-7 col-lg-9">
                                    <select name="agente" class="form-control form-control-sm"></select>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <label for="id" class="col-md-5 col-lg-3 col-form-label">Pedido</label>
                                    <div class="col-md-7 col-lg-9">
                                    <input type="text" class="form-control form-control-sm text-center" name="pedido-id" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="fecha" class="col-md-5 col-lg-3 col-form-label">Fecha</label>
                                    <div class="col-md-7 col-lg-9">
                                    <input type="text" class="form-control form-control-sm text-center" name="fecha" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="estatus" class="col-md-5 col-lg-3 col-form-label">Estatus</label>
                                    <div class="col-md-7 col-lg-9">
                                    <input type="text" class="form-control form-control-sm text-center" name="estatus" value="COTIZACION" readonly>
                                    </div>
                                </div>                       
                            </div>
                        </div>                        
                        <div class="row pb-1">
                            <div class="col-12 text-right">                            
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-add-item"><span class="material-icons align-middle">add</span> Agregar Producto</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table id="tbl-new-sale" class="table table-hover table-sm table-dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>                                       
                                            <th>Clave</th>
                                            <th>Descripción</th>
                                            <th>Ud</th>
                                            <th>Cant</th>                                            
                                            <th>Precio</th>
                                            <th>Importe</th>
                                            <th>Observaciones</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-5 offset-7">
                                <div class="row">
                                    <label for="subtotal" class="col-sm-4 col-form-label">Subtotal</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="subtotal" readonly>
                                    </div>
                                </div> 
                                <div class="row">
                                    <label for="iva" class="col-sm-4 col-form-label">IVA 16%</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="iva" readonly>
                                    </div>
                                </div> 
                                <div class="row">
                                    <label for="total" class="col-sm-4 col-form-label">Total</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="total" readonly>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="button" class="btn btn-success" id="add-new-pedido">
                                    <span class="material-icons align-middle">done</span>
                                    Crear Pedido
                                </button>
                                <!-- <button class="btn btn-info">Facturar</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="/siscontrol/administrativo/nuevo pedido/js/main.js" type="module"></script>

      
     
</html>