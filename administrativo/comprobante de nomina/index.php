<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "modales/modal-comp-nomina.html" ?>
        <?php include "modales/modal-carga-comprobantes.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-md-9">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                              <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">COMPROBANTE DE NÓMINA</span>
                        </div>                      
                        <div class="col-md-3">
                          <input type="file" name="btn-upload-pdf" id="btn-upload-pdf" class="inputfile" multiple>
                          <label for="btn-upload-pdf"><span class="material-icons align-middle">picture_as_pdf</span> Cargar Nóminas</label>                         
                        </div>  
                    </div>
                    <hr>

                    <div class="row">
                      <div class="col-12 table-responsive">
                        <table id="list-sales-notes" class="table table-hover table-sm table-dataTable" width="100%">
                          <thead>
                            <tr>
                              <th></th>
                              <th>Id</th>
                              <th>Empleado</th>                              
                            </tr>
                          </thead>                            
                          <tbody></tbody>
                        </table>
                      </div>
                    </div>                                        
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>