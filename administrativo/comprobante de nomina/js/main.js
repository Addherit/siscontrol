import { Comprobante } from "./classes/comprobante.js";
import { Empleado } from "./classes/empleado.js";
const COMPROBANTE = new Comprobante();
const EMPLEADO = new Empleado();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-upload-pdf').addEventListener('change', () => COMPROBANTE.importComprobantes());
document.querySelector('#btn-download-file').addEventListener('click', () => COMPROBANTE.exportComprobante());

function init() {
    
    EMPLEADO.setEmpleados(); 
    getOptionsYear();

}

function getOptionsYear() {

    // funcion para rellenar select-year con el año en curso y sus 5 predecesores. 
    const YEAR = parseInt(moment().format('YYYY'))
    let option = '';
    for (let index = 0; index < 5; index++) {
        let valueYear = YEAR - index;
        option += `<option value="${valueYear}">${valueYear}</option>`;
    }

    document.querySelector('#select-year').innerHTML = option
    
}
