import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
export class Empleado {

    async getEmpleados() {
        
        const DATA = await fetch('php/select-empleados.php');
        return await DATA.json();

    }

    async setEmpleados() {

        let lineas = ``;
        const DATA = await this.getEmpleados();
        DATA.forEach(empleado => {
            lineas += `
                <tr>
                    <td class="text-nowrap align-middle text-center">
                        <button class="btn btn-sm btn-info btn-tbl-modal" title="Descargar comprobante de nómina" data-toggle="modal" data-target="#modal-comp-nomina">
                            <span class="material-icons align-middle">print</span> 
                            Descargar Nómina
                        </button>
                    </td>
                    <td class="align-middle">${empleado.Id}</td>
                    <td class="align-middle">${empleado.Nombre}</td>                                                 
                </tr>
            `;
        });
        document.querySelector('#list-sales-notes tbody').innerHTML = lineas;
        this.handleConfirmDownload();

        const dataTable = new DataTable('list-sales-notes');
        dataTable.initDataTable();

    }

    handleConfirmDownload() {

        const BTNS = document.querySelectorAll('.btn-tbl-modal');
        BTNS.forEach(btn => {
            btn.addEventListener('click', (event) => {
                const TR = event.currentTarget.parentElement.parentElement;
                document.querySelector('#id-comp-empleado').value = TR.children[1].textContent;                
            })
        })

    }

}