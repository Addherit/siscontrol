export class Comprobante {

    async importComprobantes() {

        for (let key = 0; key < document.querySelector('#btn-upload-pdf').files.length; key++) {

            const FILE = document.querySelector('#btn-upload-pdf').files[key];
            const TMP_NAME = await this.copyPDFTmp(FILE);
            let message = ``;
            let type =``;
            if (TMP_NAME) {
                message = `Se subió correctamente el archivo PDF`;
                type = 'success';
            } else {
                message = `Error al subir el archivo PDF`;
                type = 'danger';
            }
            
            const MSN = new Message(type, message);
            MSN.showMessage();
        }

    }

    async exportComprobante() {
        
        const ID_EMPLEADO = document.querySelector('#id-comp-empleado').value;
        const PERIODO = document.querySelector('#select-periodo').value;
        const MES = document.querySelector('#select-mes').value;
        const YEAR = document.querySelector('#select-year').value;
        const FILE_NAME = `${PERIODO}-${ID_EMPLEADO}.pdf`;
        const URL = `https://farguaceros.com/siscontrol/TempFiles/NOMINAS/${YEAR}/${MES}/${FILE_NAME}`;        
        const FILE_EXIST = await fetch(URL);

        if (FILE_EXIST.status == 200) {
            window.open(URL, '_blank');   
            $('#modal-comp-nomina').modal('hide');
        } else {
            const MSN = new Message('warning', 'No se pudo encontrar la nómina solicitada');
            MSN.showMessage();
        }
        

    }

    copyPDFTmp(file) {

        const FORM_DATA = new FormData()
        FORM_DATA.append('file', file);

        //Create file tmp in dir tmpxml and return the tmp name
        return fetch('php/copy-tmp-pdf.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => { 
           return data.tmp_name;
        })

    }    

}