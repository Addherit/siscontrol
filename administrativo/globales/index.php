<?php
$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -30 days" ) );
?>
<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
    <script>
      function getpdf(path,id) {
        console.log(path);
        window.open(`/siscontrol/assets/CFDI33_SIFEI/pdf_fact.php?NomArchXML=${path}&NomArchPDF=${id}.pdf`);
      }
    </script>
    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
          <?php include "../../assets/modales/modal-delete-confirmation.html" ?>        
          <?php include "../../sidebar/index.html"?> 
          <?php include "../../assets/modales/modal-spinner.html" ?>
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">Historial Globales</span>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                          <label>Fecha Inicio:</label>
			                    <input type="date"  id="FechaI" value="<?php echo $FechaInicio; ?>">
                        </div>                        
                        <div class="form-group col-4">
                          <label>Fecha Final:</label>
			                    <input type="date"  id="FechaF" value="<?php echo $today; ?>">
                        </div>                        
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-12 table-responsive">
                          <table id="list-notes" class="table table-hover table-dataTable" width="100%">
                            <thead>
                              <tr>
                                <th class="th-sm"></th>
                                <th class="th-sm">Folio</th>
                                <th class="th-sm">Fecha</th>
                                <th class="th-sm">Subtotal</th>
                                <th class="th-sm">iva</th>
                                <th class="th-sm">Total</th>
                              </tr>
                            </thead>                            
                           <tbody></tbody>
                          </table>
                        </div>
                    </div>
                    
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="/siscontrol/administrativo/globales/js/main.js" type="module"></script>


     
</html>