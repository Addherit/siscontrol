
`use strict`;

import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();

document.addEventListener('DOMContentLoaded', () => Init())
document.querySelector('#FechaI').addEventListener('change', () => buscarPedidosFecha());
document.querySelector('#FechaF').addEventListener('change', () => buscarPedidosFecha());



async function Init() {

	buscarPedidosFecha();
}

function buscarPedidosFecha(){

    SPINNER.showSpinner();
    const DATE_I = document.querySelector('#FechaI').value;
    const DATE_F = document.querySelector('#FechaF').value;
    console.log(DATE_I);
    console.log(DATE_F);
	// info
    const FORM_DATA = new FormData()
    FORM_DATA.append('fechai', DATE_I)
    FORM_DATA.append('fechaf', DATE_F)
    
    let pedidos = ``;
    fetch('php/select-globales.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
	console.log(data);

	data.forEach(pedido => {
		pedidos += `
		<tr>
		<td class="nowrap">
                        <button type="button" class="btn btn-sm btn-danger btn-get-pdf" title="getpdf" onclick="getpdf('${pedido.Archivo}','${pedido.ID_Factura}')">
                            <span class="material-icons align-middle">picture_as_pdf</span> 
			    pdf
                        </button>
		</td>
		<td class="id">${pedido.Folio}</td>
		<td class="fecha">${pedido.F_Fac}</td>
		<td class="subtotal">${pedido.Subtotal}</td>
		<td class="iva">${pedido.IVA}</td>
		<td class="total">${pedido.Total}</td>
		</tr>
		`
	});
    	document.querySelector('#list-notes tbody').innerHTML = pedidos;
    })
    setTimeout(() => SPINNER.hideSpinner(), 1000);
}

