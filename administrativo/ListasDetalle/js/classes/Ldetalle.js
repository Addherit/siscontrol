import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();


export class Detalles {

    constructor () {
        this.idProduct = ``;
    }

    getDetalle(id) {

        
        const FORM_DATA = new FormData()
        FORM_DATA.append('id', id);
        return fetch('php/select-detalles.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data;
        })

    }

    async setDetalle(id) {

        SPINNER.showSpinner();
        const DATA = await this.getDetalle(id);
        let lineas = ``;

        DATA.forEach(producto => {

            const STATUS = this.getColorStatus(producto.Estado);
            const BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(producto.Estado);
            lineas += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-warning btn-edit" title="Detalles" data-toggle="modal" data-target="#modal-detalle-edit">
                            <span class="material-icons align-middle">info</span>
                            Detalles
                        </button>
                    </td>
                    <td>${producto.ID}</td>
                    <td>${producto.Categoria}</td>
                    <td>${producto.Medida}</td>
                    <td>${producto.Precio}</td>
                </tr>
            `
        });

        document.querySelector('#list-inventario tbody').innerHTML = lineas;
        const dataTable = new DataTable('list-inventario');
        this.addEventClickToBtnDeleteActivate();
        this.addEventClickToBtnEditProduct();
        dataTable.initDataTable();

    }
    addEventClickToBtnDeleteActivate() {
        document.querySelectorAll('.btn-delete-activate').forEach(btn => {
            btn.addEventListener('click', (event) => {
       
                const NAME_PRODUCT = event.currentTarget.parentElement.parentElement.children[3].textContent;
                document.querySelector('#disabled-name').innerHTML = `la entrada`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                document.querySelector('#confirm-name').innerHTML = `la entrada`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_PRODUCT;
                this.idProduct = event.currentTarget.parentElement.parentElement.children[1].textContent;
                
            })
        })

    }

    addEventClickToBtnEditProduct() {
        document.querySelectorAll('.btn-edit').forEach(btn => {
            btn.addEventListener('click', (event) => {

                const ID_PRODUCT = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoDetalleById(ID_PRODUCT);
                
            })
        })        

    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Terminada: `<td>${status}</td>`,
            Eliminada: `<td class="text-danger">${status}</td>`
        }

        return COLOR_STATUS[status];

    }

    getBtnActivateDisabled(status) {


        if (status == 'Terminada') {
            return `
            <button type="button" class="btn btn-sm btn-success btn-delete-activate" title="Activar Prodcuto" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else {
            return `
            <button type="button" class="btn btn-sm btn-danger btn-delete-activate" title="Desactivar producto" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Eliminar
            </button>`
        }
       
    }

    disabledProduct() {
        const ID_PRODUCT = this.idProduct        
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', ID_PRODUCT);
        fetch('php/eliminar-entrada.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-disabled-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    activateProduct() {
        /*
        const ID_PRODUCT = this.idProduct
        const FORM_DATA = new FormData()
        FORM_DATA.append('ID_Producto', ID_PRODUCT);
        fetch('php/activar-producto.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setAlmacen();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })
        */

    }

    UpdateDetalle() {
        const FORM_DATA = new FormData(document.querySelector("#form-save-item"))        
        fetch('php/update-detalle.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            $('#modal-detalle-edit').modal('hide');
            this.setDetalle();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
            
        })

    }

    getInfoDetalleById(id_product) {
        const FORM_DATA = new FormData()
        FORM_DATA.append('Id', id_product)        
        return fetch('php/select-detalle.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
           return data;  
        })
    }

    async setInfoDetalleById(id_product) {
        const Detalle = await this.getInfoDetalleById(id_product);
        document.querySelector('[name=categoriae]').value = Detalle[0].Categoria
        document.querySelector('[name=medidae]').value = Detalle[0].Medida
        document.querySelector('[name=rango1]').value = Detalle[0].RangoM1
        document.querySelector('[name=rango2]').value = Detalle[0].RangoM2
        document.querySelector('[name=precioe]').value = Detalle[0].Precio
        document.querySelector('[name=ID]').value = Detalle[0].ID
    }
   
}