import { Detalles } from "./classes/Ldetalle.js";
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();
const DETALLES = new Detalles();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-new-rango').addEventListener('click', () => DETALLES.ADDRango());
document.querySelector('#btn-save-rango').addEventListener('click', () => DETALLES.UpdateDetalle());


const init = () => {

    SPINNER.showSpinner();
    const PARAMS = new URLSearchParams(window.location.search);
    const ID_AGENTE = PARAMS.get('id');
    DETALLES.setDetalle(ID_AGENTE);

}



