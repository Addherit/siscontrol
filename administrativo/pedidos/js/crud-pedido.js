
import {DataTable} from '../../../assets/js/classes/dataTable.js';
import {Formatter} from "../../../assets/js/plugins/currency-format.js";
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();

export class ListNotes {

  constructor(tableId) {
      this.tableId = tableId;        
      this.stringMessage = "la nota de venta";
      this.rowToDelete = ``;
  }

  async getSalesNotes() {
  
    const FORM_DATA = new FormData()
    FORM_DATA.append('id_user', localStorage.getItem('user_id'))
    const DATA = await fetch('php/select-pedidos.php', { method: 'POST', body: FORM_DATA })
    return DATA.json();
    
  }

  async getSalesNotesByAgenteAndStatus(id_agente, status, date_i, date_f) {

    const FORM_DATA = new FormData();
    FORM_DATA.append('id_agente', id_agente);
    FORM_DATA.append('status', status);
    FORM_DATA.append('fecha_ini', date_i);
    FORM_DATA.append('fecha_fin', date_f);
    FORM_DATA.append('id_user', localStorage.getItem('user_id'));
    
    const DATA = await fetch('php/select-pedidos.php', { method: 'POST', body: FORM_DATA })
    return DATA.json();

  }

  async setSalesNotes(DATA) {

    let rows = this.makeRowsPedidos(DATA)
    document.querySelector(`#${this.tableId} tbody`).innerHTML = rows;
    this.asignEventDeletePedido();
    this.asingEventSeeDetallePedido();
    setTimeout(() => { 
      SPINNER.hideSpinner();     
      const dataTable = new DataTable('list-sales-notes');
      dataTable.initDataTable();
    }, 500);
   
    

  }

  asignEventDeletePedido() {

    // Itera todos los botones de eliminado para asignar el evento click.
    document.querySelectorAll('.btn-delete').forEach(button => {        
      button.addEventListener('click', (event) => {
        document.querySelector("#delete-name").innerHTML = this.stringMessage
        document.querySelector("#id-to-delete").innerHTML = event.currentTarget.parentElement.parentElement.children[1].textContent;
        this.rowToDelete = event.currentTarget.parentElement.parentElement;
      })
    });

  }

  asingEventSeeDetallePedido() {

    // Itera todos los botones de detalle para asignar el evento click.
    document.querySelectorAll('.btn-detalle').forEach(button => {        
      button.addEventListener('click', (event) => {          
        const ID_PEDIDO = event.currentTarget.id;        
        if (ID_PEDIDO) {
          window.location = `/siscontrol/administrativo/detalle pedido/index.php?idPedido=${ID_PEDIDO}`;  
        }
        
      })
    });

  }

  makeRowsPedidos(data) {

    const ID_type = localStorage.getItem('id_user_type');
    let t = 0;
    const FORMATTER = new Formatter();
    let pedidos = ``;
    data.forEach(pedido => {
        if ( ID_type == "4"){
          t = 0;
        }else{
          t = FORMATTER.currencyFormat(FORMATTER.numberFormat(pedido.Total));

        }
        pedidos += `
          <tr>
            <td class="text-nowrap align-middle text-center">          
              <button type="button" class="btn btn-sm btn-danger btn-delete" id="${pedido.ID_Venta}" ${this.pedidoIsDeleted(pedido.Estatus)} title="Quitar producto" data-toggle="modal" data-target="#modal-delete-confirmation"><span class="material-icons align-middle">delete</span>Eliminar</button>
              <button type="button" class="btn btn-sm btn-warning btn-detalle" id="${pedido.ID_Venta}" title="Detalle nota de venta"><span class="material-icons align-middle">edit</span>Detalle</button>
            </td> 
            <td>${pedido.ID_Venta}</td>
            <td>${pedido.Cve_Cte}</td>
            <td>${pedido.Nom_Agente}</td>
            <td>${pedido.Fecha_Vta}</td>
            <td>$ ${t}</td>
            <td class="${this.getStatusColorPedido(pedido.Estatus)}">${pedido.Estatus}</td>
          </tr>
        `
      });

      return pedidos;

  }
    
  deleteSaleNote() {

    const ID_PEDIDO = document.querySelector('#id-to-delete').innerHTML;      
    const FORM_DATA = new FormData();
    FORM_DATA.append('id_pedido', ID_PEDIDO);
    fetch('php/eliminar-pedido.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(async data => {

        $('#modal-delete-confirmation').modal('hide');
        const DATA = await this.getSalesNotes();
        this.setSalesNotes(DATA);
        const MSN = new Message(data.type, data.message);
        MSN.showMessage();

    })

  }

  updateSaleNote() {

  }

  getStatusColorPedido(estatus) {

    const ESTATUS = {

      COTIZACION: 'text-success',
      CONFIRMADO: 'text-success',
      DEMORADO: 'text-warning',
      ABIERTO: 'text-success',
      CERRADO: 'text-success',
      CANCELADO: 'text-danger',
      ELIMINADO: 'text-danger',

    }

    return ESTATUS[estatus];

  }

  pedidoIsDeleted(status) {

    let response = ``;
    status != 'COTIZACION' ? response = `disabled` : response;
    return response

  }

}