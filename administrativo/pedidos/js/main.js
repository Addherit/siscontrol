`use strict`;

import {ListNotes} from './crud-pedido.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();
const SALES_NOTES = new ListNotes('list-sales-notes')

document.addEventListener('DOMContentLoaded', () => existAgenteAndStatus())
document.querySelector('#btn-delete').addEventListener('click', () => SALES_NOTES.deleteSaleNote())

async function existAgenteAndStatus() {

    SPINNER.showSpinner();
    let data = ``;
    const PARAMS = new URLSearchParams(window.location.search);
    const ID_AGENTE = PARAMS.get('agente');
    const STATUS = PARAMS.get('status');
    const DATE_I = PARAMS.get('date_i');
    const DATE_F = PARAMS.get('date_f');
    ID_AGENTE && STATUS ? data = await SALES_NOTES.getSalesNotesByAgenteAndStatus(ID_AGENTE, STATUS, DATE_I, DATE_F) : data = await SALES_NOTES.getSalesNotes();
    SALES_NOTES.setSalesNotes(data);
    
}