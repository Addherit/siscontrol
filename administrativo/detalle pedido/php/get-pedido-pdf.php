<?php

include_once('../../../assets/db/conexion.php');
require('../../../assets/CFDI33_SIFEI/fpdf/fpdf.php');

$id_pedido = $_GET["Pedido"];
//get header info
$sql = "SELECT v.`ID_Venta`, v.`Cve_Cte`, v.`Fecha_Vta`, v.`Cve_Agente`, v.`Estatus`,v.`EstatusPago`, v.`Atencion`, v.`ID_Pedido`, v.`ID_Cotizacion`, v.`Cobro`, v.`Factura`, v.`Saldo`, TRUNCATE(v.Iva,2) as Iva,TRUNCATE(v.Total,2) as Total,TRUNCATE(v.Subtotal,2) as Subtotal,substring_index (a.Nom_Agente,' ', 2) as Nom_Agente,c.Email1,c.ID_Cliente,(SELECT   f1.Archivo FROM    factura f1 WHERE   f1.ID_Venta = v.ID_Venta order by f1.`F_Fac` desc limit 1) as Path   FROM `venta` v left join cliente c on v.Cve_Cte = c.Nombre_Cte left join agente a on a.ID_Agente = v.`Cve_Agente` where v.`ID_Venta`=$id_pedido";
$res = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
//datos del header
// Column headings
$headerH = array('Cliente', 'Agente', 'Fecha', 'Pedido');
// Data loading
$dataH = array (
  array($res[0]['Cve_Cte'],$res[0]['Nom_Agente'],$res[0]['Fecha_Vta'],$id_pedido)
);

//datos del Sat
// Column headings
$headerS = array('Subtotal', 'IVA 16%', 'Total');
// Data loading
$dataS = array (
  array('$'.$res[0]['Subtotal'],'$'.$res[0]['Iva'],'$'.$res[0]['Total'])
);
//set Estatus
$estados = $res[0]['Estatus'];


//Get Data por el detalle
$sqld = "SELECT v.*, CASE WHEN v.Clave_Prod = 's0' THEN 'Servicio de corte' ELSE substring_index(v.Descripcion,substring_index (v.Descripcion,' ', 2),-1) END AS DDescripcion ,p.CatProdServ,p.CatUnid,u.Nombre as Udescrip FROM `ventad` v left join productos p on v.`Clave_Prod` = p.`Clave_Prod` and p.Estatus = 'Activo'  left join `cfdi33_cat_unimed` u on p.CatUnid = u.Clave where `ID_Venta` = $id_pedido order by `ID_VtaD`asc";
$resd = $con->query($sqld)->fetchAll(PDO::FETCH_ASSOC );

//datos del detalle
// Column headings
$header = array('No','Descripcion', 'Ud', 'Cant', 'Precio', 'Importe', 'PesoReal');
// Data loading
$data = array ();

//obv tabla
// Column headings
$headero = array('No','Observaciones');
// Data loading
$datao = array ();
/*
  array("BH4528","1518 45 X 28 MM // Largo: 3.06m Pza:25.92","Kg","25.92","89.00","2,306.88"),
  array("s0","Servicio de corte","","1","5.00","5.00")
);
*/
$iter = 0;
$itero = 0;
$iteruo = 0;
foreach ($resd as $row ) {
	if($row['Descripcion'] == "Servicio de corte"){
		$data[$iter] = array (' ',$row['Descripcion'],$row['Unidad'],$row['Cantidad'],'$'.$row['Precio'],'$'.$row['Importe'],'');
		$iteruo--;
	}else{
		$data[$iter] = array ($iteruo,$row['Descripcion'],$row['Unidad'],$row['Cantidad'],'$'.$row['Precio'],'$'.$row['Importe'],'');
	}
	//print_r($data);
	if($row['Observaciones'] != ''){
		$datao[$itero++] = array ($iteruo,$row['Observaciones']);
	}
	$iter++;
	$iteruo++;
	//print_r($row['Observaciones']);
}


class PDF extends FPDF {

	public $estatus;
	public function __construct($estado){
		parent::__construct();
		$this->estatus = $estado;
	}

	// Page header
	function Header() {
		
		// Add logo to page
		$this->Image('../../../assets/img/logo.png',10,8,33);
		
		// Set font family to Arial bold
		$this->SetFont('Arial','B',20);
		
		// Move to the right
		$this->Cell(80);
		
		// Header
		$this->Cell(50,10,"$this->estatus",1,0,'C');
		
		// Line break
		$this->Ln(40);
	}

	// Page footer
	function Footer() {
		
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		
		// Page number
		$this->Cell(0,10,'Page ' .
			$this->PageNo() . '/{nb}',0,0,'C');
	}

	// Cotizacion table
	function CotizacionTable($header, $data){
		// Column widths
		$w = array(10,160, 20, 30,30,30);
		// Header
		for($i=0;$i<count($header)-1;$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Cell($w[4],6,$row[4],'LR');
			$this->Cell($w[5],6,$row[5],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	// Abierto Coinfirmado table
	function NoCotizacionTable($header, $data){
		// Column widths
		$w = array(30,180, 20, 30, 20);
		// Header
		for($i=0;$i<count($header)-2;$i++){
			if($i == 4){
				$this->Cell($w[$i],7,$header[6],1,0,'C');
			}else{
				$this->Cell($w[$i],7,$header[$i],1,0,'C');
			}
		}
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Cell($w[6],6,$row[6],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}
	// Comentarios table
	function ObvTable($header, $data){
		// Column widths
		$w = array(20,260);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Cell($w[3],6,$row[3],'LR');
			$this->Cell($w[4],6,$row[4],'LR');
			$this->Cell($w[5],6,$row[5],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	function HeaderTable($header, $data){
		// Colors, line width and bold font
		$this->SetFillColor(38,198,218);
		$this->SetTextColor(0);
		$this->SetDrawColor(57,73,171);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		// Header
		$w = array(140, 90, 30, 20);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = false;
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
			$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[2],6,$row[2],'LR',0,'R',$fill);
			$this->Cell($w[3],6,$row[3],'LR',0,'R',$fill);
			$this->Ln();
			$fill = !$fill;
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}

	// Sat table
	function SatTable($header, $data){
		// Column widths
		$w = array(60, 60, 60);
		// Header
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C');
		$this->Ln();
		// Data
		foreach($data as $row)
		{
			$this->Cell($w[0],6,$row[0],'LR');
			$this->Cell($w[1],6,$row[1],'LR');
			$this->Cell($w[2],6,$row[2],'LR');
			$this->Ln();
		}
		// Closing line
		$this->Cell(array_sum($w),0,'','T');
	}
}

// Instantiation of FPDF class
$pdf = new PDF($estados);

// Define alias for number of pages
$pdf->AliasNbPages();
$pdf->addpage('O');
$pdf->SetFont('Times','',10);



//construccion del pdf
$pdf->HeaderTable($headerH,$dataH);
if($estados == "COTIZACION"){
	$ot = 0;
	$pdf->Ln(5);
	$pdf->CotizacionTable($header,$data);
	$pdf->Ln(5);
	if(count($datao) != 0){
		if(count($datao) >= 3 and count($data) >=10){
			$pdf->addPage('O');
			$ot = 0;
		}else{
			$ot = 1;
		}
		$pdf->ObvTable($headero,$datao);
	}
	if(count($data)>= 10){
		if($ot !=0){
			$pdf->addPage('O');
		}
	}
	$pdf->Ln(5);
	$pdf->SatTable($headerS,$dataS);
}else{
	$pdf->Ln(5);
	//print_r($resd);
	//print_r($data);
	$pdf->NoCotizacionTable($header,$data);
	$pdf->Ln(5);
	if(count($data)>= 10){
		$pdf->addPage('O');
	}
	if(count($datao) != 0){
		$pdf->ObvTable($headero,$datao);
	}
}
//salida	
$pdf->Output();

?>
