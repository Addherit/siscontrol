<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {
// variables
    $id = $_POST['idpedido'];
    $idu = $_POST['idU'];

//Check stock que tenga
    $token = 0;
    $code = array();
    $insertdata = array();
    $sqlcheck = "SELECT s.`Cantidad`,(sk.Stock - IFNULL((select sum(sl.Cantidad) from StockPedido sl where sl.Id_stocks = s.`Id_stocks` and sl.Estatus = 'Usado'),0) ) as Stock_usable,p.Clave_Prod,s.`Id` FROM `StockPedido` s left join Stocks sk on sk.Id = s.`Id_stocks` left join productos p on p.ID_Producto = s.`Id_Producto` where s.`Id_venta` = $id";
    $re = $con->query($sqlcheck)->fetchAll(PDO::FETCH_ASSOC );

    foreach ($re as $key) {
        if($key['Cantidad'] <= $key['Stock_usable']){
            $insertdata[] = ["Cantidad"=>$key['Cantidad'], "Idsp" =>$key['Id']];
        }else{
            array_push($code, $key['Clave_Prod']); 
        }

    }

    if(count($code) == 0){
        //tiene todos los stocks correctos seteados
        $token = 1;
    }else{
        $codigostr = "No tienen Stock Disponible: ";
        $codigostr .= implode("|",$code);
    }

//Revisiones
    if($token == 1){
        //Update Estatus
        $sql = "UPDATE `venta` set `Estatus`='CERRADO' where `ID_Venta`=$id ";
        $res = $con->query($sql);
        //actualizacion
        foreach ($insertdata as $key ) {
            $idu =$key['Idsp'];
            $sql = "UPDATE `StockPedido` set `Estatus`='Usado' where `Id`= $idu";
            $res = $con->query($sql);
        }

        $result =["type"=>'success',"message"=>'Se Cerro el pedido correctamente',"sql"=> $sqlcheck];

    }else{ // stocks no correctos
        $result =["type"=>'danger',"message"=>$codigostr,"sql"=> $sqlcheck];
    }



} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($result);
//}
?>