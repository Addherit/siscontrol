<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {
    $id = $_POST['id_pedido'];
    $idU = $_POST['id_user']; //user

//    $sql = "SELECT v.*,a.Nom_Agente FROM `venta` v left join agente a on a.ID_Agente = v.`Cve_Agente` where v.`ID_Venta`=$id ";
    $sql = "SELECT v.`ID_Venta`, v.`Cve_Cte`, v.`Fecha_Vta`, v.`Cve_Agente`, v.`Estatus`,v.`EstatusPago`, v.`Atencion`, v.`ID_Pedido`, v.`ID_Cotizacion`, v.`Cobro`, v.`Factura`, v.`Saldo`, FORMAT(v.Iva,2) as Iva,TRUNCATE(v.Total,2) as Total,TRUNCATE(v.Subtotal,2) as Subtotal,a.Nom_Agente,c.Email1,c.ID_Cliente,c.Nom_Agente as 'AgenteCliente',(SELECT   f1.Archivo FROM    factura f1 WHERE   f1.ID_Venta = v.ID_Venta order by f1.`F_Fac` desc limit 1) as Path   FROM `venta` v left join cliente c on v.Cve_Cte = c.Nombre_Cte left join agente a on a.ID_Agente = v.`Cve_Agente` where v.`ID_Venta`=$id ";
    $result = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
    //add header
    $res['header'] = $result;
    //add body
    $sql1 = "SELECT v.*,p.CatProdServ,p.CatUnid,u.Nombre as Udescrip  FROM `ventad` v left join productos p on v.`Clave_Prod` = p.`Clave_Prod` and p.Estatus = 'Activo' left join `cfdi33_cat_unimed` u on p.CatUnid = u.Clave where `ID_Venta` = $id order by `ID_VtaD` asc";
    $resu = $con->query($sql1)->fetchAll(PDO::FETCH_ASSOC );
    $res['body'] = $resu;

} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($res);
    //comentario de revision
//}
?>