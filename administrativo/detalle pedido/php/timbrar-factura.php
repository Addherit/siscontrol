<?php 
header('content-type: application/json');
include_once('../../../assets/db/conexion.php');
include("../../../assets/CFDI33_SIFEI/qrlib/qrlib.php");

try {

    $datos = json_decode(file_get_contents('php://input'));
    $datos =$datos->{'facturaData'} ;
    $header =$datos->{'cabecera'} ;
    $partidas =$datos->{'partidas'} ;

    //var sets
    $tipocompro =$header->{'tipoComprobante'} ;
    $mpago =$header->{'metodoPgo'} ;
    $fpago =$header->{'formaPago'} ;
    $subtotal =$header->{'subtotal'} ;
    $iva =$header->{'immpuestoTrasladado'} ;
    $total =$header->{'total'} ;
    $idventa =$header->{'idVenta'} ;
    $cp =$header->{'cp'} ;
    $cfdi =$header->{'cfdi'} ;
    $rfcE =$header->{'rfcEmisor'} ;
    $nombreE =utf8_encode($header->{'nombreEmisor'} );
    $rfcR =$header->{'rfcReceptor'} ;
    $nombreR =$header->{'nombreReceptor'} ;
    $ivas =$header->{'nombreReceptor'} ;
    $moneda ="MXN" ;
    // test
    $parti = array();
    $partis0 = array();
    $partis0n = array();
    //separacion
    foreach ($partidas as $k) {
        $claveprod= $k->{'claveProducto'};
        if($claveprod == 's0'){
            $partis0[] = $k;
        }else{
            $parti[] = $k;
        }
    }
    //revision de s0
    if(count($partis0) > 1){
        // si tiene 2 o mas
        $importes = array();
        $cantidades = array();
        foreach ($partis0 as $k ) {
            $claveprod= $k->{'claveProducto'};
            $importe= $k->{'importe'};
            $cantidad= $k->{'cantidad'};
            $desc =  $k->{'descripcion'};
            $catprodserv= $k->{'CatProdServ'};
            $catunid= $k->{'CatUnid'};
            $udes= $k->{'Udescript'};
            $valoru= $k->{'valorUnitario'};
            $descuento= $k->{'descuento'};
            $obs= $k->{'Observaciones'};

            $partis0n[0]['claveProducto'] = $claveprod;
            $partis0n[0]['cantidad'] += $cantidad;
            $partis0n[0]['descripcion'] = $desc;
            $partis0n[0]['CatUnid'] = $catunid;
            $partis0n[0]['CatProdServ'] = $catprodserv;
            $partis0n[0]['Udescript'] = " ";
            $partis0n[0]['valorUnitario'] = $valoru;
            $partis0n[0]['descuento'] = $descuento;
            $partis0n[0]['importe'] += $importe;
            $partis0n[0]['Observaciones'] = $obs;
        }

        $i = $partis0n[0]['importe'];
        $c = $partis0n[0]['cantidad'];
        
        $v =($i / $c );
        //$v = bcdiv($v,'1',4);
        $partis0n[0]['valorUnitario'] = $v;

        $aux = array();
        foreach ($parti as $key) {
            $aux[]= $key;
        }
        foreach ($partis0n as $key) {
            $aux[]= (object) $key;
        }
        $partidas = $aux;
    }

    //print_r($partidas);


    //exit("se termino el scprit");

    //get the folio
    $sql = "SELECT max(`Folio`) as id FROM `factura`";
    $res = $con->query($sql)->fetchAll(PDO::FETCH_ASSOC );
    $folio = $res[0]['id'] + 1;

    //insert header timbrado
    $sqlu = "insert into `factura`(`tipocompro`,`serie`, `folio`,`m_pago`, `fpago`, `subtotal`, `iva`, `total`, `moneda`, `id_venta`) 
     values ('$tipocompro','B','$folio','$mpago','$fpago','$subtotal','$iva','$total','$moneda','$idventa')";
    $res = $con->query($sqlu);
    $id = $con->lastInsertId(); 

    foreach ($partidas as $k) {
        $claveprod= $k->{'claveProducto'};
        $importe= $k->{'importe'};
        $cantidad= $k->{'cantidad'};
        $desc =  $k->{'descripcion'};
        $catprodserv= $k->{'CatProdServ'};
        $catunid= $k->{'CatUnid'};
        $valoru= $k->{'valorUnitrario'};
        $descuento= $k->{'descuento'};
        $obs= $k->{'Observaciones'};
        //print_r($k);

        $sqlu = "INSERT INTO `facturad`( `Folio`,`Idfactura`, `Cve_Prod`, `Descripcion`,`Cantidad`, `Unidad`, `PU`, `Importe`, `Obs`, `CveProdServ`) 
            VALUES ($folio,$id,'$claveprod','$desc',$cantidad,'$catunid',0,$importe,'$obs','$catprodserv')";
        //print($sqlu);

        $res = $con->query($sqlu);
    }

    //exit("se termino el scprit");

### CÓDIGO FUENTE, FACTURACIÓN ELECTRÓNICA CFDI VERSIÓN 3.3 ACORDE A LOS REQUIRIMIENTOS DEL SAT, ANEXO 20.

### 1. CONFIGURACIÓN INICIAL ######################################################

    # 1.1 Configuración de zona horaria
    date_default_timezone_set('America/Mexico_City'); // 

    # 1.2 Muestra la zona horaria predeterminada del servidor (opcional a mostrar)


### 2. ASIGNACIÓN DE VALORES A VARIABLES ###################################################
    $SendaPEMS  = "../../../assets/CFDI33_SIFEI/archs_pem/";   // 2.1 Directorio en donde se encuentran los archivos *.cer.pem y *.key.pem (para efectos de demostración se utilizan los que proporciona el SAT para pruebas).
    $SendaCFDI  = "../../../assets/CFDI33_SIFEI/archs_cfdi/";  // 2.2 Directorio en donde se almacenarán los archivos *.xml (CFDIs).
    $SendaGRAFS = "../../../assets/CFDI33_SIFEI/archs_graf/";  // 2.3 Directorio en donde se almacenan los archivos .jpg (logo de la empresa) y .png (códigos bidimensionales).
    $SendaXSD   = "../../../assets/CFDI33_SIFEI/archs_xsd/";   // 2.4 Directorio en donde se almacenan los archivos .xsd (esquemas de validación, especificaciones de campos del Anexo 20 del SAT);
    
    
    // 2.5 Datos de acceso del usuario (proporcionados por www.finkok.com) modo de integración (para pruebas) o producción.

    $userSIFEI="MUMR6604247X9";
    $passwordSIFEI ="650976ba";
    $idEquipoSIFEI ="ZDZmMzVlZjgtOWNlYi0xNjE3LWY3ZGUtNTJkODQ1MjIyMWFi";

    
    ### MUESTRA LOS DATOS DEL USUARIO QUE ESTÁ TIMBRANDO (OPCIONAL A MOSTRAR) ######
    
    
### 3. DEFINICIÓN DE VARIABLES INICIALES ##########################################
    $noCertificado = "30001000000400002447"; //"20001000000300005692";  // 3.1 Número de certificado.
    $file_cer      = "IIA040805DZ4.cer.pem";  // 3.2 Nombre del archivo .cer.pem 
    $file_key      = "IIA040805DZ4.key.pem";  // 3.3 Nombre del archivo .cer.key
    
###################################################################################
    
    
### 4. DATOS GENERALES DE LA FACTURA ##############################################
    $fact_serie        = "B";                             // 4.1 Número de serie.
    $fact_folio        = $folio;             // 4.2 Número de folio (para efectos de demostración se asigna de manera aleatoria).
    $NoFac             = $fact_serie.$fact_folio;         // 4.3 Serie de la factura concatenado con el número de folio.
    $fact_tipcompr     = $tipocompro;                             // 4.4 Tipo de comprobante.
    $tasa_iva          = 16;                              // 4.5 Tasa del impuesto IVA.
    $subTotal          = 0;                               // 4.6 Subtotal, suma de los importes antes de descuentos e impuestos (se calculan mas abajo). 
    //193980.18 sub
    //importe 31036.8608
    //total
    $descuento         = 0;       // 4.7 Descuento (se calculan mas abajo).
    $IVA               = number_format(536,2,'.','');     // 4.8y hora de facturació IVA, suma de los impuestos (se calculan mas abajo).
    $total             = $total;                               // 4.9 Total, Subtotal - Descuentos + Impuestos (se calculan mas abajo). 
    $fecha_fact        = date("Y-m-d")."T".date("H:i:s"); // 4.10 Fecha n.
  //  $NumCtaPago        = "6473";                          // 4.11 Número de cuenta (sólo últimos 4 dígitos, opcional).
    $condicionesDePago = "CONDICIONES";                   // 4.12 Condiciones de pago.
    $formaDePago       = $fpago;                            // 4.13 Forma de pago.
    $metodoDePago      = $mpago;                           // 4.14 Clave del método de pago. Consultar catálogos de métodos de pago del SAT.
    $TipoCambio        = 1;                               // 4.15 Tipo de cambio de la moneda.
    $LugarExpedicion   = $cp;                         // 4.16 Lugar de expedición (código postal).
    $moneda            = "MXN";                           // 4.17 Moneda
    $totalImpuestosRetenidos   = 0;                       // 4.18 Total de impuestos retenidos (se calculan mas abajo).
    $totalImpuestosTrasladados = 0;                       // 4.19 Total de impuestos trasladados (se calculan mas abajo).
    
### 5. MUESTRA LA ZONA HORARIA PREDETERMINADA DEL SERVIDOR (OPCIONAL A MOSTRAR) ######

    
### 6. ARRAYS QUE CONTIENEN LOS ARTICULOS QUE FORMAN PARTE DE LA VENTA #####################
        $Array_ClaveProdServ = array(); // 6.1 Clave del SAT correspondiente al artículo o servicio (consultar el catálogo de productos del SAT).
        $Array_NoIdentificacion =array();    // 6.2 Clave asignada al artículo o servicio, sistema local.
        $Array_Cantidad =array();                  // 6.3 Cantidad.
        $Array_ClaveUnidad = array();               // 6.4 Clave del SAT correspondiente a la unidad de medida (consultar el catálogo de productos del SAT).M
        $Array_Unidad = array();      // 6.5 Descripción de la unidad de medida.
        $Array_Descripcion = array();       // 6.6 Descripción del artículo o servicio.
        $Array_ValorUnitario = array();         // 6.7 Valor unitario del artículo o servicio.
        $Array_Importe = array();             // 6.8 Importe del artículo o servicio.
        $Array_Descuento = array();                         // 6.9 Descuento aplicado al artículo o servicio.
        //arrays
        $ArrayTraslado_Base =  array();//, '2400', '17000'];                // 7.1 Atributo requerido para señalar la base para el cálculo del impuesto, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos
        $ArrayTraslado_Impuesto = array();//, '002', '002'];                   // 7.2 Atributo requerido para señalar la clave del tipo de impuesto trasladado aplicable al concepto (consultar catálogos del SAT).
        $ArrayTraslado_TipoFactor = array();//, 'Tasa', 'Tasa'];              // 7.3 Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto (consultar catálogos del SAT).
        $ArrayTraslado_TasaOCuota = array();//, '0.160000', '0.160000'];  // 7.4 Atributo condicional para señalar el valor de la tasa o cuota del impuesto que se traslada para el presente concepto. Es requerido cuando el atributo TipoFactor tenga una clave que corresponda a Tasa o Cuota (consultar catálogos del SAT).
        $ArrayTraslado_Importe = array();//, '384', '2720'];                // 7.5 Atributo condicional para señalar el importe del impuesto trasladado que aplica al concepto. No se permiten valores negativos. Es requerido cuando TipoFactor sea Tasa o Cuota
    $i = 0;
    foreach ($partidas as $k) {
     //   print_r($k);
        $claveprod= $k->{'claveProducto'};
        $importe= $k->{'importe'};
        $cantidad= $k->{'cantidad'};
        $desc =  $k->{'descripcion'};
        $catprodserv= $k->{'CatProdServ'};
        $catunid= $k->{'CatUnid'};
        $valoru= $k->{'valorUnitario'};
        $udesc= $k->{'Udescript'};
        $descuento= $k->{'descuento'};
        $obs= $k->{'Observaciones'};
        //unidad desc
        $udesc = "";
        switch ($catunid) {
            case "KGM":
                $udesc = "Kilogramo";
                break;
            case "CMT":
                $udesc = "Centímetro";
                break;
            case "MMT":
                $udesc = "Milímetro";
                break;
            case "MTR":
                $udesc = "Metro";
                break;
            case "LTR":
                $udesc = "Litro";
                break;
            case "INH":
                $udesc = "Pulgada";
                break;
            case "INQ":
                $udesc = "Pulgada cúbica";
                break;
            case "FOT":
                $udesc = "Pie";
                break;
            case "H87":
                $udesc = "Pieza";
                break;
        }
        $Array_ClaveProdServ[$i]= $catprodserv; // 6.1 Clave del SAT correspondiente al artículo o servicio (consultar el catálogo de productos del SAT).
        $Array_NoIdentificacion[$i] =$i+ 1;    // 6.2 Clave asignada al artículo o servicio, sistema local.
        $Array_Cantidad[$i] = $cantidad;                  // 6.3 Cantidad.
        $Array_ClaveUnidad[$i] = $catunid;               // 6.4 Clave del SAT correspondiente a la unidad de medida (consultar el catálogo de productos del SAT).M
        $Array_Unidad[$i] = $udesc;      // 6.5 Descripción de la unidad de medida.
        $Array_Descripcion[$i]= $desc;       // 6.6 Descripción del artículo o servicio.
        $Array_ValorUnitario[$i] = $valoru;         // 6.7 Valor unitario del artículo o servicio.
        $Array_Importe[$i] = (float)$valoru * (float)$cantidad;             // 6.8 Importe del artículo o servicio.
        $Array_Descuento[$i] = $descuento;                         // 6.9 Descuento aplicado al artículo o servicio.
        //impuestos
        $ArrayTraslado_Base[$i] = (float)$valoru * (float)$cantidad;//, '2400', '17000'];                // 7.1 Atributo requerido para señalar la base para el cálculo del impuesto, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos
        $ArrayTraslado_Impuesto[$i] = '002';//, '002', '002'];                   // 7.2 Atributo requerido para señalar la clave del tipo de impuesto trasladado aplicable al concepto (consultar catálogos del SAT).
        $ArrayTraslado_TipoFactor[$i] = 'Tasa';//, 'Tasa', 'Tasa'];              // 7.3 Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto (consultar catálogos del SAT).
        $ArrayTraslado_TasaOCuota[$i] = '0.160000';//, '0.160000', '0.160000'];  // 7.4 Atributo condicional para señalar el valor de la tasa o cuota del impuesto que se traslada para el presente concepto. Es requerido cuando el atributo TipoFactor tenga una clave que corresponda a Tasa o Cuota (consultar catálogos del SAT).
        $ArrayTraslado_Importe[$i] =(float)$ArrayTraslado_Base[$i]*0.160000; //, '384', '2720'];                // 7.5 Atributo condicional para señalar el importe del impuesto trasladado que aplica al concepto. No se permiten valores negativos. Es requerido cuando TipoFactor sea Tasa o Cuota
        $i++;
    }
    
 ### 7. ARRAYS QUE CONTIENEN LOS IMPUESTOS TRASLADADOS Y RETENIDOS POR CONCEPTO #############

 // verificacion de bloqueo con url de sw
    // Trasladados.
    //manejo de corte es un concepto
    //producto de servicio 01010101 //la unidad de medida bbusacar algo parecido 
    //base == importe
    //importe = base * .16
    /*
    $ArrayTraslado_Base = ['1000'];//, '2400', '17000'];                // 7.1 Atributo requerido para señalar la base para el cálculo del impuesto, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos
    $ArrayTraslado_Impuesto = ['002'];//, '002', '002'];                   // 7.2 Atributo requerido para señalar la clave del tipo de impuesto trasladado aplicable al concepto (consultar catálogos del SAT).
    $ArrayTraslado_TipoFactor = ['Tasa'];//, 'Tasa', 'Tasa'];              // 7.3 Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto (consultar catálogos del SAT).
    $ArrayTraslado_TasaOCuota = ['0.160000'];//, '0.160000', '0.160000'];  // 7.4 Atributo condicional para señalar el valor de la tasa o cuota del impuesto que se traslada para el presente concepto. Es requerido cuando el atributo TipoFactor tenga una clave que corresponda a Tasa o Cuota (consultar catálogos del SAT).
    $ArrayTraslado_Importe = ['160'];//, '384', '2720'];                // 7.5 Atributo condicional para señalar el importe del impuesto trasladado que aplica al concepto. No se permiten valores negativos. Es requerido cuando TipoFactor sea Tasa o Cuota
    */
    // Retenidos.
    /*
    $ArrayRetencion_Base = ['225000', '2400', '17000'];                // 7.6 Atributo requerido para señalar la base para el cálculo de la retención, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos.
    $ArrayRetencion_Impuesto = ['002', '002', '002'];                  // 7.7 Atributo requerido para señalar la clave del tipo de impuesto retenido aplicable al concepto (consultar catálogos del SAT).
    $ArrayRetencion_TipoFactor = ['Tasa', 'Tasa', 'Tasa'];             // 7.8 Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto (consultar catálogos del SAT).
    $ArrayRetencion_TasaOCuota = ['0.160000', '0.160000', '0.160000']; // 7.9 Atributo requerido para señalar la tasa o cuota del impuesto que se retiene para el presente concepto (consultar catálogos del SAT).
    $ArrayRetencion_Importe = ['36000', '384', '2720'];                // 7.10 Atributo requerido para señalar el importe del impuesto retenido que aplica al concepto. No se permiten valores negativos.
    */
//print_r($Array_Cantidad) ;
    
### 8 DETERMINANDO TOTALES ###################################################    
    
    // 8.1 Calculando subTotal.
    for ($i=0; $i<count($Array_Importe); $i++){
        
        $subTotal = $subTotal + $Array_Importe[$i];
    }
    
    $subTotal = number_format($subTotal,2,'.',''); 
 //  print_r($Array_Importe) ;
//   print_r($subTotal) ;
    // 8.2 Total impuestos trasladados.
    $totalImpuestosTrasladados = 0.0;
    for ($i=0; $i<count($ArrayTraslado_Importe); $i++){
        $totalImpuestosTrasladados = $totalImpuestosTrasladados + $ArrayTraslado_Importe[$i];
    }
    //print_r()
    //print_r($totalImpuestosTrasladados);
    // 8.3 Total impuestos retenidos.
    //for ($i=0; $i<count($ArrayRetencion_Importe); $i++){
        //$totalImpuestosRetenidos = $totalImpuestosRetenidos + $ArrayRetencion_Importe[$i];
    //}

    // 8.4 Calculando Total.
    $total = $subTotal - $descuento + $totalImpuestosTrasladados; // - $totalImpuestosRetenidos;
    
    
### 9. DATOS GENERALES DEL EMISOR #################################################  
    
    $emisor_rs     = $nombreE;  // 9.1 Nombre o Razón social.
    $emisor_rfc    = $rfcE;                        // 9.2 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos)
    $emisor_regfis = "REGIMEN GENERAL DE PERSONAS MORALES"; // 9.3 Régimen fiscal.    
        
    
### 10. DATOS GENERALES DEL RECEPTOR (CLIENTE) #####################################
    
    $RFC_Recep = $rfcR;                                                              // 10.1 RFC (al momento de timbrar el SAT comprueba que el RFC se encuentre registrado y vigente en su base de datos).
    if (strlen($RFC_Recep)==12){$RFC_Recep = " ".$RFC_Recep; }else{$RFC_Recep = $RFC_Recep;}  // 10.2 Al RFC de personas morales se le antecede un espacio en blanco para que su longitud sea de 13 caracteres ya que estos son de longitud 12.
    $receptor_rfc = $RFC_Recep;                                                               // 10.3 RFC.
    $receptor_rs  = $nombreR;                       // 10.4 Nombre o razón social.
    
//print_r($RFC_Recep);
### 11. CREACIÓN Y ALMACENAMIENTO DEL ARCHIVO .XML (CFDI) ANTES DE SER TIMBRADO ###################
    
    #== 11.1 Creación de la variable de tipo DOM, aquí se conforma el XML a timbrar posteriormente.
    $xml = new DOMdocument('1.0', 'UTF-8'); 
    $root = $xml->createElement("cfdi:Comprobante");
    $root = $xml->appendChild($root); 
    
    $cadena_original='||';
    $noatt=  array();
    
    #== 11.2 Se crea e inserta el primer nodo donde se declaran los namespaces ======
    cargaAtt($root, array("xsi:schemaLocation"=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd",
            "xmlns:cfdi"=>"http://www.sat.gob.mx/cfd/3",
            "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance"
        )
    );
    
//print_r($xml);
    #== 11.3 Rutina de integración de nodos =========================================
    cargaAtt($root, array(
             "Version"=>"3.3", 
             "Serie"=>$fact_serie,
             "Folio"=>$fact_folio,
             "Fecha"=>date("Y-m-d")."T".date("H:i:s"),
             "FormaPago"=>$formaDePago,
             "NoCertificado"=>$noCertificado,
             "CondicionesDePago"=>$condicionesDePago,
             "SubTotal"=>$subTotal,
             "Descuento"=>$descuento,
             "Moneda"=>$moneda,
             "TipoCambio"=>$TipoCambio,
             "Total"=>$total,
             "TipoDeComprobante"=>$fact_tipcompr,
             "MetodoPago"=>$metodoDePago,
             "LugarExpedicion"=>$LugarExpedicion
          )
       );
    
    
    $emisor = $xml->createElement("cfdi:Emisor");
    $emisor = $root->appendChild($emisor);
    cargaAtt($emisor, array("Rfc"=>$emisor_rfc,
                            "Nombre"=>$emisor_rs,
                            "RegimenFiscal"=>"601"
                             )
                        );
    
    
    $receptor = $xml->createElement("cfdi:Receptor");
    $receptor = $root->appendChild($receptor);
    cargaAtt($receptor, array("Rfc"=>$receptor_rfc,
                    "Nombre"=>$receptor_rs,
                    "UsoCFDI"=>$cfdi
                )
            );
    
    
    $conceptos = $xml->createElement("cfdi:Conceptos");
    $conceptos = $root->appendChild($conceptos);
    
    #== 11.4 Ciclo "for", recopilación de datos de artículos e integración de sus respectivos nodos = 
    
    for ($i=0; $i<count($Array_Cantidad); $i++){
       	
        $concepto = $xml->createElement("cfdi:Concepto");
        $concepto = $conceptos->appendChild($concepto);
        cargaAtt($concepto, array(
               "ClaveProdServ"=>$Array_ClaveProdServ[$i],
               "NoIdentificacion"=>$Array_NoIdentificacion[$i],
               "Cantidad"=>$Array_Cantidad[$i],
               "ClaveUnidad"=>$Array_ClaveUnidad[$i],
               "Unidad"=>$Array_Unidad[$i],
               "Descripcion"=>$Array_Descripcion[$i],
               "ValorUnitario"=>number_format($Array_ValorUnitario[$i],2,'.',''),
               "Importe"=>number_format($Array_Importe[$i],2,'.',''),
               "Descuento"=>number_format($Array_Descuento[$i],2,'.','')
            )
        );
    
        $impuestos = $xml->createElement("cfdi:Impuestos");
        $impuestos = $concepto->appendChild($impuestos);

            $Traslados = $xml->createElement("cfdi:Traslados");
            $Traslados = $impuestos->appendChild($Traslados);
            
                $Traslado = $xml->createElement("cfdi:Traslado");
                $Traslado = $Traslados->appendChild($Traslado);
                
                    cargaAtt($Traslado, array(
                           "Base"=>number_format($ArrayTraslado_Base[$i],2,'.',''),
                           "Impuesto"=>$ArrayTraslado_Impuesto[$i],
                           "TipoFactor"=>$ArrayTraslado_TipoFactor[$i],
                           "TasaOCuota"=>$ArrayTraslado_TasaOCuota[$i],
                           "Importe"=>number_format($ArrayTraslado_Importe[$i],2,'.','')
                        ) 
                    );    
                  
        
            /*$Retenciones = $xml->createElement("cfdi:Retenciones");
            $Retenciones = $impuestos->appendChild($Retenciones);
            
                $Retencion = $xml->createElement("cfdi:Retencion");
                $Retencion = $Retenciones->appendChild($Retencion);
                
                    cargaAtt($Retencion, array(
                           "Base"=>number_format($ArrayRetencion_Base[$i],2,'.',''),
                           "Impuesto"=>$ArrayRetencion_Impuesto[$i],
                           "TipoFactor"=>$ArrayRetencion_TipoFactor[$i],
                           "TasaOCuota"=>$ArrayRetencion_TasaOCuota[$i],
                           "Importe"=>number_format($ArrayRetencion_Importe[$i],2,'.','')
                        ) 
                    );*/
              
    }

#== 11.5 Impuestos retenidos y trasladados ==========================================

$Impuestos = $xml->createElement("cfdi:Impuestos");
$Impuestos = $root->appendChild($Impuestos);

    /*$Retenciones = $xml->createElement("cfdi:Retenciones");
    $Retenciones = $Impuestos->appendChild($Retenciones);    

        $Retencion = $xml->createElement("cfdi:Retencion");
        $Retencion = $Retenciones->appendChild($Retencion);

            cargaAtt($Retencion, array(
                   "Impuesto"=>"002",
                   "Importe"=>number_format($totalImpuestosRetenidos,2,'.','')
                ) 
            );

            cargaAtt($Impuestos, array(
                            "TotalImpuestosRetenidos"=>number_format($totalImpuestosRetenidos,2,'.','')
                        )
                    );*/
            
            
    $Traslados = $xml->createElement("cfdi:Traslados");
    $Traslados = $Impuestos->appendChild($Traslados);

        $Traslado = $xml->createElement("cfdi:Traslado");
        $Traslado = $Traslados->appendChild($Traslado);

            cargaAtt($Traslado, array(
                   "Impuesto"=>"002",
                   "TipoFactor"=>"Tasa",
                   "TasaOCuota"=>"0.160000",
                   "Importe"=>bcdiv($totalImpuestosTrasladados,1,2)
                ) 
            );    
            
        //$v = bcdiv($v,'1',4);
            cargaAtt($Impuestos, array(
                    "TotalImpuestosTrasladados"=>number_format($totalImpuestosTrasladados,2,'.','')
                )
            );

                         
    $complemento = $xml->createElement("cfdi:Complemento");
    $complemento = $root->appendChild($complemento);

    
    #== 11.6 Termina de conformarse la "Cadena original" con doble ||
    $cadena_original .= "|";   
    
    $file = fopen($SendaCFDI."CadenaOriginal_Factura_".$NoFac.".txt", "w");
    fwrite($file, $cadena_original . PHP_EOL);
    fclose($file);
    chmod($SendaCFDI."CadenaOriginal_Factura_".$NoFac.".txt", 0777);      
    
    #=== Muestra la cadena original (opcional a mostrar) =======================
    //echo $cadena_original;
    
    
    # 11.7 PROCESO OPCIONAL, NO NECESARIO PARA TIMBRAR UN DOCUMENTO FISCAL #####
    #== Proceso para agregar una Addenda con datos del sistema local (estos datos son ignorados por el PAC al momento de timbrar el documento XML). 
    
    // 11.17.1 Datos a integrar a la Addenda ===================================
    /*$IdEmpresa        = "TX34JK83";
    $UsuarioDeSistema = "ALEJANDRA OROSIO";
    $Fecha            = date("d/m/Y");
    $Hora             = date("H:i:s");
    $TipDocOrigen     = "PEDIDO";
    $FolioDocOrigen   = "A345";
    $Observaciones    = "ESTE ES UN EJEMPLO DE TEXTO CORRESPONDIENTE A OBSERVACIONES CAPTURADAS POR EL USUARIO QUE SE INTEGRAN AL DOCUMENTO .XML DEL CFDI COMO UNA ADDENDA DEL SISTEMA LOCAL, NO ES REQUISITO PARA TIMBRAR UN CFDI VERSION 3.3";
    
    // 11.17.2 Integración del nodo "Addenda" al documento .XML ================
    $Addenda = $xml->createElement("cfdi:Addenda");
    $Addenda = $root->appendChild($Addenda);     
    
        $SisLoc = $xml->createElement("cfdi:SistemaLocal");
        $SisLoc = $Addenda->appendChild($SisLoc);
    
        cargaAttSinIntACad($SisLoc, array(
                "IdEmpresa"=>$IdEmpresa,
                "UsuarioDeSistema"=>$UsuarioDeSistema,
                "Fecha"=>$Fecha,
                "Hora"=>$Hora,
                "TipoDocOrigen"=>$TipDocOrigen,
                "FolioDocOrigen"=>$FolioDocOrigen,
                "Observaciones"=>$Observaciones
            )
        );*/
        
    ## Fin de la integración de la Addenda. ####################################
    
    
    #== 11.8 Proceso para obtener el sello digital del archivo .pem.key ========
    $keyid = openssl_get_privatekey(file_get_contents($SendaPEMS.$file_key));
    openssl_sign($cadena_original, $crypttext, $keyid, OPENSSL_ALGO_SHA256);
    openssl_free_key($keyid);
    

    #== 11.9 Se convierte la cadena digital a Base 64 ==========================
    $sello = base64_encode($crypttext);    
    
    #=== Muestra el sello (opcional a mostrar) =================================
    //echo $sello;
    
    #== 11.10 Proceso para extraer el certificado del sello digital ============
    $file = $SendaPEMS.$file_cer;      // Ruta al archivo
    $datos = file($file);
    $certificado = ""; 
    $carga=false;  
    for ($i=0; $i<sizeof($datos); $i++){
        if (strstr($datos[$i],"END CERTIFICATE")) $carga=false;
        if ($carga) $certificado .= trim($datos[$i]);

        if (strstr($datos[$i],"BEGIN CERTIFICATE")) $carga=true;
    } 
    
    #=== Muestra el certificado del sello digital (opcional a mostrar) =========
    //echo $certificado;
    
    #== 11.12 Se continua con la integración de nodos ===========================   
    $root->setAttribute("Sello",$sello);
    $root->setAttribute("Certificado",$certificado);   # Certificado.
    
    
    #== Fin de la integración de nodos =========================================
    
    
    $NomArchCFDI = $SendaCFDI."PreCFDI-33_Factura_".$NoFac.".xml";
    
    
    #=== 11.12 Se guarda el archivo .XML antes de ser timbrado =======================
    $cfdi = $xml->saveXML();
    $xml->formatOutput = true;             
    $xml->save($NomArchCFDI); // Guarda el archivo .XML (sin timbrar) en el directorio predeterminado.
    unset($xml);
    
    #=== 11.13 Se dan permisos de escritura al archivo .xml. =========================
    chmod($NomArchCFDI, 0777);

    $nombre ="PreCFDI-33_Factura_".$NoFac.".xml";
    $sqlu = "UPDATE `factura` SET `Estatus`= 'VALIDA',`Archivo`='$nombre' WHERE `ID_Factura` = $id";
    $res = $con->query($sqlu);
    if($mpago == 'PUE'){
        $sqluv = "UPDATE `venta` SET `Estatus` = 'FACTURADO',EstatusPago = 'Pagado' WHERE `ID_Venta` = $idventa";
    }else{
        $sqluv = "UPDATE `venta` SET `Estatus` = 'PAGOS',EstatusPago = 'Inicial' WHERE `ID_Venta` = $idventa";
    }
    $res = $con->query($sqluv);
    $ress['path'] = $nombre;
    echo json_encode($ress);

    ##### FIN DE LA CREACIÓN DEL ARCHIVO .XML ANTES DE SER TIMBRADO ####################################################   

    ## 12. PROCESO DE TIMBRADO CON SIFEI ########################################################
    $path=$NomArchCFDI;
    $xmlSellado = file_get_contents( $path );
   // print_r($xmlSellado);
    //Para el atributo archivoXMLZip no es necesario aplicar el base64_encode ya que la misma libreria SoapClient lo hace, por eso la mandamos tal cual.
    $parametros=array('Usuario'=> $userSIFEI, 'Password'=>$passwordSIFEI, 'archivoXMLZip'=>$xmlSellado, 'Serie'=>'', 'IdEquipo'=>$idEquipoSIFEI);


    //url de pruebas
    $wsdl ="http://devcfdi.sifei.com.mx:8080/SIFEI33/SIFEI?wsdl";

    /*
    libxml_disable_entity_loader(false);
    $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, ));
//    print_r($client);
    try {
        
        $res = $client->__soapCall('getCFDI', array($parametros));
        $fileTmpZip = "../../../assets/CFDI33_SIFEI/timbrado.zip";  //nombre del zip
        //mandamos en un zip el xml timbrado en caso de exito
        file_put_contents($fileTmpZip, $res->return);
        //mandamos en un txt el mensaje soap del request
        file_put_contents("../../../assets/CFDI33_SIFEI/request.txt", $client->__getLastRequest());
        //mandamos en un txt el response del xml timbrado
        file_put_contents("../../../assets/CFDI33_SIFEI/response.txt", $client->__getLastResponse());
        $tmpDirName = $SendaCFDI; //nombre del directorio temporal
        $originalName = $SendaCFDI."CFDI-33_Factura_".$NoFac.".xml";; //nombre del xml que se envio, para obtenerlo con el mismo nombre
   
                
        $zipXml = new ZipArchive();

        if ($zipXml->open($fileTmpZip) === TRUE) {

            $zipXml->extractTo($tmpDirName);
            $zipXml->close();
        }

        $xmlTimbrado="";

        foreach (glob($tmpDirName . "/*.xml") as $file) {
            $xmlTimbrado = file_get_contents($file);

            unlink($file);

            file_put_contents($originalName, $xmlTimbrado);
            
        }
        
        $RespServ = $xmlTimbrado;
        
        
    } catch (SoapFault $e) {

        var_dump($e->faultcode, $e->faultstring, $e->detail);
        //mandamos en un txt el response del error
        file_put_contents("../../../assets/CFDI33_SIFEI/response.txt", $client->__getLastResponse());
    }   
    */
    
    echo htmlspecialchars($RespServ);
   
    ##### FIN DEL TIMBRADO  #####################################################

    ## 13. PROCESOS POSTERIORES AL TIMBRADO ########################################
    
    $NomArchXML = "CFDI-33_Factura_".$NoFac.".xml";
    $NomArchPDF = "CFDI-33_Factura_".$NoFac.".pdf";

     #== 13.7 Procesos para extraer datos del Timbre Fiscal del CFDI =========
     $docXML = new DOMDocument();
     $docXML->load($SendaCFDI."CFDI-33_Factura_".$NoFac.".xml");
     $comprobante = $docXML->getElementsByTagName("TimbreFiscalDigital");

    #== 13.8 Se obtienen contenidos de los atributos y se asignan a variables para ser mostrados =======
    foreach($comprobante as $timFis){
        $version_timbre = $timFis->getAttribute('Version');
        $sello_SAT      = $timFis->getAttribute('SelloSAT');
        $cert_SAT       = $timFis->getAttribute('NoCertificadoSAT'); 
        $sello_CFD      = $timFis->getAttribute('SelloCFD'); 
        $tim_fecha      = $timFis->getAttribute('FechaTimbrado'); 
        $tim_uuid       = $timFis->getAttribute('UUID'); 
/*
        echo 'Versión del timbre: <span style="color: #088A29;">'.$version_timbre.'</span><br>';
        echo 'Sello del SAT: <span style="color: #088A29">'.$sello_SAT.'</span><br>';
        echo 'Certificado del SAT: <span style="color: #088A29">'.$cert_SAT.'</span><br>';
        echo 'Sello del CFDI: <span style="color: #088A29">'.$sello_CFD.'</span><br>';
        echo 'Fecha de timbrado: <span style="color: #088A29">'.$tim_fecha.'</span><br>';
        echo 'Folio fiscal: <span style="color: #0830D2">'.$tim_uuid.'</span><br>';
        */
    }

     #== 13.8.1 Se muestra el número de factura asignado por el sistema local (no asingado por el SAT).
  //   echo 'No. de factura asignado por el sistema local: <span style="color: #B71616">'.$NoFac.'</span><br><br>';
            
     $params = $docXML->getElementsByTagName('Emisor');
     foreach ($params as $param) {
         $Emisor_RFC = $param->getAttribute('Rfc');
     }  
     
     $params = $docXML->getElementsByTagName('Receptor');
     foreach ($params as $param) {
         $Receptor_RFC = $param->getAttribute('Rfc');
     }              
     
     $params = $docXML->getElementsByTagName('Comprobante');
     foreach ($params as $param) {
         $total = $param->getAttribute('Total');
     }        
     
     #== 13.9 Se crea el archivo .PNG con codigo bidimensional =================================
     $filename = "archs_graf/Img_".$tim_uuid.".png";
     $CadImpTot = ProcesImpTot($total);
     //$Cadena = "?re=".$Emisor_RFC."&rr=".$Receptor_RFC."&tt=".$CadImpTot."&id=".$tim_uuid;
     $fe = substr($sello_CFD, -8);
     $Cadena = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?&id=".$tim_uuid."&re=".$Emisor_RFC."&rr=".$Receptor_RFC."&tt=".$CadImpTot."&fe=".$fe;
     QRcode::png($Cadena, $filename, 'H', 3, 2);    
     chmod($filename, 0777); 
 //    echo $filename;
    /* 
     echo '<div style="font-size: 11pt; color: #000099; ; font-family: Verdana, Arial, Helvetica, sans-serif;">';
     echo 'GRÁFICO "QR" RESULTANTE.';
     echo '</div>';
     echo '<img src="'.$filename.'" width="159" height="159" alt="'.$filename.'" style="margin-right: 20px;" />';      
    ´*/
     
     #== 13.10 Se crea código HTML para mostrar opciones al usuario.
  
 
 
##### FIN DE PROCEDIMIENTOS ####################################################   
    //$result = ["mensaje" => "timbrado hecho "];
} catch (Exception  $e) {
    $result = ["mensaje" => "error: ".$e];
}

// echo json_encode($result);
    //comentario de revision
//}
### 14. FUNCIONES DEL MÓDULO #########################################################
        
    # 14.1 Función que integra los nodos al archivo .XML y forma la "Cadena original".
    function cargaAtt(&$nodo, $attr){
        global $xml, $cadena_original;
        $quitar = array('sello'=>1,'noCertificado'=>1,'certificado'=>1);
        foreach ($attr as $key => $val){
            $val = preg_replace('/\s\s+/', ' ', $val);
            $val = trim($val);
            if (strlen($val)>0){
                 $val = utf8_encode(str_replace("|","/",$val));
                 $nodo->setAttribute($key,$val);
                 if (!isset($quitar[$key])) 
                   if (substr($key,0,3) != "xml" &&
                       substr($key,0,4) != "xsi:")
                    $cadena_original .= $val . "|";
            }
         }
     }
     
     
    # 14.2 Función que integra los nodos al archivo .XML sin integrar a la "Cadena original". 
    function cargaAttSinIntACad(&$nodo, $attr){
        global $xml;
        $quitar = array('sello'=>1,'noCertificado'=>1,'certificado'=>1);
        foreach ($attr as $key => $val){
            $val = preg_replace('/\s\s+/', ' ', $val);
            $val = trim($val);
            if (strlen($val)>0){
                 $val = utf8_encode(str_replace("|","/",$val));
                 $nodo->setAttribute($key,$val);
                 if (!isset($quitar[$key])) 
                   if (substr($key,0,3) != "xml" &&
                       substr($key,0,4) != "xsi:");
            }
         }
     }     

    
    # 14.3 Funciónes que da formato al "Importe total" como lo requiere el SAT para ser integrado al código QR.
     
    function ProcesImpTot($ImpTot){
        $ImpTot = number_format($ImpTot, 4); // <== Se agregó el 30 de abril de 2017.
        $ArrayImpTot = explode(".", $ImpTot);
        $NumEnt = $ArrayImpTot[0];
        $NumDec = ProcesDecFac($ArrayImpTot[1]);
        
        return $NumEnt.".".$NumDec;
    }
    
    function ProcesDecFac($Num){
        $FolDec = "";
        if ($Num < 10){$FolDec = "00000".$Num;}
        if ($Num > 9 and $Num < 100){$FolDec = $Num."0000";}
        if ($Num > 99 and $Num < 1000){$FolDec = $Num."000";}
        if ($Num > 999 and $Num < 10000){$FolDec = $Num."00";}
        if ($Num > 9999 and $Num < 100000){$FolDec = $Num."0";}
        return $FolDec;
    }        
    

?>