<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
//tomar las variables
    $id = $_POST['idpedido'];
    $idu = $_POST['idU'];

//check el tipo de usuario
    $tipocheck = "SELECT `Tipo_usuario` FROM `Users` where `userId` = $idu ";
    $tc = $con->query($tipocheck)->fetchAll(PDO::FETCH_ASSOC );
    $tipuser = $tc[0]['Tipo_usuario'];

//check que tengan almacen
    $tokenalma = 0;
    $alcode = array();
    $sqlcheckalmacen = "SELECT v.`Clave_Prod`,v.`Almacen` FROM `ventad` v where v.`ID_Venta` = $id and v.`Clave_Prod` <> 's0'";
    $real = $con->query($sqlcheckalmacen)->fetchAll(PDO::FETCH_ASSOC );

    foreach ($real as $key) {
        if($key['Almacen'] =="323"){
            array_push($alcode, $key['Clave_Prod']); 
        }
    }

    if(count($alcode) == 0){
        //tiene todos los almacenes seteados
        $tokenalma = 1;
    }else{
        $codigostr = "No tienen Almacen: ";
        $codigostr .= implode("|",$alcode);
    }

//check que tengan stocks de tu medida 
    $token = 0;
    $code = array();
    $insertdata = array();
    $sqlchecklargo = "SELECT v.`ID_VtaD`,v.`Clave_Prod`,v.`ltsPz`,v.PesoTeorico,v.PesoReal,v.`Medida_placa`,v.`Almacen`,v.Largo, p.ID_Producto,a.Id,s.Stock,s.Id as Idst,s.MedidaL FROM `ventad` v LEFT JOIN `productos` p on p.Clave_Prod = v.`Clave_Prod` LEFT JOIN `Almacen` a on a.Id = v.`Almacen` LEFT join `Stocks` s on s.Id_product = p.ID_Producto and s.Id_almacen = a.Id and v.Largo <= s.MedidaL LEFT JOIN Entradas_Salidas e on e.Id = s.Entrada where v.`ID_Venta` = $id and v.`Clave_Prod` <> 's0' and e.Estatus = 'Activo'  order by  v.`Clave_Prod` desc";
    $relar = $con->query($sqlchecklargo)->fetchAll(PDO::FETCH_ASSOC );

    $i = 0;
    $j = 0;
    $lastid = "0";
    $currentid = "";

   // echo"s:";
    //print_r(sizeof($relar));

    foreach ($relar as $key) {
        //echo"iter:";
        //print_r($j++);
        //echo"c:";
        //print_r($currentid);
        //echo"l:";
        //print_r($lastid);
        //echo"i:";
        //print_r($i);
        //get the current id
        $currentid = $key['ID_VtaD'];
        if(is_numeric($key['PesoReal'])){ // si tiene peso real toma ese 

            if($key['PesoReal'] <= $key['Stock']){

                if($currentid == $lastid){
                    if($i != 0){ //no se ingreso anteriormente
                        $insertdata[] = ["Cantidad"=>$key['PesoReal'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
                        $i = 0;
                        array_pop($code);
                    }else{ //se ingreso
                        //hacer nada sacar el ultimo ingreso 
                    }
                }else{ //no  es el mismo  y se resetea el i
                    $i = 0;
                    $insertdata[] = ["Cantidad"=>$key['PesoReal'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
                }
            }else{

                if($currentid == $lastid){
                    if($i == 0){ //se ingreso anteriormente
                        //nada
                    }else{
                        array_push($code, $key['Clave_Prod']); 
                        $i++;
                    }
                }else{
                    array_push($code, $key['Clave_Prod']); 
                    $i++;
                }
            }
        }else{ // si no tiene peso real toma peso teorico
            if($key['PesoTeorico'] <= $key['Stock']){

                if($currentid == $lastid){
                    if($i != 0){ //no se ingreso anteriormente
                        $insertdata[] = ["Cantidad"=>$key['PesoTeorico'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
                        $i = 0;
                        array_pop($code);
                    }else{ //se ingreso
                        //hacer nada sacar el ultimo ingreso
                    }
                }else{ //no  es el mismo  y se resetea el i
                    $i = 0;
                    $insertdata[] = ["Cantidad"=>$key['PesoTeorico'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
                }
            }else{

                if($currentid == $lastid){
                    if($i == 0){ //se ingreso anteriormente
                        //nada
                    }else{
                        array_push($code, $key['Clave_Prod']); 
                        $i++;
                    }
                }else{
                    array_push($code, $key['Clave_Prod']); 
                    $i++;
                }
            }
        }
        $lastid = $key['ID_VtaD'];
        //print_r($code);
    }
    /*
s:3
iter:0
c:247965
l:0
i:0
Array
(
    [0] => BH3220
)
iter:1
c:247965
l:247965
i:1
Array
(
)
iter:2
c:247966
l:247965
i:0
Array
(
)
    */
    if(count($code) == 0){
        //tiene todos los stocks correctos seteados
        $token = 1;
    }else{
        $codigostr = "No tienen Stock o de su largo: ";
        $codigostr .= implode("|",$code);
    }

// Revisiones
    if($tipuser == 1){ // token admin
        if($tokenalma == 1){ // token almacen
            $sql = "update `venta` set `estatus`='CONFIRMADO' where `id_venta`=$id ";
            $res = $con->query($sql);
            $result =["type"=>'success',"message"=>'se confirmo el pedido correctamente',"sql"=> $sql];
        }else{
            $result =["type"=>'danger',"message"=>$codigostr,"sql"=> $sqlcheckalmacen];
        }
    }else{ // no admin

        if($tokenalma == 1){ // token almacen
            if($token == 1){ // ultimate token
                //update estado
                $sql = "UPDATE `venta` set `Estatus`='CONFIRMADO' where `ID_Venta`=$id ";
                $res = $con->query($sql);
                //insertar
                $sqlinsert = "INSERT INTO `StockPedido` (`Id_Producto`, `Id_almacen`, `Id_stocks`,`Cantidad`,`Piezas`,`Id_venta`) VALUES ";
                $values =" ";
                foreach ($insertdata as $key) {
                    //echo "ids:";
                    //print_r($key);
                    $values .= "(".$key['Idp'].",".$key['ida'].",".$key['ids'].",'".$key['Cantidad']."','".$key['pieza']."',".$id."),";

                }
                $values = rtrim($values, ", ");
                $sqlinsert .= $values;
                $res = $con->query($sqlinsert);

                $result =["type"=>'success',"message"=>'Se Confirmo el pedido correctamente',"sql"=> $sqlinsert];
            }else{ // falta stock
                $result =["type"=>'danger',"message"=>$codigostr,"sql"=> $sqlchecklargo];
            }
        }else{
            $result =["type"=>'danger',"message"=>$codigostr,"sql"=> $sqlcheckalmacen];
        }

    }


} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($result);
?>