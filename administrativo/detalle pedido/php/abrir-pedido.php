<?php 
header('Content-type: application/json');
include_once('../../../assets/db/conexion.php');

try {
//tomar las variables
    $id = $_POST['idpedido'];
    $idu = $_POST['idU'];
    
//check el tipo de usuario
    $tipocheck = "SELECT `Tipo_usuario` FROM `Users` where `userId` = $idu ";
    $tc = $con->query($tipocheck)->fetchAll(PDO::FETCH_ASSOC );
    $tipuser = $tc[0]['Tipo_usuario'];

//check que tengan almacen
    $tokenalma = 0;
    $alcode = array();
    $sqlcheckalmacen = "SELECT v.`Clave_Prod`,v.`Almacen` FROM `ventad` v where v.`ID_Venta` = $id and v.`Clave_Prod` <> 's0'";
    $real = $con->query($sqlcheckalmacen)->fetchAll(PDO::FETCH_ASSOC );

    foreach ($real as $key) {
        if($key['Almacen'] =="323"){
            array_push($alcode, $key['Clave_Prod']); 
        }
    }

    if(count($alcode) == 0){
        //tiene todos los almacenes seteados
        $tokenalma = 1;
    }else{
        $codigostral = "No tienen Almacen: ";
        $codigostral .= implode("|",$alcode);
    }

//check que tengan stocks de tu medida 
    $token = 0;
    $code = array();
    $insertdata = array();
    $sqlchecklargo = "SELECT v.`Clave_Prod`,v.`ltsPz`,v.PesoTeorico,v.PesoReal,v.`Medida_placa`,v.`Almacen`,v.Largo, p.ID_Producto,a.Id,s.Stock,s.Id as Idst,s.MedidaL FROM `ventad` v LEFT JOIN `productos` p on p.Clave_Prod = v.`Clave_Prod` LEFT JOIN `Almacen` a on a.Id = v.`Almacen` LEFT join `Stocks` s on s.Id_product = p.ID_Producto and s.Id_almacen = a.Id and v.Largo <= s.MedidaL LEFT JOIN Entradas_Salidas e on e.Id = s.Entrada where v.`ID_Venta` = $id and v.`Clave_Prod` <> 's0' and e.Estatus = 'Activo'";
    $relar = $con->query($sqlchecklargo)->fetchAll(PDO::FETCH_ASSOC );

    foreach ($relar as $key) {
        if(is_numeric($key['PesoReal'])){
            if($key['PesoReal'] <= $key['Stock']){
                $insertdata[] = ["Cantidad"=>$key['PesoReal'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
            }else{
                array_push($code, $key['Clave_Prod']); 
            }
        }else{
            if($key['PesoTeorico'] <= $key['Stock']){
                $insertdata[] = ["Cantidad"=>$key['PesoTeorico'], "Idp" =>$key['ID_Producto'],"ida"=>$key['Almacen'],"ids"=>$key['Idst'],"pieza"=>$key['ltsPz']];
            }else{
                array_push($code, $key['Clave_Prod']); 
            }
        }
    }

    if(count($code) == 0){
        //tiene todos los stocks correctos seteados
        $token = 1;
    }else{
        $codigostr = "No tienen Stock o de su largo: ";
        $codigostr .= implode("|",$code);
    }


// Revisiones
    if($tipuser == 1){ // token admin

        if($tokenalma == 1){ // token almacen
            $sql = "UPDATE `venta` set `Estatus`='ABIERTO' where `ID_Venta`=$id ";
            $res = $con->query($sql);
            $result =["type"=>'success',"message"=>'Se abrio el pedido correctamente',"sql"=> $sql];
        }else{ // falta almacen
            $result =["type"=>'danger',"message"=>$codigostral,"sql"=> $sqlcheckalmacen];
        }

    }else{ // no admin

        if($tokenalma == 1){ // token almacen
            if($token == 1){ // ultimate check
                //update estado
                $sql = "UPDATE `venta` set `Estatus`='ABIERTO' where `ID_Venta`=$id ";
                $res = $con->query($sql);
                //insertar
                $sqlinsert = "INSERT INTO `StockPedido` (`Id_Producto`, `Id_almacen`, `Id_stocks`,`Cantidad`,`Piezas`,`Id_venta`) VALUES ";
                $values =" ";
                foreach ($insertdata as $key) {
                    $values .= "(".$key['Idp'].",".$key['ida'].",".$key['ids'].",'".$key['Cantidad']."','".$key['pieza']."',".$id."),";

                }
                $values = rtrim($values, ", ");
                $sqlinsert .= $values;
                $res = $con->query($sqlinsert);

                $result =["type"=>'success',"message"=>'Se abrio el pedido correctamente',"sql"=> $sqlinsert];
            }else{ // falta stock
                $result =["type"=>'danger',"message"=>$codigostr,"sql"=> $sqlchecklargo];
            }

        }else{ // falta almacen
            $result =["type"=>'danger',"message"=>$codigostral,"sql"=> $sqlcheckalmacen];
        }

    }


} catch (PDOException  $e) { // Mensaje de Error
    $result = ["mensaje" => "Error: ".$e];
}
//final
echo json_encode($result);
?>