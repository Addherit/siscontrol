import { Parcialidad } from "./classes/parcialidad.js";
import { Partida } from "./classes/partida.js";
import { Pedido } from "./classes/pedido.js";
import { Pagos } from "./classes/pagos.js";
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";
//import { jsPDF } from "jspdf";

const SPINNER = new Spinner();
const PEDIDO = new Pedido();
const PARCIALIDAD = new Parcialidad();
const PARTIDA = new Partida();
const PAGOS = new Pagos();


document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-confirm').addEventListener('click', () => PEDIDO.confirmPedido());
document.querySelector('#btn-open').addEventListener('click', () => PEDIDO.openPedido());
document.querySelector('#btn-close').addEventListener('click', () => PEDIDO.closePedido());
document.querySelector('#btn-cancel').addEventListener('click', () => PEDIDO.cancelPedido());
document.querySelector('#btn-generar-factura').addEventListener('click', () => PEDIDO.facturarPedido());
document.querySelector('#btn-add-parcialidad').addEventListener('click', () => addParcialidad());
document.querySelector('#btn-ver-factura').addEventListener('click', () => verFactura());
document.querySelector('[name=num-cuenta-pago]').addEventListener('keyup', (e) => getNumCuentaFormat(e, 'num-cuenta-pago'))
document.querySelector('[name=numero-cuenta]').addEventListener('keyup', (e) => getNumCuentaFormat(e, 'numero-cuenta'))

document.querySelector('#btn-open-modal-certificado').addEventListener('click', () => handleModales());
document.querySelector('#btn-imprimir').addEventListener('click', () => PARTIDA.imprimirPartidaDetalle());
//pdf
document.querySelector('#btn-pdf').addEventListener('click', () => PEDIDO.imprimirPedidoDetalle());
document.querySelector('#btn-almacen').addEventListener('click', () => PARTIDA.UpdateAlmacen());

document.querySelector('[name=monto-pago]').addEventListener('keyup', () => PAGOS.montoValido());

document.querySelector('#btn-crear-certificado').addEventListener('click', () => getCertificado())
document.querySelector('#btn-crear-c').addEventListener('click', () => openModalCrearCeritificado())
document.querySelector('#btn-download-c').addEventListener('click', () => openModalDownloadCeritificado())

document.querySelector('#btn-create-parcialidad').addEventListener('click', () => PARCIALIDAD.createParcialidad());
document.querySelector('#btn-add-renglon').addEventListener('click', () => addRenglon());

document.querySelector('#peso-real').addEventListener('keyup', () => PesoRealVsPesoTeorico());
document.querySelector('#btn-apply-p-real').addEventListener('click', () => PEDIDO.applyPesoReal())


document.querySelector('#form-pagos').addEventListener('submit', (e) => {
    
    e.preventDefault();
    PAGOS.createPay();

})

function init() { 

    SPINNER.showSpinner();
    PEDIDO.setInfoPedido();
    setTimeout(() => SPINNER.hideSpinner(), 1000);
    
}

function addParcialidad() {
    
    PARCIALIDAD.statusValidoParaCrearParcialidad();
    PARCIALIDAD.setParcialidades();
    PARTIDA.setPartidas();

}

const verFactura = () => {

    const ID_PEDIDO = document.querySelector('[name=pedido-id]').value;
    const PATH = document.querySelector('[name=pedido-id]').id;
    window.open(`/siscontrol/assets/CFDI33_SIFEI/pdf_fact.php?NomArchXML=${PATH}&NomArchPDF=${ID_PEDIDO}.pdf`);
    
}

function getCertificado () {
    
    html2canvas(document.querySelector("#capture")).then(canvas => {
        let enlace = document.createElement('a');
        enlace.download = "Certificado de calidad.png";      
        enlace.href = canvas.toDataURL();      
        enlace.click();
        const msn = new Message('success', 'Se descargó el ceritficado exitosamente');
        msn.showMessage();
    });

}

function handleModales() {

    // validar si se quiere hacer el certificado o descargar uno hecho
    $('#modal-detalle-partida').modal('hide');
    setTimeout(() => $('#modal-choise').modal('show'), 500);

}

function openModalCrearCeritificado() {

    $('#modal-choise').modal('hide');
    setTimeout(() => $('#modal-create-certificado').modal('show'), 500);
    
}

function openModalDownloadCeritificado() {

    const NAME_CERTIFICADO = localStorage.getItem('cert_name');

    if (NAME_CERTIFICADO) {

        NAME_CERTIFICADO.replace(' ','').split(',').forEach(async certificado => {
            const URL = `https://farguaceros.com/siscontrol/TempFiles/Certificados/${certificado}`;        
            const FILE_EXIST = await fetch(URL);

            if (FILE_EXIST.status == 200) {
                window.open(URL, '_blank');   
                $('#modal-choise').modal('hide');
            } else {
                const MSN = new Message('danger', 'error al descargar el certificado');
                MSN.showMessage();
            }
        })
        
        
    } else {
        const MSN = new Message('warning', 'No hay certificados para descargar');
        MSN.showMessage()
    }    
    
}

function addRenglon() {
    
    const NEW_ELEMENT = `
        <div class="col-4 mb-3">
            <div class="shadow p-2">
                <table class="table-bordered">
                    <tr>
                        <th>Elemento</th>
                        <td contenteditable class="w-100" style="background-color: #ffffea;"></td>
                    </tr>
                    <tr>
                        <th>Resultado</th>
                        <td contenteditable class="w-100" style="background-color: #ffffea;"></td>
                    </tr>
                    <tr>
                        <th>Max</th>
                        <td contenteditable class="w-100" style="background-color: #ffffea;"></td>
                    </tr>
                    <tr>
                        <th>Min</th>
                        <td contenteditable class="w-100" style="background-color: #ffffea;"></td>
                    </tr>
                </table>
            </div>
        </div>`;

    $('.elementos').append(NEW_ELEMENT)
    
}

function getNumCuentaFormat(e, name) {
    
    let numCuenta = e.target.value.replace(/-/g, '');
    let numParseCuenta = [...numCuenta].reduce((p, c, i) => p += (i && !(i % 4)) ? "-" + c : c, "");
    document.querySelector(`[name=${name}]`).value = numParseCuenta;

}

function PesoRealVsPesoTeorico() {

    const PESO_REAL = parseFloat(document.querySelector('#peso-real').value);
    const PESO_TEORICO = parseFloat(document.querySelector('#peso-teorico').value);    
    const RANGO = PESO_TEORICO * .10;
    const R_MAX = PESO_TEORICO + RANGO;
    const R_MIN = PESO_TEORICO - RANGO;
    
    // tiene un rango de +/- 10 % 
    if(PESO_REAL > R_MAX || PESO_REAL < R_MIN ) {

        const msn = new Message('warning', 'Peso real fuera de rango. Rango permitido +/- 10%');
        msn.showMessage();
    
    }
    
}



