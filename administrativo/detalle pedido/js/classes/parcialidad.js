import { Partida } from "./partida.js";

export class Parcialidad {

    async getParcialidades(id_pedido) {

        const FORMDATA = new FormData();
        FORMDATA.append('id_pedido', id_pedido);
        const DATA = await fetch('php/select-parcialidades.php', { method: 'POST', body: FORMDATA })
        return await DATA.json();

    }

    async createParcialidad() {

        const FORMDATA = new FormData();
        const PARCIALIDAD = this.getDataParcialidad(); 
        FORMDATA.append('parcialidad', JSON.stringify(PARCIALIDAD));
        const INSERT_DATA = await fetch('php/insert-parcial.php', { method: 'POST', body: FORMDATA })
        const RESPONSE = await INSERT_DATA.json();
        const MSN = new Message(RESPONSE.type, RESPONSE.message);
        MSN.showMessage();
        this.setParcialidades();
        const PARTIDA = new Partida;
        PARTIDA.setPartidas();

    }

    async setParcialidades() {

        let lineas = ``;
        const PARCIALIDADES = await this.getParcialidades(document.querySelector('[name=pedido-id]').value);
        if (PARCIALIDADES.length) {
            PARCIALIDADES.forEach(parcialidad => {
            
                lineas += `
                    <tr>
                        <td nowrap>
                            <button type="button" class="btn btn-sm btn-warning btn-detalle-parcialidad" title="Detalle de la parcialidad">
                                <span class="material-icons align-middle">edit</span>
                                Detalle
                            </button>
                        </td>
                        <td>${parcialidad.Id}</td>
                        <td>${moment().format('YYYY-MM-DD, h:mm:ss a', parcialidad.CreatedDate)}</td>
                        <td>${parcialidad.Observacion}</td>
                    </tr>
                `;            
    
            });
        } else {
            lineas += `<tr class="bg-warning"><td colspan="4" class="text-center">No hay parcialidades creadas</td></tr>`; 
        }
        
        document.querySelector('#tbl-parcialidades tbody').innerHTML = lineas;
        this.addEventDetalleParcialidad();

    }

    getDataParcialidad() {
        
        let body = [];    
        const TRS = document.querySelectorAll('#tbl-detalle-partidas tbody tr');
        TRS.forEach(tr => {

            const CORTES = parseInt(tr.children[2].textContent);
            let cant_original = tr.children[1].textContent; 
            CORTES ? cant_original = parseInt(tr.children[1].textContent) + CORTES : cant_original;

            body.push({

                id_item: tr.children[0].id,
                clave_producto: tr.children[0].textContent,
                precio: tr.children[1].id,
                cantidad_original: cant_original,
                cortes: CORTES,
                cantidad_entregar: tr.children[3].children[0].value,
                restante: parseInt(tr.children[4].textContent) - parseInt(tr.children[3].children[0].value),                
                observaciones: tr.children[5].children[0].value,
                


            })
        });
        
        let parcialidad = {
            header:  {
                id_pedido: document.querySelector('[name=pedido-id]').value
            },
            body: {
                body
            }
        };
        return parcialidad;
    
    }

    addEventDetalleParcialidad() {

        document.querySelectorAll('.btn-detalle-parcialidad').forEach(btn => {
            btn.addEventListener('click', (event) => {
                const ID_PARCIALIDAD = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setParcialidad(ID_PARCIALIDAD);
            })
        })
    }

    async setParcialidad(id_parcialidad) {

        let lineas = ``;
        const DATA = await this.getParcialidad(id_parcialidad);

        if (DATA.length) {
            DATA.forEach((detalle, index) => {
                lineas += `
                    <tr>
                        <td>${index + 1}</td>
                        <td>${detalle.Clave_Prod}</td>
                        <td>${detalle.CantidadOriginal}</td>
                        <td>${detalle.Cantidad}</td>
                        <td>${detalle.Restante}</td>                        
                    </tr>
                `
            });
        } else {
            lineas = `<tr><td class="text-center bg-warning" colspan="6">No hay detalle de parcialidad</td></tr>`;
        }
        

        document.querySelector('#tbl-detalle-parcialidad tbody').innerHTML = lineas;
    }

    async getParcialidad(id_parcialidad) {

        const FORMDATA = new FormData();
        FORMDATA.append('id_parcial', id_parcialidad);
        const DATA = await fetch('php/select-parcialidad.php', { method: 'POST', body: FORMDATA });
        return await DATA.json();

    }

    statusValidoParaCrearParcialidad() {

        const STATUS = document.querySelector('[name=estatus]').value;
        if(!STATUS == 'ABIERTO') {
            const msn = new Message('warning', 'El estatus debe ser ABIERTO para poder crear parcialidades');
            msn.showMessage();
            document.querySelector('#btn-create-parcialidad').disabled = true;            
        }

    }

}