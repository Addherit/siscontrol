import { Formatter } from "../../../../assets/js/plugins/currency-format.js";

export class Pagos {

    createPay() {
        
        let estado = '';
        const METODO_PAGO = document.querySelector('[name=pago-metodo-pago]');
        const FORMA_PAGO = document.querySelector('[name=pago-forma-pago]');

        METODO_PAGO.value == 'PUE' ? estado = 'completo' : estado = 'parcial';
        !this.getRestante().debe ? estado = 'ultimo' : estado;
        const FORMATTER = new Formatter();
        const FORMDATA = new FormData();
        

        FORMDATA.append('estado', estado);
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value);
        FORMDATA.append('monto_venta', FORMATTER.numberFormat(document.querySelector('[name=total]').value.split(' ')[1]))
        FORMDATA.append('monto', FORMATTER.numberFormat(document.querySelector('[name=monto-pago]').value))
        FORMDATA.append('num_cuenta', document.querySelector('[name=num-cuenta-pago]').value)
        FORMDATA.append('observaciones', document.querySelector('[name=observacones-pago]').value)
        FORMDATA.append('forma_pago', FORMA_PAGO.value)        
        FORMDATA.append('saldo_insoluto', FORMATTER.numberFormat(document.querySelector('#cant-restante').innerText.split(' ')[1]))
        FORMDATA.append('metodo_pago', METODO_PAGO.value)
        FORMDATA.append('cp', document.querySelector('[name=cp]').value)    
        FORMDATA.append('rfcemisor', 'FAGL8909302H3')
        FORMDATA.append('nombreemisor', 'LEOPOLDO FARIAS GUTIERREZ')
        FORMDATA.append('rfcrecptor', document.querySelector('[name=rfc-receptor]').value)
        FORMDATA.append('nombrerecptor', document.querySelector('[name=razon-social-receptor]').value)

        if(FORMA_PAGO.value && METODO_PAGO.value) {
            fetch('php/insert-detalle-pago.php', { method: 'POST', body: FORMDATA })
            .then(data => data.json())
            .then(data => {

                data.type === 'success' ? document.querySelector('#form-pagos').reset() : false;
                const msn = new Message(data.type, data.message);
                msn.showMessage();
                this.getPays();

            })
        } else {
            const msn = new Message('warning', 'Selecciona un método y una forma de pago');
            msn.showMessage();
        }

    }

    async getPays() {

        const FORMDATA = new FormData();
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value);
        
        const DATA = await fetch('php/select-pagos.php', { method: 'POST', body: FORMDATA });
        const PAGOS = await DATA.json();
        const FORMATTER = new Formatter()
        let lineas = ``;
        if (PAGOS.length > 0) {
            PAGOS.forEach(pago => {
                lineas +=`
                    <tr>
                        <td>
                            <button id="${pago.Archivo}" class="btn btn-sm btn-info download-comprobante">
                                <span class="material-icons align-middle">download</span>
                                Comprobante
                            </button>
                        </td>
                        <td>${pago.Id}</td>
                        <td>${pago.Datecreated}</td>
                        <td class="monto-row">$ ${FORMATTER.currencyFormat(pago.Monto)}</td>
                        <td>${pago.ModoPago}</td>
                        <td>${pago.Observaciones}</td>
                    </tr>
                `
            });
        } else {
            lineas = `<tr><td colspan="6" style="background-color: #fbfcd8" class="text-center">No hay comprobantes de pagos</td></tr>`;  
        }

        document.querySelector('#table-pagos tbody').innerHTML = lineas;
        const CANT_RESTANTE = this.getRestante().cant_restante;        
        document.querySelector('#cant-restante').innerHTML = `$ ${FORMATTER.currencyFormat(CANT_RESTANTE)}`;
        this.addEventDownloadComprobante()

    }
    
    getRestante() {

        const FORMATER = new Formatter()
        let sumaPagos = 0;
        let debe = true;
        const TDS = document.querySelectorAll('#table-pagos tbody tr .monto-row');
        const TOTAL_PEDIDO = FORMATER.numberFormat(document.querySelector('[name=total]').value.split(' ')[1]);
        const PAGO_EN_CURSO = document.querySelector('[name=monto-pago]').value ? document.querySelector('[name=monto-pago]').value : 0;

        if (TDS !== null) {
            TDS.forEach(td => {
                sumaPagos = sumaPagos + parseFloat(!td.textContent || td.textContent.split(' ')[1] == 'NaN' ? 0 : FORMATER.numberFormat(td.textContent.split(' ')[1]));
            });
            sumaPagos = parseFloat(sumaPagos) + parseFloat(PAGO_EN_CURSO);
        }        
        TOTAL_PEDIDO == sumaPagos ? debe = false : debe;        
        return {
            debe: debe,
            cant_restante: TOTAL_PEDIDO - sumaPagos,        
        }                

    }

    montoValido() {

        const FORMATTER = new Formatter()
        const MONTO = FORMATTER.numberFormat(document.querySelector('[name=monto-pago]').value);
        const TOTAL_DEBE = parseFloat(FORMATTER.numberFormat(document.querySelector('#cant-restante').textContent.split(' ')[1]));
        const BTN_CREAR_PAGO = document.querySelector('#btn-crear-pago');
        if(MONTO > TOTAL_DEBE){

            BTN_CREAR_PAGO.disabled = true;
            const msn = new Message('warning', 'El monto escrito, es mayor al total de la factura');
            msn.showMessage();
            
        } else {
            BTN_CREAR_PAGO.disabled = false;
        } 

    }

    addEventDownloadComprobante() {

        document.querySelectorAll('.download-comprobante').forEach(btn => {
            btn.addEventListener('click', (e) => {
                const NAME_FILE = e.currentTarget.id;                
                window.open(`/siscontrol/assets/CFDI33_SIFEI/pdf_fact.php?NomArchXML=${NAME_FILE}&NomArchPDF=${document.querySelector('[name=pedido-id]').value}.pdf`);                
            })
        })

    }

}