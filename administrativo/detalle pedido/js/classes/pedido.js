import { Formatter } from "../../../../assets/js/plugins/currency-format.js";
import { Partida } from "./partida.js";
import { Pagos } from "./pagos.js";
//import { jsPDF } from "jspdf";

const FORMATTER = new Formatter();

export class Pedido {


    async getPedido() {

        const PARAMS = new URLSearchParams(window.location.search);
        const FORMDATA = new FormData();
        FORMDATA.append('id_pedido', PARAMS.get('idPedido'));
        const DATA = await fetch('php/select-pedido.php', { method: 'POST', body: FORMDATA });
        return await DATA.json();

    }

    async setInfoPedido() {

        const DATA_PEDIDO = await this.getPedido();
        this.setDataCAB(DATA_PEDIDO.header[0]);
        this.setDataDET(DATA_PEDIDO.body);
        this.setDataCertificado(DATA_PEDIDO.header[0])
        this.configByEStatus();
        this.getFormaPago();
        this.getMetodoPago();
        this.getTipoMoneda();
        this.getCFDIS();
        this.getTipoComprobante();
        setTimeout(() => {
            this.setInfoFacturaCliente();
        }, 1000);

    }

    getFormaPago() {

        fetch('php/select-cat-formapago.php')
        .then(data => data.json())
        .then(data => {
            let formas_pago = '<option value="" selected disabled>Selecciona una forma de pago</option>';
            data.forEach(element => { 
                formas_pago += `<option value="${element.Clave}">${element.Descrip}</option>`;
            });
            document.querySelector('[name=forma-pago]').innerHTML = formas_pago;
            document.querySelector('[name=pago-forma-pago]').innerHTML = formas_pago;
        })
    
    }

    getMetodoPago() {
    
        fetch('php/select-cat-metodopago.php')
        .then(data => data.json())
        .then(data => {
            let metodo_pago = '<option value="" selected disabled>Selecciona un metodo de pago</option>';
            data.forEach(element => { 
                metodo_pago += `<option value="${element.Clave}">${element.Descrip}</option>`;
            });
            document.querySelector('[name=metodo-pago]').innerHTML = metodo_pago;
            document.querySelector('[name=pago-metodo-pago]').innerHTML = metodo_pago;
        })
        
    }

    getTipoMoneda() {

        fetch('php/select-cat-moneda.php')
        .then(data => data.json())
        .then(data => {
            let tipo_moneda = '<option value="" disabled>Selecciona una moneda</option>';
            data.forEach(element => { 
                // Se selecciona peso mexicano por default
                let moneda_seleccionada = ''
                element.Clave === 'MXN' ? moneda_seleccionada = 'selected' : moneda_seleccionada;
                tipo_moneda += `<option value="${element.Clave}" ${moneda_seleccionada}>${element.Descrip}</option>`;
            });
            document.querySelector('[name=tipo-moneda]').innerHTML = tipo_moneda;
        })
            
    }

    getCFDIS() {
        
        fetch('php/select-cfdi.php')
        .then(data => data.json())
        .then(data => {
            let options = `<option value="" selected disabled>Selecciona un CFDI</option>`;
            data.forEach( cfdi => {
                options += `<option value="${cfdi.ID_UsoCfdi}">${cfdi.UsoCfdi}</option>`;
            });
            document.querySelector("[name=cfdi]").innerHTML = options;            
        })

    }

    setInfoFacturaCliente() {

        const FORMDATA = new FormData();
        FORMDATA.append('id_cliente', document.querySelector('[name=client]').id)
        fetch('php/select-info-factura.php', { method: 'POST', body: FORMDATA })
        .then(data => data.json())
        .then(data => {
            const DATA = data[0];
            document.querySelector('[name=razon-social-receptor]').value = DATA.Razon_Soc;
            document.querySelector('[name=rfc-receptor]').value = DATA.RFC;
            document.querySelector('[name=email-fiscal]').value = DATA.Email1;
            document.querySelector('[name=forma-pago]').value = DATA.TipoPago;
            document.querySelector('[name=cfdi]').value = DATA.Cfdi;
            document.querySelector('[name=cp]').value = DATA.CP;

            
        })
        const PAGOS = new Pagos();
        PAGOS.getPays()

    }

    setDataCAB(DATA_PEDIDO) {

        const ID_type = localStorage.getItem('id_user_type');
        document.querySelector('[name=client]').value = DATA_PEDIDO.Cve_Cte;
        document.querySelector('[name=client]').id = DATA_PEDIDO.ID_Cliente;
        document.querySelector('[name=email]').value = DATA_PEDIDO.Email1;
        document.querySelector('[name=agente]').value = DATA_PEDIDO.Nom_Agente;
        document.querySelector('[name=pedido-id]').value = DATA_PEDIDO.ID_Venta;
        document.querySelector('[name=pedido-id]').id = DATA_PEDIDO.Path;
        document.querySelector('[name=fecha]').value = DATA_PEDIDO.Fecha_Vta;
        document.querySelector('[name=estatus]').value = DATA_PEDIDO.Estatus;


        if(ID_type == "4"){
            document.querySelector('[name=subtotal]').value = `$ ${FORMATTER.currencyFormat(0)}`;
            document.querySelector('[name=iva]').value = `$ ${FORMATTER.currencyFormat(0)}`;
            document.querySelector('[name=total]').value = `$ ${FORMATTER.currencyFormat(0)}`;

        }else{
            document.querySelector('[name=subtotal]').value = `$ ${FORMATTER.currencyFormat(DATA_PEDIDO.Subtotal)}`;
            document.querySelector('[name=iva]').value = `$ ${DATA_PEDIDO.Iva}`;
            console.log(DATA_PEDIDO.Iva);
            document.querySelector('[name=total]').value = `$ ${FORMATTER.currencyFormat(DATA_PEDIDO.Total)}`;
            //check the user and client
            if(ID_type == "2"){
                var nagente =DATA_PEDIDO.Nom_Agente;
                var cagente = DATA_PEDIDO.AgenteCliente;
                //console.log(nagente);
                //console.log(cagente);
                if(nagente == cagente){
                    document.querySelector('[name=tu]').innerHTML ="";
                }else{
                    document.querySelector('[name=tu]').innerHTML ="   El cliente de este pedido no lo tienes Asignado";
                }
            }
        }

        setTimeout(() => {
            if (DATA_PEDIDO.EstatusPago == 'Pagado') {
                document.querySelector('#btn-add-pagos').disabled = true
            }    
        }, 1000);

    }

    setDataDET(DATA_PEDIDO) {

        const ID_type = localStorage.getItem('id_user_type');
        let p = 0;
        let i = 0;
        let lineas = ``
        this.setDetNotaCredito(DATA_PEDIDO)
        DATA_PEDIDO.forEach(row => {
            
            let btn_detalle = ``;
            if (row.Descripcion != 'Servicio de corte') {
                btn_detalle = `<button type="utton" class="btn btn-sm btn-warning btn-detalle-partida" title="Detalle del producto" data-toggle="modal" data-target="#modal-detalle-partida">
                <span class="material-icons align-middle">edit</span> 
                Detalle
            </button>`
            }
            if(ID_type == "4"){
                p = 0;
                i = 0;
            }else{
                p = FORMATTER.currencyFormat(row.Precio);
                i = FORMATTER.currencyFormat(row.Importe);
            }
            lineas += `
                <tr>
                    <td nowrap>
                        ${btn_detalle}
                    </td>
                    <td id="${row.ID_VtaD}" classs="code-item">${row.Clave_Prod}</td>
                    <td class="descripcion-det" id="${row.CatProdServ}">${row.Descripcion}</td>
                    <td class="unidad-det" id="${row.CatUnid}">${row.Unidad}</td>
                    <td class="cantidad-det" id="${row.Udescrip}">${row.Cantidad}</td>
                    <td nowrap class="precio-det">$ ${p}</td>
                    <td nowrap class="importe-det">$ ${i}</td>
                    <td class="observaciones-det">${row.Observaciones}</td>
                </tr>
            `;
            
        });

        document.querySelector('#tbl-detalle-sale tbody').innerHTML = lineas;
        this.addEventDetallePartida();

    }

    setDataCertificado(DATA_PEDIDO) {

        document.querySelector('#tbl-certificado #tbl-fecha').textContent = DATA_PEDIDO.Fecha_Vta;
        document.querySelector('#tbl-certificado #tbl-factura').textContent = DATA_PEDIDO.ID_Venta;
        document.querySelector('#tbl-certificado #tbl-cliente').textContent = DATA_PEDIDO.Cve_Cte;
        document.querySelector('#tbl-certificado #tbl-idCliente').textContent = DATA_PEDIDO.ID_Cliente;

    }

    setDetNotaCredito(DATA_PEDIDO) {

        let lineas = ``;
        DATA_PEDIDO.forEach(row => {
            lineas += `
                <tr>
                    <td nowrap>
                        <button type="button" class="btn btn-sm btn-danger btn-borrar-concepto" title="Detalle del producto">
                            <span class="material-icons align-middle">delete</span> 
                            Borrar
                        </button>
                    </td>
                    <td id="${row.ID_VtaD}" classs="code-item">${row.Clave_Prod}</td>
                    <td class="descripcion-det td-description" id="${row.CatProdServ}">${row.Descripcion}</td>
                    <td class="unidad-det" id="${row.CatUnid}">${row.Unidad}</td>
                    <td class="cantidad-det" id="${row.Udescrip}">${row.Cantidad}</td>
                    <td nowrap class="precio-det">$ ${FORMATTER.currencyFormat(row.Precio)}</td>
                    <td nowrap class="importe-det">$ ${FORMATTER.currencyFormat(row.Importe)}</td>                    
                </tr>
            `;
        })

        document.querySelector('#tbl-nota-credito tbody').innerHTML = lineas

        this.addEventBoorrarConcepto();

    }

    addEventBoorrarConcepto() {

        const BTNS = document.querySelectorAll('.btn-borrar-concepto');
        BTNS.forEach(btn => {
            btn.addEventListener('click', (event) => {                
                const CONCEPT = event.currentTarget.parentElement.parentElement;
                CONCEPT.remove();                
            })
        })

    }

    async confirmPedido() {

        // se pone en estatus confirmado para entregar el pedido en una sola exhibición
        const FORMDATA = new FormData();
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value)
        FORMDATA.append('idU', localStorage.getItem('user_id'))
        const DATA = await fetch('php/confirmar-pedido.php', { method: 'POST', body: FORMDATA });
        const PARSE_DATA = await DATA.json();
        const msn = new Message(PARSE_DATA.type, PARSE_DATA.message);
        msn.showMessage();
        this.setInfoPedido();
        //se genera corte de placa en caso de que se necesite
    }

    async closePedido() {

        // Se finaliza el proceso del pedido
        const FORMDATA = new FormData();
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value)
        FORMDATA.append('idU', localStorage.getItem('user_id'))
        const DATA = await fetch('php/cerrar-pedido.php', { method: 'POST', body: FORMDATA });
        const PARSE_DATA = await DATA.json();
        const msn = new Message(PARSE_DATA.type, PARSE_DATA.message);
        msn.showMessage();
        this.setInfoPedido();

    }

    async openPedido() {

        // Se pone en estatus abierto para poder entregar parcialidades del pedido
        const FORMDATA = new FormData();
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value)
        FORMDATA.append('idU', localStorage.getItem('user_id'))
        const DATA = await fetch('php/abrir-pedido.php', { method: 'POST', body: FORMDATA });
        const PARSE_DATA = await DATA.json();
        const msn = new Message(PARSE_DATA.type, PARSE_DATA.message);
        msn.showMessage();
        this.setInfoPedido();


    }

    async cancelPedido() {

        // Solo se puede cancelar el pedido si el estatus es abierto o confirmado
        const STATUS = document.querySelector('[name=estatus]').value;
        if (STATUS === 'ABIERTO' || STATUS === 'CONFIRMADO') {

            const FORMDATA = new FormData();
            FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value)
            FORMDATA.append('idU', localStorage.getItem('user_id'))
            const DATA = await fetch('php/cancelar-pedido.php', { method: 'POST', body: FORMDATA });
            const PARSE_DATA = await DATA.json();
            const msn = new Message(PARSE_DATA.type, PARSE_DATA.message);
            msn.showMessage();
            this.setInfoPedido();

        } else {

            //mensaje El pedido tiene que estar en estatus ABIERTO o CONFIRMADO para poder cancelarlo
            const msn = new Message('warning', 'El pedido tiene que estar en estatus ABIERTO o CONFIRMADO para poder cancelarlo');
            msn.showMessage();

        }

    }

    addEventDetallePartida() {

        document.querySelectorAll('.btn-detalle-partida').forEach(btn => {
            btn.addEventListener('click', (event) => {
                event.preventDefault()
                const PARTIDA = new Partida();
                PARTIDA.setPartida(event);
                console.log(event) ;

            })
        })
    }

    configByEStatus() {

        const ID_type = localStorage.getItem('id_user_type');
        const STATUS = document.querySelector("[name=estatus]").value.toUpperCase();
        // meter la actualizacion por tipo de uisuario aqui si no se encuentra el cambio uusuarios
        //la impresion del as facturas por dashboard
        let STATUS_JSON = {};
        console.log(ID_type);
        switch (ID_type) {
            case "1":
                STATUS_JSON = {

                    ABIERTO: {btnParcialidad: false, btnConfirm: true, btnOpen: true, btnClose: false, btnCancel: false, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    CONFIRMADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: false, btnCancel: false, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    COTIZACION: {btnParcialidad: true, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    CERRADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    CANCELADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: true},
                    ELIMINADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: true},
                    FACTURADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: false, btnNCredito: false, btnPdf: false},
                    PAGOS: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: false, btnNCredito: false, btnPdf: false},

                }
                
                break;
            case "2":
                STATUS_JSON = {

                    ABIERTO: {btnParcialidad: false, btnConfirm: true, btnOpen: true, btnClose: false, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    CONFIRMADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: false, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    COTIZACION: {btnParcialidad: true, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: false},
                    CERRADO: {btnParcialidad: false, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: true},
                    CANCELADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: true},
                    ELIMINADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: false, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf: true},
                    FACTURADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: true, btnNCredito: true, btnPdf: true},
                    PAGOS: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: true, btnNCredito: true, btnPdf: true},

                }
                
                break;

            case "3":// reinstalarlo
                STATUS_JSON = {

                    ABIERTO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CONFIRMADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CERRADO: {btnParcialidad: false, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: false, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    FACTURADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: false, btnNCredito: true, btnPdf:true},
                    PAGOS: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: false, btnPagos: false, btnNCredito: true, btnPdf:true},

                }
                
                break;

            case "4":
                STATUS_JSON = {

                    ABIERTO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CONFIRMADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CERRADO: {btnParcialidad: false, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    FACTURADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:true},
                    PAGOS: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:true},

                }
                
                break;
            case "5":
                STATUS_JSON = {

                    ABIERTO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CONFIRMADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    CERRADO: {btnParcialidad: false, btnConfirm: false, btnOpen: false, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:false},
                    FACTURADO: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:true},
                    PAGOS: {btnParcialidad: true, btnConfirm: true, btnOpen: true, btnClose: true, btnCancel: true, btnFacturar: true, btnverFact: true, btnPagos: true, btnNCredito: true, btnPdf:true},

                }
                
                break;
        
            default:
                break;
        }
        console.log(STATUS_JSON)
        
        document.querySelector("#btn-add-parcialidad").disabled = STATUS_JSON[STATUS].btnParcialidad;
        document.querySelector("#btn-confirm").disabled = STATUS_JSON[STATUS].btnConfirm;
        document.querySelector("#btn-open").disabled = STATUS_JSON[STATUS].btnOpen;
        document.querySelector("#btn-close").disabled = STATUS_JSON[STATUS].btnClose;
        document.querySelector("#btn-cancel").disabled = STATUS_JSON[STATUS].btnCancel;
        document.querySelector("#btn-facturar").disabled = STATUS_JSON[STATUS].btnFacturar;
        document.querySelector("#btn-ver-factura").disabled = STATUS_JSON[STATUS].btnverFact;
        //document.querySelector("#btn-add-pagos").disabled = STATUS_JSON[STATUS].btnPagos;
        document.querySelector("#btn-nota-credito").disabled = STATUS_JSON[STATUS].btnNCredito;
        document.querySelector("#btn-pdf").disabled = STATUS_JSON[STATUS].btnPdf;
        

    }


    async facturarPedido() {        

        let facturaData = { 
            facturaData: {
                cabecera: {
                    idVenta: document.querySelector('[name=pedido-id]').value,
                    rfcEmisor: document.querySelector('[name=rfc-emisor]').value,
                    rfcReceptor: document.querySelector('[name=rfc-receptor]').value,
                    nombreEmisor: document.querySelector('[name=razon-social-emisor]').value,
                    nombreReceptor: document.querySelector('[name=razon-social-receptor]').value,
                    subtotal: FORMATTER.numberFormat(document.querySelector('[name=subtotal]').value.split(' ')[1]),
                    immpuestoTrasladado: FORMATTER.numberFormat(document.querySelector('[name=iva]').value.split(' ')[1]),
                    impuestoRetenido: 0.00,
                    total: FORMATTER.numberFormat(document.querySelector('[name=total]').value.split(' ')[1]),
                    cfdi: document.querySelector('[name=cfdi]').value,
                    metodoPgo: document.querySelector('[name=metodo-pago]').value,
                    formaPago: document.querySelector('[name=forma-pago]').value,
                    tipoMoneda: document.querySelector('[name=tipo-moneda]').value,
                    cp: document.querySelector('[name=cp]').value,
                    tipoComprobante: document.querySelector('[name=tipo-comprobante]').value,
                    descuento: 0,
                    numCuenta: document.querySelector('[name=numero-cuenta]').value,

                },
                partidas: this.getPartidasParaFacturar(),               
            }
        }
        // aqui se revisa si se puede cerrar.
        // Se finaliza el proceso del pedido
        const FORMDATA = new FormData();
        FORMDATA.append('idpedido', document.querySelector('[name=pedido-id]').value)
        FORMDATA.append('idU', localStorage.getItem('user_id'))
        const DATA = await fetch('php/cerrar-pedido.php', { method: 'POST', body: FORMDATA });
        const PARSE_DATA = await DATA.json();

        //se revisa si se cerro o no
        if(PARSE_DATA.type == 'danger'){
            const msn = new Message(PARSE_DATA.type, PARSE_DATA.message);
            msn.showMessage();
            this.setInfoPedido();
        }else{
            // aqui se timbra si se cierra. 
            fetch('php/timbrar-factura.php', { method: 'POST', body: JSON.stringify(facturaData) })
            .then(data => data.json())
            .then(data => {

                window.open(`/siscontrol/assets/CFDI33_SIFEI/pdf_fact.php?NomArchXML=${data.path}&NomArchPDF=${document.querySelector('[name=pedido-id]').value}.pdf`);
                this.setInfoPedido();

            })
            $('#modal-datos-fiscales').modal('hide');
        }
        
    }

    getPartidasParaFacturar() {        

        let partidas = [];
        const ROWS = document.querySelectorAll('#tbl-detalle-sale tbody tr');
        ROWS.forEach(tr => {    
            let description = tr.children[2].textContent;
            let CatProdServ = tr.children[2].id;
            let CatUnidad = tr.children[3].id;
            description === 'Servicio de corte' ? CatProdServ = '01010101': CatProdServ;
            description === 'Servicio de corte' ? CatUnidad = '-': CatUnidad;
            partidas.push({
                claveProducto: tr.children[1].textContent,
                cantidad: tr.children[4].textContent,
                descripcion: description,
                CatProdServ: CatProdServ,
                CatUnid: CatUnidad,
                Udescript: tr.children[4].id,
                valorUnitario: tr.children[5].textContent.split(' ')[1],
                descuento: 0.00,
                importe: tr.children[6].textContent.split(' ')[1],
                Observaciones: tr.children[7].textContent,
            })
            
        })

        return partidas;

    }

    getTipoComprobante() {

        fetch('php/select-cat-tipocomprobante.php')
        .then(data => data.json())
        .then(data => {

            let tipo_comprobante = '<option value="" disabled>Selecciona un tipo</option>';
            data.forEach(element => {
                if (element.clave == 'I') {
                    tipo_comprobante += `<option value="${element.clave} selected">${element.Name}</option>`;    
                }               
                
            });
            document.querySelector('[name=tipo-comprobante]').innerHTML = tipo_comprobante;
            
        })

    }

    async applyPesoReal() {       
                
        const ID_ROW = document.querySelector('#id-row').value;
        const PESO_REAL = document.querySelector('#peso-real').value;  
        const PRECIO = FORMATTER.numberFormat(document.querySelector('#precio-producto').value.split(' ')[1]);
        const NEW_IMPORTE = PESO_REAL * PRECIO
        const NEW_VALUES = this.geValuesWhitPesoReal(ID_ROW, NEW_IMPORTE);
        console.log(PRECIO);
        console.log(NEW_IMPORTE);
        console.log(NEW_VALUES);
        const SUBTOTAL = NEW_VALUES.subtotal;
        const IVA = NEW_VALUES.iva;
        const TOTAL = NEW_VALUES.total;

        const FORM_DATA = {
            
           id_pedido: document.querySelector('[name=pedido-id]').value,
           id_partida: document.querySelector('#id-row').value,
           cantidad: document.querySelector('#peso-real').value,
           importe: NEW_IMPORTE,
           subtotal: SUBTOTAL,
           iva: IVA,
           total: TOTAL,

        }
        
        const DATA = await fetch('php/insert-nuevo-peso.php', { method: 'POST', body : JSON.stringify(FORM_DATA) });
        const RES = await DATA.json();
        if (RES.type === 'success') {

            document.querySelectorAll('#tbl-detalle-sale tbody tr').forEach(tr => {
                if(tr.children[1].id == ID_ROW) {
                    tr.children[4].textContent = PESO_REAL;
                    tr.children[6].textContent = `$ ${FORMATTER.currencyFormat(parseFloat(NEW_IMPORTE).toFixed(2))}`;
                    document.querySelector('[name=subtotal]').value = `$ ${FORMATTER.currencyFormat(parseFloat(SUBTOTAL).toFixed(2))}`;
                    document.querySelector('[name=iva]').value = `$ ${FORMATTER.currencyFormat(parseFloat(IVA).toFixed(2))}`;
                    document.querySelector('[name=total]').value = `$ ${FORMATTER.currencyFormat(parseFloat(TOTAL).toFixed(2))}`;
                }
            })           
            $('#modal-detalle-partida').modal('hide');
            
        }
        const MSN = new Message(RES.type, RES.message);
        MSN.showMessage();

    }

    geValuesWhitPesoReal(idRow, importe) {

        const TRS = document.querySelectorAll('#tbl-detalle-sale tbody tr');
        let subtotal = 0;
        let iva = 0;
        let importe_total = 0;

        TRS.forEach(tr => {
            if(tr.children[1].id == idRow) {
                importe_total = parseFloat(importe_total) + parseFloat(importe);
                iva = iva + parseFloat(importe) * .16;            
                //console.log(importe_total);
            } else {
                importe_total = parseFloat(importe_total) + parseFloat(tr.children[6].textContent.split(' ')[1].replace(',',''));
                iva = iva + parseFloat(tr.children[6].textContent.split(' ')[1].replace(',','')) * .16;                
                //console.log(importe_total);
            }
            subtotal = importe_total;
        })
        /*
        console.log("ipt");
        console.log(importe_total);
        console.log("sbt");
        console.log(subtotal);
        console.log("iva");
        console.log(iva);
        */
        let data = {     

            subtotal : subtotal,
            iva: iva,
            total: parseFloat(subtotal) + parseFloat(iva)

        };

        return data;

    }

    imprimirPedidoDetalle() {
        const PARAMS = new URLSearchParams(window.location.search);
        const id = PARAMS.get('idPedido');

        window.open(`/siscontrol/administrativo/detalle%20pedido/php/get-pedido-pdf.php?Pedido=${id}`);
        const msn = new Message('success', 'Se imprimió el pedido correctamente ');
        msn.showMessage();
    }
}