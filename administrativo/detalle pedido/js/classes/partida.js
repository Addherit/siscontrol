import { Formatter } from "../../../../assets/js/plugins/currency-format.js";

const FORMATTER = new Formatter();

export class Partida {

    async getPartida(event) {

        const TR = event.currentTarget.parentElement.parentElement;
        console.log(TR);
        const FORMDATA = new FormData();
        FORMDATA.append('id_item', TR.children[1].id)
        const DATA = await fetch('php/select-partida.php', { method: 'POST', body: FORMDATA });
        return await DATA.json();        

    }

    async setPartida(event) {

        const DATA = await this.getPartida(event);
        document.querySelector('#id-row').value = DATA[0].ID_VtaD;
        document.querySelector('#list-Codes').value = DATA[0].Clave_Prod;
        document.querySelector('#descripcion-Codes').value = DATA[0].Descripcion;
        document.querySelector('#list-prices').value = DATA[0].Lista_precio;
        document.querySelector('#precio-producto').value = `$ ${FORMATTER.currencyFormat(DATA[0].Precio)}`;
        document.querySelector('#precio-corte').value = `$ ${FORMATTER.currencyFormat(DATA[0].Precio_corte)}`;
        document.querySelector('#unidad').value = DATA[0].Unidad;
        document.querySelector('#cantidad').value = DATA[0].ltsPz;
        document.querySelector('#cortes').value = DATA[0].Corte;
//        document.querySelector('#almacen').value = DATA[0].Almacen;        


        /////////////

        document.querySelector('[name=largo]').value = DATA[0].Largo;
        document.querySelector('[name=ancho]').value = DATA[0].Ancho;
        document.querySelector('[name=espesor]').value = DATA[0].Espesor;
        document.querySelector('[name=variante-fija]').value = DATA[0].Variante_fija;
        document.querySelector('[name=diametro]').value = DATA[0].Diametro;
        document.querySelector('[name=diametro-externo]').value = DATA[0].Diametro_ext;
        document.querySelector('[name=diametro-interno]').value = DATA[0].Diametro_int;
        document.querySelector('[name=medidas-placas]').value = DATA[0].Medida_placa;
        document.querySelector('[name=peso-real]').value = DATA[0].PesoReal;
        document.querySelector('[name=peso-teorico]').value = parseFloat(DATA[0].PesoTeorico).toFixed(2);

        this.choseCategory(DATA[0].Formula);
        this.hasMerma(DATA[0].Formula, DATA[0].Medida_placa, DATA[0].Largo, DATA[0].Ancho);
        localStorage.setItem('cert_name', DATA[0].FIlecertificado);
        this.mermaSelected(DATA[0].Merma)
        console.log(DATA[0].Almacen);
        this.setAlmacen(DATA[0].Almacen);
        
    }

    getCortes(event) {
        
        let cortes = 0;
        const ROW = event.currentTarget.parentElement.parentElement;

        if (ROW.nextElementSibling) {
            if (ROW.nextElementSibling.children[2].textContent == 'Servicio de corte') {
                cortes = ROW.nextElementSibling.children[4].textContent;    
            }
        }

        return cortes;

    }

    mermaSelected(hasMerma) {
        
        const SECTION_MERMA = document.querySelector('.merma-section');
        switch (hasMerma.toLowerCase()) {
            case 'si':
                SECTION_MERMA.hidden = true;
                break;
            case 'no':
                SECTION_MERMA.hidden = false;
                break;                   
        }
        
    }

    hasMerma(formula, medida_placa, largo, ancho) {

        if(formula.toLowerCase() == 'rojo' || formula.toLowerCase() == 'aquaplaca' || formula.toLowerCase() == 'amarilloplaca' || formula.toLowerCase() == 'placany' ) {
            document.querySelector('.merma-section').hidden = false;
            document.querySelector("#btn-apply-p-real").disabled = true;
            const cant = document.querySelector('#cantidad').value;
            const largo_placa = medida_placa.toLowerCase().split(' x ')[0].replace('"', '');
            const ancho_placa = medida_placa.toLowerCase().split(' x ')[1].replace('"', '');

            const mayor_medida = Math.max(largo, ancho) * cant;
            const menor_medida = Math.min(largo, ancho) * cant;

            const mayor_placa = Math.max(largo_placa, ancho_placa);
            const menor_placa = Math.min(largo_placa, ancho_placa);            
            if(mayor_medida < menor_placa) {
                
                let lado1 = menor_placa - mayor_medida;
                let lado2 = mayor_placa - menor_medida;
                
                // opcion 1 merma
                let optionA = [
                    `${lado1}" x ${mayor_placa}"`,
                    `${lado2}" x ${mayor_medida}"`
                ]

                // opcion2 merma
                let optionB = [
                    `${lado1}" x ${menor_medida}`,
                    `${lado2}" x ${menor_placa}"`
                ]                  

                this.visualizarMerma(optionA, optionB)


            } else if(mayor_medida > menor_placa) {

                let lado1 = mayor_placa - mayor_medida;
                let lado2 = menor_placa - menor_medida;
               
                 // opcion 1 merma
                 let optionA = [
                    `${lado1}" x ${menor_placa}"`,
                    `${lado2}" x ${mayor_medida}"`
                ]

                // opcion2 merma
                let optionB = [
                    `${lado1}" x ${menor_medida}"`,
                    `${lado2}" x ${mayor_placa}"`
                ]

                this.visualizarMerma(optionA, optionB)
                
            } else if (mayor_medida == menor_placa) {

                let lado1 = mayor_placa - menor_medida;
                


                 // opcion 1 merma
                 let optionA = [
                    `${lado1}" x ${menor_placa}"`,                    
                ]
                this.visualizarMerma(optionA, {})               
               
            }




        } else {
            document.querySelector('.merma-section').hidden = true;
        }
    }

    visualizarMerma(optionA, optionB) {

        let mermas = ``;
        if (optionA.length) {
            mermas += `
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Opción A</h5>
                        <form id="formOptionA">`
                        
                            optionA.forEach(merma => {

                                mermas += ` 
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" data-merma="${merma.replace('"', '')}">
                                    <label class="form-check-label" for="merma2">${merma}</label>
                                </div>`
                            });                                                    
                           
                            mermas += ` 
                            <button type="submit" class="btn btn-info mt-3">
                                <span class="material-icons align-middle">check</span>
                                Guardar Merma
                            </button>                                        
                        </form>
                    </div>
                </div>
            </div>`;            
        }

        if (optionB.length) {
            mermas += `
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Opción B</h5>
                        <form id="formOptionB">`
                        
                            optionB.forEach(merma => {

                                mermas += ` 
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" data-merma="${merma.replace('"', '')}">
                                    <label class="form-check-label" for="merma2">${merma}</label>
                                </div>`

                            });                                                    
                           
                            mermas += ` 
                            <button type="submit" class="btn btn-info mt-3">
                                <span class="material-icons align-middle">check</span>
                                Guardar Merma
                            </button>                                        
                        </form>
                    </div>
                </div>
            </div>`;            
        }

        document.querySelector('.card-mermas').innerHTML = mermas;
        this.handleFormsMermas()

        
    }

    setInfoCertificado(DATA) {

        document.querySelector('#tbl-certificado #tbl-peso').textContent = DATA[0].PesoReal
        document.querySelector('#tbl-certificado #tbl-pieza').textContent = DATA[0].Cantidad
        document.querySelector('#tbl-certificado #tbl-code-material').textContent = DATA[0].Clave_Prod
        document.querySelector('#tbl-certificado #tbl-descripcion-material').textContent = DATA[0].Descripcion
    
    }

    handleFormsMermas() {

        const FORM_A = document.querySelector('#formOptionA');
        const FORM_B = document.querySelector('#formOptionB');
        if (FORM_A) {
            let arrA = []
            FORM_A.addEventListener('submit', (e) => {
                e.preventDefault();
                for (const key in FORM_A.elements) {
                   
                    if (FORM_A.elements[key].checked) {                        
                        arrA.push(FORM_A.elements[key].dataset.merma);
                    }
                }

                this.saveMerma(arrA);

              
                   
                
            })
        }

        if (FORM_B) {
            let arrB = [];
            FORM_B.addEventListener('submit', (e) => {
                e.preventDefault();
                for (const key in FORM_B.elements) {
                   
                    if (FORM_A.elements[key].checked) {
                        arrB.push(FORM_A.elements[key].dataset.merma);
                    }
                }

                this.saveMerma(arrB);
                   
                
            })
        }

    }

    saveMerma(mermas) {


        let req = {
            header: {
                codigo: document.querySelector('#list-Codes').value,
                almacen: document.querySelector('#almacen').value,
                placa:  document.querySelector('#medida-placa').value,
                id_partida:  document.querySelector('#id-row').value,
            },
            body: {
                mermas: mermas
            }
        }

        fetch('php/insert-merma.php', {method: 'POST', body: JSON.stringify(req)})
        .then(data => data.json())
        .then(data => {
            const msn = new Message(data.type, data.message);
            msn.showMessage(); 
            data.type === 'success' ? document.querySelector('.merma-section').hidden = true : false;       
        })
    }

    async getPartidas(id_pedido) {

        const FORMDATA = new FormData();
        FORMDATA.append('id_pedido', id_pedido);
        const DATA = await fetch('php/select-partidas.php', { method: 'POST', body: FORMDATA });
        return await DATA.json();

    }

    async setPartidas() {

        let lineas = ``;
        const PARTIDAS = await this.getPartidas(document.querySelector('[name=pedido-id]').value);

        PARTIDAS.forEach(partida => {
            lineas += `
                <tr>
                    <td id="${partida.ID_VtaD}">${partida.Clave_Prod}</td>
                    <td id="${partida.Precio}">${partida.Cantidad}</td>
                    <td>${partida.Corte}</td>
                    <td><input type="text" class="form-control form-control-sm input-entrega-parcialidad" style="border: none !important; width: 100px; background: #fefeda" placeholder="0"></td>
                    <td>${partida.Restante}</td>                    
                    <td><input type="text" class="form-control form-control-sm" placeholder="Escribe una observación" style="border: none !important; background: #fefeda"></td>
                </tr>
            `
        });
        document.querySelector('#tbl-detalle-partidas tbody').innerHTML = lineas;
        this.addEventEntregaParcialidadValidador();
        this.hasPendientePorEntrega();
    }

    addEventEntregaParcialidadValidador() {

        document.querySelectorAll('.input-entrega-parcialidad').forEach(input => {
            // valida que la cantidad escrita en entrega sea menor a la cantidad restante
            input.addEventListener('keyup', (event) => {
                const ENTREGA = parseInt(event.currentTarget.value);
                const RESTANTE = parseInt(event.currentTarget.parentElement.parentElement.children[4].textContent);
                if (ENTREGA > RESTANTE) {
                    const msn = new Message('warning', 'La entrega es mayor al restante, favor de corregir');
                    msn.showMessage();
                    document.querySelector('#btn-create-parcialidad').disabled = true;
                } else {
                    this.validarTodasLasEntregas();
                    document.querySelector('#btn-create-parcialidad').disabled = false;
                }
            })
        })
        
    }

    hasPendientePorEntrega() {

        // valida que la cantidad de entrega de todas las partidas sean en cero
        const TRS = document.querySelectorAll('#tbl-detalle-partidas tbody tr');
        const BTN_CREATE_PARC =  document.querySelector('#btn-create-parcialidad')
        let restante = 0
        TRS.forEach(tr => restante = restante + parseInt(tr.children[4].textContent))
        restante ? BTN_CREATE_PARC.disabled = false : BTN_CREATE_PARC.disabled = true;

    }

    validarTodasLasEntregas() {

        document.querySelectorAll('.tbl-detalle-partidas tbody tr').forEach(tr => {
            const ENTREGA = parseInt(tr.children[3].childElementCountldren[0].value);
            const RESTANTE = parseInt(tr.children[4].childElementCountldren[0].value);
            let count = 0;
            if (ENTREGA > RESTANTE) {
                count = count + 1;
            }

            if (count) {
                document.querySelector('#btn-create-parcialidad').disabled = true;
            }            
                
        })
    }


    async choseCategory(formula) {
       
        this.cleanInputsCategory();  
        const CATEGORIAS = {
          azul: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          naranja: ['espesor', 'ancho', 'variante-fija', 'largo', 'peso-teorico'],
          pistache: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          negro: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          blanco: ['diametro-externo', 'diametro-interno', 'largo', 'peso-teorico'],
          amarillo: ['espesor', 'ancho', 'variante-fija', 'largo', 'peso-teorico'],
          morado: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          rey: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          gris: ['diametro', 'variante-fija', 'largo', 'peso-teorico'],
          verde: ['diametro-externo', 'diametro-interno', 'variante-fija', 'largo', 'peso-teorico'],
          placany: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
          rojo: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
          acuaplaca: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
          amarilloplaca: ['placa','espesor', 'ancho', 'largo', 'peso-teorico'],
          default: []
        };        
        this.showInputsCategory(CATEGORIAS[formula]);    
  
      }   
  
    showInputsCategory(formula) {
  
        if (formula.length) {
            formula.forEach(input => {
            const ELEMENT_DOM = document.querySelector(`.${input}`);
            ELEMENT_DOM.hidden = false;            
            });
        } else {
            const msn = new Message('info', 'Producto sin categoria');
            msn.showMessage();
        }   
  
    }
  
    cleanInputsCategory() {
            
        const INPUTS = ['diametro', 'variante-fija', 'largo', 'espesor', 'ancho', 'diametro-externo', 'diametro-interno', 'peso-teorico']
        INPUTS.forEach(input => {
            document.querySelector(`.${input}`).hidden = true;
        })

    }

    imprimirPartidaDetalle() {

        html2canvas(document.querySelector("#section-to-imprimir")).then(canvas => {
            let enlace = document.createElement('a');
            enlace.download = "Detalle de la partida.png";      
            enlace.href = canvas.toDataURL();      
            enlace.click();
            const msn = new Message('success', 'Se imprimió la partida correctamente ');
            msn.showMessage();
        });
        
    }

    async setAlmacen(token){
        console.log(token);
        let DATA = await fetch('php/select-almacenes.php');
        let ALMACENES = await DATA.json();
        let option = `<option value="" disabled selected>Selecciona uno</option>`;
        ALMACENES.forEach(almacen => {
            option += `<option value="${almacen.Id}">${almacen.Nombre}</option>`;
        })
        console.log(option);
        document.querySelector('[name=almacen]').innerHTML = option;

        switch (token) {
			case '1':
				document.getElementById("almacen").selectedIndex = 1;	
				break;
			case '2':
				document.getElementById("almacen").selectedIndex = 2;	
				break;
		
			case '3':
				document.getElementById("almacen").selectedIndex = 3;	
				break;
			case '323':
				document.getElementById("almacen").selectedIndex = 0;	
				break;
		}


    }

    UpdateAlmacen() {

        var sel = document.getElementById("almacen");
        var ida = sel.options[sel.selectedIndex].value;
        let idr = document.querySelector('#id-row').value;
        console.log(ida);
        console.log(idr);

        const FORMDATA = new FormData();
        FORMDATA.append('id_almacen', ida )
        FORMDATA.append('id_partida', idr )
        fetch('php/update-almacen.php', {method: 'POST', body: FORMDATA})
        .then(data => data.json())
        .then(data => {
            const msn = new Message(data.type, data.message);
            msn.showMessage(); 
        })

    }
}