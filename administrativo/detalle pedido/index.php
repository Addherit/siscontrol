<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
            <?php include "./modales/modal-create-certificado.html"?>
            <?php include "./modales/modal-detalle-partida.html"?>
            <?php include "./modales/modal-datos-fiscales.html"?>
            <?php include "./modales/modal-nota-credito.html"?>
            <?php include "./modales/modal-parcialidad.html"?>
            <?php include "./modales/modal-choise.html"?>
            <?php include "./modales/modal-pagos.html"?>
            <?php include "../../sidebar/index.html"?>
            <?php include "../../assets/modales/modal-spinner.html"?>
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">DETALLE PEDIDO</span>
                            <span class="h4 text-secondary" id="title-user" name="tu"></span>
                        </div>                        
                    </div>
                    <hr>                   
                    <form id="new-sale-note"> 
                        <div class="row">
                            <div class="col-md-8">                    
                                <div class="row">
                                    <label for="cliente" class="col-md-5 col-lg-2 col-form-label">Cliente</label>
                                    <div class="col-md-7 col-lg-9">                                        
                                        <input type="text" class="form-control form-control-sm" name="client" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-md-5 col-lg-2 col-form-label">E-mail</label>
                                    <div class="col-md-7 col-lg-9">
                                        <input type="text" class="form-control form-control-sm" name="email" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="agente" class="col-md-5 col-lg-2 col-form-label">Agente</label>
                                    <div class="col-md-7 col-lg-9">
                                        <input type="text" class="form-control form-control-sm" name="agente" readonly>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <label for="id" class="col-md-5 col-lg-3 col-form-label">Pedido</label>
                                    <div class="col-md-7 col-lg-9">
                                        <input type="text" class="form-control form-control-sm text-center" name="pedido-id" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="fecha" class="col-md-5 col-lg-3 col-form-label">Fecha</label>
                                    <div class="col-md-7 col-lg-9">
                                        <input type="text" class="form-control form-control-sm text-center" name="fecha" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="estatus" class="col-md-5 col-lg-3 col-form-label">Estatus</label>
                                    <div class="col-md-7 col-lg-9">
                                        <input type="text" class="form-control form-control-sm text-center" name="estatus" readonly>
                                    </div>
                                </div>                       
                            </div>
                        </div>   
                        <div class="row pb-1">
                            <div
                             class="col-12 text-right">
                                <button type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#modal-nota-credito" id="btn-nota-credito"><span class="material-icons align-middle">picture_as_pdf</span>Nota de crédito</button>
                                <button type="button" class="btn btn-danger mb-2" id="btn-ver-factura"><span class="material-icons align-middle">picture_as_pdf</span>Ver Factura</button>
                                <button type="button" class="btn btn-success mb-2" data-toggle="modal" data-target="#modal-pagos" id="btn-add-pagos"><span class="material-icons align-middle">add</span>Pagos</button>
                                <button type="button" class="btn btn-warning mb-2" data-toggle="modal" data-target="#modal-parcialidad" id="btn-add-parcialidad"><span class="material-icons align-middle">add</span> Crear Parcialidad</button>
                                <!-- <button type="button" class="btn btn-info mb-2" data-toggle="modal" data-target="#modal-add-item" disabled><span class="material-icons align-middle">add</span> Agregar Producto</button> -->
                            </div>
                        </div>                    
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table id="tbl-detalle-sale" class="table table-hover table-sm table-dataTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>                                       
                                            <th>Clave</th>
                                            <th>Descripción</th>
                                            <th>Ud</th>
                                            <th>Cant</th>
                                            <th>Precio</th>
                                            <th>Importe</th>
                                            <th>Observaciones</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5 offset-7">
                                <div class="row">
                                    <label for="subtotal" class="col-sm-4 col-form-label">Subtotal</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="subtotal" readonly>
                                    </div>
                                </div> 
                                <div class="row">
                                    <label for="iva" class="col-sm-4 col-form-label">IVA 16%</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="iva" readonly>
                                    </div>
                                </div> 
                                <div class="row">
                                    <label for="total" class="col-sm-4 col-form-label">Total</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-center" name="total" readonly>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="button" class="btn btn-info mb-2" id="btn-pdf">
                                    <span class="material-icons align-middle">picture_as_pdf</span>
                                    PDF
                                </button>
                                <button type="button" class="btn btn-info mb-2" id="btn-confirm">
                                    <span class="material-icons align-middle">done_all</span>
                                    Confirmar
                                </button>
                                <button type="button" class="btn btn-info mb-2" id="btn-open">
                                    <span class="material-icons align-middle">done</span>
                                    Abrir
                                </button>
                                <button type="button" class="btn btn-info mb-2" id="btn-facturar" data-toggle="modal" data-target="#modal-datos-fiscales">
                                    <span class="material-icons align-middle">picture_as_pdf</span>
                                    Facturar
                                </button>
                                <button type="button" class="btn btn-success mb-2" id="btn-close">
                                    <span class="material-icons align-middle">library_add_check</span>
                                    Cerrar</button>
                                <button type="button" class="btn btn-danger mb-2" id="btn-cancel">
                                    <span class="material-icons align-middle">block</span>
                                    Cancelar
                                </button>
                                <!-- <button class="btn btn-info">Facturar</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
    <script src="/siscontrol/administrativo/detalle pedido/js/main.js" type="module"></script>

      
     
</html>