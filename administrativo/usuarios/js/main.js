import { Usuario } from "./classes/user.js";
const USUARIO = new Usuario();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-disabled').addEventListener('click', () => USUARIO.disabledUser());
document.querySelector('#btn-activate').addEventListener('click', () => USUARIO.activateUser());
document.querySelector('#btn-new-user').addEventListener('click', () => newUser());
document.querySelector('#btn-edit-user').addEventListener('click', () => editUser());
document.querySelector('#btn-confirm').addEventListener('click', () => confirmAction());
document.querySelector('#btn-see-password').addEventListener('click', () => seePass());


function init() {
    
    USUARIO.setUsers();
    USUARIO.getUsersType();

}

function newUser() {

    document.querySelector('#completing-msg').innerHTML = `de agregar el nuevo usuario`;
    localStorage.setItem('func-todo', 'putUser');
    
}

function editUser() {
    
    document.querySelector('#completing-msg').innerHTML = `editar la información del usuario`;
    localStorage.setItem('func-todo', 'updateUser');

}

function confirmAction() {
    
    actionsConfirm(localStorage.getItem('func-todo'));
    $('#modal-confirmation').modal('hide');

}

function actionsConfirm(action) {
    
    switch (action) {
        case 'putUser':
            USUARIO.putUser();
            break;
        case 'updateUser':
            USUARIO.updateUser();
            break;
    }

}

function seePass() {

    const INPUT_PASS = document.querySelector('[name=edit-password]');
    INPUT_PASS.type === 'password' ? INPUT_PASS.type = 'text' : INPUT_PASS.type = 'password';

}