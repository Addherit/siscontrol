import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();

export class Usuario {
    
    constructor() {
        this.idUser = ``;
    }

    putUser() {

        const FORM_DATA = new FormData(document.querySelector("#form-new-user"))        
        fetch('php/insert-user.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-add-user').modal('hide');
            this.setUsers();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    updateUser() {

        const FORM_DATA = new FormData(document.querySelector("#form-edit-user"))     
        FORM_DATA.append('edit-id', this.idUser)   
        fetch('php/update-user.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-edit-user').modal('hide');
            this.setUsers();
            const MSN = new Message('success', 'Se actualizó la información del usuario correctamente');
            MSN.showMessage();
        })

    }

    disabledUser() {
        
        const ID_USER = this.idUser

        if (ID_USER == localStorage.getItem('user_id')) {

            const MSN = new Message('danger', 'No se puede desactivar el usuario mientras esté en sesión');
            MSN.showMessage();
            
        } else {

            const FORM_DATA = new FormData()
            FORM_DATA.append('id_user', ID_USER);
            fetch('php/desactivar-user.php', { method: 'POST', body: FORM_DATA })
            .then(data => data.json())
            .then(data => {            
                $('#modal-disabled-confirmation').modal('hide');
                this.setUsers();
                const MSN = new Message(data.type, data.message);
                MSN.showMessage();
            })

        }

    }

    activateUser() {

        const ID_USER = this.idUser        
        const FORM_DATA = new FormData()
        FORM_DATA.append('id_user', ID_USER);
        fetch('php/activar-user.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setUsers();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    getUsers() {

        return fetch('php/select-users.php')
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data 
        })
        
    }

    async setUsers() {

        SPINNER.showSpinner();
        const DATA = await this.getUsers();
        let lines = ``;

        DATA.forEach(user => {
            const STATUS = this.getColorStatus(user.status);
            const BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(user.status, user.userId);
            lines += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-warning btn-edit-user" title="Editar usuario" data-toggle="modal" data-target="#modal-edit-user"><span class="material-icons align-middle">edit</span>Editar</button>
                    </td>
                    <td>${user.userId}</td>
                    <td style="white-space: nowrap">${user.Nombre}</td>
                    <td>${user.Tipo}</td>
                    <td>${user.name}</td>
                    <td>${user.pass}</td>                    
                    ${STATUS}
                </tr>
            `
        });

        document.querySelector('#list-users tbody').innerHTML = lines;
        const dataTable = new DataTable('list-users');
    
        // Itera todos los botones de eliminado para asignar el evento click.
        this.addEventClickToBtnActivateDisabled();
        this.addEventClickToBtnEdit();
        dataTable.initDataTable();
    }

    addEventClickToBtnActivateDisabled() {

        document.querySelectorAll('.btn-activate-disabled').forEach(button => {        
            button.addEventListener('click', (event) => {
                const NAME_USER = event.currentTarget.parentElement.parentElement.children[2].textContent;
                document.querySelector('#disabled-name').innerHTML = `el usuario`;
                document.querySelector('#id-to-disabled').innerHTML = NAME_USER;
                document.querySelector('#confirm-name').innerHTML = `el usuario`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_USER;
                this.idUser = event.currentTarget.parentElement.parentElement.children[1].textContent;
            })
        });

    }

    addEventClickToBtnEdit() {

        document.querySelectorAll('.btn-edit-user').forEach(button => {
            button.addEventListener('click', (event) => {
                this.idUser = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoUserById();
            })
        });

    }

    setInfoUserById() {

        const FORMDATA = new FormData()
        FORMDATA.append('id_user', this.idUser)
        fetch('php/select-user.php', { method: 'POST', body: FORMDATA })
        .then(data => data.json())
        .then(data => {
            document.querySelector("[name=edit-user-id]").value = data[0].userId
            document.querySelector("[name=edit-user-name]").value = data[0].Nombre
            document.querySelector("[name=edit-user]").value = data[0].name
            document.querySelector("[name=edit-password]").value = data[0].pass
            document.querySelector("[name=edit-user-type]").value = data[0].Tipo_usuario
        })

    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Inactivo: `<td>${status}</td>`,
            Moroso: `<td class="text-danger">${status}</td>`
        }
        return COLOR_STATUS[status];

    }



    getBtnActivateDisabled(status, id) {

        if (status == 'Inactivo') {
            return `
            <button type="button" id="${id}" class="btn btn-sm btn-success btn-activate-disabled" title="Activar Usuario" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else {
            return `
            <button type="button" id="${id}" class="btn btn-sm btn-danger btn-activate-disabled" title="Desactivar Usuario" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Desactivar
            </button>`
        }
       
    }

    getUsersType() {

        fetch('php/select-type.php')
        .then(data => data.json())
        .then(data => {
            let types = '<option value="" selected disabled>Selecciona un tipo</option>';
            data.forEach(type => types += `<option value="${type.id}">${type.Tipo}</option>`);
            document.querySelector('[name=user-type]').innerHTML = types;
            document.querySelector('[name=edit-user-type]').innerHTML = types;
        })

    }

}