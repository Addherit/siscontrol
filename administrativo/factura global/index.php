<?php

$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -30 days" ) );
?>
<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
          <?php include "../../assets/modales/modal-delete-confirmation.html" ?>        
          <?php include "../../sidebar/index.html"?> 
          <?php include "../../assets/modales/modal-spinner.html" ?>
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">Factura Publico</span>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                          <label>Fecha Inicio:</label>
			                    <input type="date"  id="FechaI" value="<?php echo $FechaInicio; ?>">
                        </div>                        
                        <div class="form-group col-4">
                          <label>Fecha Final:</label>
			                    <input type="date"  id="FechaF" value="<?php echo $today; ?>">
                        </div>                        
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-12 table-responsive">
                          <table id="list-notes" class="table table-hover table-dataTable" width="100%">
                            <thead>
                              <tr>
                                <th class="th-sm"></th>
                                <th class="th-sm">Id</th>
                                <th class="th-sm">Cliente</th>
                                <th class="th-sm">Asesor</th>
                                <th class="th-sm">Fecha</th>
                                <th class="th-sm">Subtotal</th>
                                <th class="th-sm">iva</th>
                                <th class="th-sm">Total</th>
                              </tr>
                            </thead>                            
                           <tbody></tbody>
                          </table>
                        </div>
                    </div>
                    
                </div>
                  <div class="row">
                    <div class="col-11">
                      <button type="button" class="btn btn-success float-right" id="btn-timbre">
                          <span class="material-icons align-middle">picture_as_pdf</span>
                          Facturar
                      </button>                   
                    </div>
                  </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="/siscontrol/administrativo/factura global/js/main.js" type="module"></script>


     
</html>