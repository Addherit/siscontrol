`use strict`;

import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();

document.addEventListener('DOMContentLoaded', () => Init())
document.querySelector('#FechaI').addEventListener('change', () => buscarPedidosFecha());
document.querySelector('#FechaF').addEventListener('change', () => buscarPedidosFecha());
document.querySelector('#btn-timbre').addEventListener('click', () => send());


async function Init() {

	buscarPedidosFecha();
}

function buscarPedidosFecha(){

    SPINNER.showSpinner();
    const DATE_I = document.querySelector('#FechaI').value;
    const DATE_F = document.querySelector('#FechaF').value;
    console.log(DATE_I);
    console.log(DATE_F);
	// info
    const FORM_DATA = new FormData()
    FORM_DATA.append('fechai', DATE_I)
    FORM_DATA.append('fechaf', DATE_F)
    
    let pedidos = ``;
    fetch('php/select-pedidos.php', { method: 'POST', body: FORM_DATA })
    .then(data => data.json())
    .then(data => {
	console.log(data);

	data.forEach(pedido => {
		pedidos += `
		<tr>
		<td class="static"><input type="checkbox"></td>
		<td class="id">${pedido.ID_Venta}</td>
		<td class="cliente">${pedido.Cve_Cte}</td>
		<td class="agente">${pedido.Nom_Agente}</td>
		<td class="fecha">${pedido.Fecha_Vta}</td>
		<td class="subtotal">${pedido.Subtotal}</td>
		<td class="iva">${pedido.IVA}</td>
		<td class="total">${pedido.Total}</td>
		</tr>
		`
	});
    	document.querySelector('#list-notes tbody').innerHTML = pedidos;
    })
    setTimeout(() => SPINNER.hideSpinner(), 1000);
}

function send(){
	//get the selected rows
	var row_afectadas_y_sus_datos = [];     
        var indexrow = 0;
        $('#list-notes .static').each((index, element) => {
        
            if (element.childNodes[0].checked) {

                indexrow++;
                var linea_single = {};
                element.parentNode.childNodes.forEach((element, index) => {                                                                                    
                    if (element.className === 'id') { linea_single["id"] = element.textContent }
                    if (element.className === 'cliente') { linea_single["cliente"] = element.textContent }
                    if (element.className === 'agente') { linea_single["agente"] = element.textContent }
                    if (element.className === 'total') { linea_single["total"] = element.textContent }
                    if (element.className === 'iva') { linea_single["iva"] = element.textContent }
                    if (element.className === 'subtotal') { linea_single["subtotal"] = element.textContent }
                })
                row_afectadas_y_sus_datos.push(linea_single);
                
            }                
        })
	//debug
	console.log(row_afectadas_y_sus_datos);
	//send to timbrar
	const FORM_DATA = new FormData()
	FORM_DATA.append('filas', JSON.stringify(row_afectadas_y_sus_datos))
	
	let pedidos = ``;
	fetch('php/timbrar-factura.php', { method: 'POST', body: FORM_DATA })
	.then(data => data.json())
	.then(data => {
		console.log(data);
                window.open(`/siscontrol/assets/CFDI33_SIFEI/pdf_fact.php?NomArchXML=${data.path}&NomArchPDF=${data.idfact}.pdf`);
	})

}
