<?php 
header('Content-type: application/json');
include_once('../../assets/db/conexion.php');

//if($_SERVER["REQUEST_METHOD"] == "POST") {
try {
    $id = $_POST['id_user'];
    $ida = $_POST['id_agente'];
    $estatus = $_POST['status'];
    $fechai = $_POST['fecha-ini'];
    $fechaf  = $_POST['fecha-end'];
    $tf = 0;

    if(isset($fechai) && !is_null($fechai) && $fechai !=''){
        if(isset($fechaf) && !is_null($fechaf) && $fechaf !=''){
            $tf =1;
        }
    }
    
    //info de la tabla
    if(isset($ida) && !is_null($ida) && $ida !=''){ 
        if($ida == 'Todos'){// si no tiene filtro de agente
            if(isset($estatus) && !is_null($estatus) && $estatus != ''){
                if($estatus != 'Todos'){
                    if($tf == 1){
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where v.Estatus = '$estatus' and v.Fecha_Vta between '$fechai' and '$fechaf'  GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }else{
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where v.Estatus = '$estatus' GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }
                }else{
                    if($tf == 1){
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where  v.Fecha_Vta between '$fechai' and '$fechaf' GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }else{
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente  GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }
                }
            }
        }else{ // si tiene filtro de agente.
            if(isset($estatus) && !is_null($estatus) && $estatus != ''){
                if($estatus != 'Todos'){
                    if($tf == 1){
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where a.ID_Agente = $ida and v.Estatus = '$estatus' and v.Fecha_Vta between '$fechai' and '$fechaf'  GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }else{
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where a.ID_Agente = $ida and v.Estatus = '$estatus' GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }
                }else{
                    if($tf == 1){
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where a.ID_Agente = $ida  and v.Fecha_Vta between '$fechai' and '$fechaf' GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }else{
                        $sqli = "SELECT a.`ID_Agente`,a.`Nom_Agente`,count(v.ID_Venta) as cuentaTotal, TRUNCATE(Sum(v.Total),2) as ventaTotal FROM `agente` a inner join venta v on a.`ID_Agente` = v.Cve_Agente where a.ID_Agente = $ida GROUP by a.`ID_Agente`,a.`Nom_Agente` ";
                    }
                }
            }
        }
    }
    $result = $con->query($sqli)->fetchAll(PDO::FETCH_ASSOC );
//    $result['query'] = $sqli;

} catch (PDOException  $e) {
    $result = ["mensaje" => "Error: ".$e];
}

echo json_encode($result);
    //comentario de revision
//}
?>