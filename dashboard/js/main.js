import { Agente } from "./classes/agente.js";
import { Estado } from "./classes/estado.js";
import { Grafica } from "./classes/grafica.js";
import { Pedido } from "./classes/pedido.js";
import { Formatter } from "../../assets/js/plugins/currency-format.js";
import { Cliente } from "./classes/cliente.js";

const AGENTE = new Agente();
const PEDIDO = new Pedido();
const CHART = new Grafica();
const ESTADOS = new Estado();
const CLIENTE = new Cliente();


document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-cunsultar-pedidos').addEventListener('click', () => GetChartByFiltro());

async function init() {
    
    AGENTE.setAgentes();
    ESTADOS.setEstados();
    CLIENTE.setClientes();
    setTimeout(async () => {
        const DATA = await PEDIDO.getPedidos();
        CHART.getChartLineal(DATA);
        getTotalPedidos(DATA);
        getVentaTotal(DATA);
    }, 1000);

}

async function GetChartByFiltro() {

    const DATA = await PEDIDO.getPedidos();
    const GRAFICA = document.querySelector('#chartBar');
    CHART.destroyChart(GRAFICA)
    CHART.getChartLineal(DATA);

}

function getTotalPedidos(data) {

    let totalPedidos = 0;
    data.forEach(agente => {
        totalPedidos = totalPedidos + parseInt(agente.cuentaTotal)        
    });    
    document.querySelector('#total-pedidos').innerHTML = totalPedidos;

}

function getVentaTotal(data) {

    let ventaTotal = 0;
    data.forEach(agente => {
        ventaTotal = ventaTotal + parseInt(agente.ventaTotal)        
    });    
    const FORMATTER = new Formatter();            
    document.querySelector('#venta-total').innerHTML =`$ ${FORMATTER.currencyFormat(ventaTotal)}`;
    
}


