export class Cliente { 

    async getClientes() {

        const DATA = await fetch('php/select-clientes.php');
        return await DATA.json();

    }

    async setClientes() {

        const DATA = await this.getClientes();
        let options = `<option value="Todos" selected>Todos los clientes</option>`;
        DATA.forEach(cliente => {
            options += `<option value="${cliente.ID_Cliente}">${cliente.Nombre_Cte}</option>`;
        });
        document.querySelector('[name=slt-cliente]').innerHTML = options;
        
    }

}
