export class Agente { 

    async getAgentes() {

        const DATA = await fetch('php/select-agentes.php');
        return await DATA.json();

    }

    async setAgentes() {

        const DATA = await this.getAgentes();
        let options = `<option value="Todos" selected>Todos los agentes</option>`;
        DATA.forEach(agente => {
            options += `<option value="${agente.ID_Agente}">${agente.Nom_Agente}</option>`;
        });
        document.querySelector('[name=slt-agentes]').innerHTML = options;
        
    }

}