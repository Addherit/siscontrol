export class Pedido {

    async getPedidos() {

        const FORM_DATA = new FormData();
        FORM_DATA.append('id_user', localStorage.getItem('user_id'));
        FORM_DATA.append('id_agente', document.querySelector('[name=slt-agentes').value);
        FORM_DATA.append('status', document.querySelector('[name=slt-estatus').value);
        FORM_DATA.append('cliente', document.querySelector('[name=slt-cliente').value);
        FORM_DATA.append('fecha-ini', document.querySelector('[name=date-init').value);
        FORM_DATA.append('fecha-end', document.querySelector('[name=date-end').value);
        const DATA = await fetch('php/select-cantidad-pedidos.php', { method: 'POST', body: FORM_DATA });
        return await DATA.json();
        
    }
    
}