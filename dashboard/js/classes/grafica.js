
export class Grafica {

    getChartLineal(data) {

        let agentes = [];
        let ventas = [];
        data.forEach(agente => {
            agentes.push(agente.Nom_Agente);
            ventas.push(agente.cuentaTotal);
        });        

        const LABELS = agentes;
        const DATA = {
            labels: LABELS,
            datasets: [{
                label: 'Pedidos',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: ventas,
            }]
        };
        const CONFIG = {
            type: 'bar',
            data: DATA,
            options: {}
        };

        const ELEMENT = document.getElementById('myChart').getContext("2d");
        new Chart(ELEMENT, CONFIG);
        

    }

    destroyChart(chart) {

        chart.firstElementChild.firstElementChild.remove();
        const NEW_CANVAS = '<canvas id="myChart"></canvas>'
        chart.firstElementChild.innerHTML = NEW_CANVAS;
        
    }


}