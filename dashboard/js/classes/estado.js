export class Estado {

    async getAllEstatus() {

        const DATA = await fetch('php/select-estados.php');
        return await DATA.json();

    }

    async setEstados() {

        const DATA = await this.getAllEstatus();
        let options = `<option value="Todos" selected>Todos los Estatus</option>`;
        DATA.forEach(estatus => {
            options += `<option value="${estatus.Estatus}">${estatus.Estatus}</option>`;
        });
        document.querySelector('[name=slt-estatus]').innerHTML = options;
        
    }

}