<!DOCTYPE html>
<html lang="en">
    <?php include "../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "../assets/modales/modal-delete-confirmation.html" ?>
            <?php include "../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                            <i class="fas fa-bars fa-lg"></i>
                          </a>
                            <span class="h1 align-middle" id="title-page">DASHBOARD</span>
                        </div>                        
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-9">
                            <div class="card shadow">
                                <div class="card-header text-center bg-info text-white">
                                    <h5 class="card-title">Grafica pedidos por agentes</h5>
                                </div>
                                <div class="card-body" id="chartBar">                                    
                                    <div>
                                        <canvas id="myChart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-3">
                            <div class="card mb-3 shadow">
                                <div class="card-header text-center bg-info text-white">
                                    <h5 class="card-title">Filtros</h5>
                                </div>
                                <div class="card-body">
                                    
                                    <form id="form-pedidos">
                                        <div class="mb-3">
                                            <label for="slt-agentes" class="form-label">Agentes</label>
                                            <select class="form-select form-control form-control-sm" name="slt-agentes"></select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="slt-agentes" class="form-label">Estatus</label>
                                            <select class="form-select form-control form-control-sm" name="slt-estatus"></select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="slt-cliente" class="form-label">Cliente</label>
                                            <select class="form-select form-control form-control-sm" name="slt-cliente"></select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Fecha Inicio</label>
                                            <input type="date" class="form-control form-control-sm" name="date-init">
                                        </div>
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Fecha Final</label>
                                            <input type="date" class="form-control form-control-sm" name="date-end">
                                        </div>
                                       
                                        <button type="button" class="btn btn-info btn-block shadow" id="btn-cunsultar-pedidos">
                                            <span class="material-icons align-middle">search</span>
                                            Consultar
                                        </button>
                                    </form>
                                </div>
                            </div>

                            <div class="card mb-3 shadow">
                                <div class="card-body">
                                    <div class="row text-center">
                                        <div class="col-12">
                                            <h5>PEDIDOS</h5>
                                            <span id="total-pedidos"></span>
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                            <div class="card mb-3 shadow">
                                <div class="card-body">
                                    <div class="row text-center">
                                        <div class="col-12">
                                            <h5>VENTA TOTAL</h5>
                                            <span id="venta-total"></span>
                                        </div>
                                    </div>     
                                </div>
                            </div>
                            <!-- <div class="card mb-3 shadow">
                                <div class="card-body">
                                    
                                    mas cosas
                                </div>
                            </div> -->
                        </div>                                               
                    </div>
                    
                    <div class="row">
                        <div class="col-12 text-center">
                            <div>
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    </div>                                        
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>