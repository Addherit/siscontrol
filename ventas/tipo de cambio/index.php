<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">        
        <?php include "../../assets/modales/modal-confirmation.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                                <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">TIPO DE CAMBIO</span>
                        </div>                        
                    </div>
                    <hr>
                    <br>
                    <div class="row">
                        <div class="col-8 offset-2">
                            <h5>Escribe el Tipo de cambio del día</h5>
                            <br>
                            <div class="row mb-3">
                                <div class="col-4">
                                    <label for="exampleFormControlInput1" class="form-label">Tipo de cambio anterior</label>                            
                                    <input class="form-control form-control-sm" id="tco" readonly>                           
                                </div>
                                <div class="col-4">
                                    <label for="exampleFormControlInput1" class="form-label">Nuevo tipo de cambio</label>                            
                                    <input class="form-control form-control-sm" id="tcn">                           
                                </div>
                            
                                <div class="col-4">
                                    <label for="exampleFormControlInput1" class="form-label">Diferencial</label>                            
                                    <input class="form-control form-control-sm" id="diferencial" readonly>                           
                                </div>
                            </div> 
                            
                            <div class="row text-center mb-2">
                                <div class="col-8 offset-2">
                                    <button type="button" class="btn btn-info btn-block" id="get-diferencial" data-toggle="modal" data-target="#modal-confirmation">
                                        <span class="material-icons align-middle">track_changes</span>
                                        Obtener Diferencial
                                    </button>
                                </div>
                            </div> 
                            <hr>  
                            <div class="row mb-2">
                                
                                <div class="col-6">
                                    <button type="button" class="btn btn-success btn-block" id="apply-diferencial" title="Aplicar Diferencial" data-toggle="modal" data-target="#modal-confirmation">
                                        <span class="material-icons align-middle">done</span>
                                        Aplicar Diferencial
                                    </button>
                                    <label for="">Al aplicar la diferencia, se modificarán los precios de todos los productos dentro de almacén</label>
                                </div>
                                <div class="col-6">
                                  <div class="table-container">
                                  <table class="table table-sm table-stripped" id="tbl-categorias">
                                        <thead> 
                                            <tr>
                                                <th>Check</th>
                                                <th>Categoría</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>   
                                  </div>   
                                </div>
                            </div>     

                        </div>
                    </div>                      
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>