import {Formatter} from "../../../../assets/js/plugins/currency-format.js";
import { Categorias } from "./categorias.js";

export class TipoCambio {

    getDiferencial() {

        const TCO = parseFloat(document.querySelector("#tco").value.split(' ')[1]);

        if (document.querySelector("#tcn").value) {

            const TCN = parseFloat(document.querySelector("#tcn").value); 
            const DIFERENCIAL = Math.abs(100 - (100 * (this.decimalAdjust(TCO / TCN)))) ;
            document.querySelector("#diferencial").value = `${DIFERENCIAL} %`;

            const FORMDATA = new FormData();
            FORMDATA.append('tipo-cambio', TCN)
            fetch('php/insert-ntc.php', { method: 'POST', body: FORMDATA })
            .then(data => data.json())
            .then(data => {
                const MSN = new Message(data.type, data.message);
                MSN.showMessage();
            })

        } else {
            const MSN = new Message('warning', 'Escribe un nuevo tipo de cambio para obtener el diferencial');
            MSN.showMessage();
        }
        
    }

    getOldTipoCambio() {

        fetch('php/select-tc.php')
        .then(data => data.json())
        .then(data => {
        
            const TCO = parseFloat(data[0].Valor);
            const FORMATTER = new Formatter();
            document.querySelector("#tco").value = `$ ${FORMATTER.currencyFormat(TCO)}`;

        })

    }


    decimalAdjust(value) {

        let exp = -2;
        value = value.toString().split('e');
        value = Math['round'](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));

    }

    validateDiferencial() {


        const TCO = parseFloat(document.querySelector("#tco").value.split(' ')[1]);
        const TCN = parseFloat(document.querySelector("#tcn").value);
        const DIFERENCIAL_PERCENT = document.querySelector("#diferencial").value.split(' ')[0];



        if (TCO < TCN) {
            // Si el nuevo tipo de cambio es mayor al tipo de cambio viejo, se valida que el nuevo tipo de cambio sea mayor a $ 20.00 mxn
            if (TCN >= 20) {
                this.applyDiferencial(DIFERENCIAL_PERCENT);
            } else {
                const MSN = new Message('warning', 'El tipo de cambio no es mayor a $ 20.00 mxn, no es aplicable');
                MSN.showMessage();
            }
        } else {
            if (DIFERENCIAL_PERCENT < 10) {
                const MSN = new Message('warning', 'El diferencial es menor al 10%, no se aplican cambios');
                MSN.showMessage()
            } else {
                this.applyDiferencial(DIFERENCIAL_PERCENT);
            }
        }
        $('#modal-confirmation').modal('toggle');

    }

    applyDiferencial() {

        const DIFERENCIAL = document.querySelector('#diferencial').value.split(' ')[0];
        const ROWS = document.querySelectorAll('#tbl-categorias tbody tr')
        let array_categorias = [];
        ROWS.forEach(tr => {
            if (tr.children[0].children[0].checked == true) {
                array_categorias.push({categoria: tr.children[1].textContent, diferencial: document.querySelector('#diferencial').value.split(' ')[0]})
            }
        });

        if (!document.querySelector('#tcn').value) {

            const MSN = new Message('warning', 'No hay diferencial para aplicar');
            MSN.showMessage();
            
        } else {
            if (array_categorias.length) {

                if (this.validateIfApplyDiferencial()) {

                    const FORMDATA = new FormData();
                    FORMDATA.append('diferencial', DIFERENCIAL)
                    FORMDATA.append('categorias', JSON.stringify(array_categorias))
                    FORMDATA.append('accion', this.getAccionToApplyDiferencial())
                    fetch('php/update-precios-bydiferencial.php', { method: 'POST', body: FORMDATA })
                    .then(data => data.json())
                    .then(data => {
                        const CATEGORIAS = new Categorias()
                        CATEGORIAS.setCategorias();
                        const MSN = new Message('success', 'El diferencial se aplicó correctamente');
                        MSN.showMessage();
                    })

                } else {

                    if (this.getAccionToApplyDiferencial() == 'decremento') {
                        const MSN = new Message('warning', 'No se aplica el diferencial. El diferencial no es mayor al 10%');
                        MSN.showMessage();
                    } else {
                        const MSN = new Message('warning', 'No se aplica el diferencial. El el nuevo tipo de cambio no es mayo a los $20.00 mxn');
                        MSN.showMessage();
                    }
                    
                }
                

            } else {
                const MSN = new Message('warning', 'No hay categorias seleccionadas');
                MSN.showMessage();
            }
        }
        

    }
    
    getAccionToApplyDiferencial() {

        const TIPO_CAMBIO_OLD = parseFloat(document.querySelector('#tco').value.split(' ')[1]);
        const TIPO_CAMBIO_NEW = parseFloat(document.querySelector('#tcn').value);
        let response = 'decremento';

        TIPO_CAMBIO_NEW > TIPO_CAMBIO_OLD ? response = 'incremento': response;
        return response;
        
    }

    validateIfApplyDiferencial() {  

        const TYPE_APPLY = this.getAccionToApplyDiferencial();
        const TIPO_CAMBIO_NEW = parseFloat(document.querySelector('#tcn').value);
        const DIFERENCIAL = parseFloat(document.querySelector('#diferencial').value.split(' ')[0]);
        let response = false;

        if (TYPE_APPLY == 'incremento') {
            TIPO_CAMBIO_NEW >= 20 ? response = true : response;
        } else {
            DIFERENCIAL >= 10 ? response = true : response;
        }
        return response;

    }


}