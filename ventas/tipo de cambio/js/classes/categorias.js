export class Categorias { 
 
    
    getCategorias() {

        return fetch('php/select-categorias.php')
        .then(data => data.json())
        .then(data => {
            return data
        })

    }

    getCategoriasRows(data) { 

        let linea = ``;
        data.forEach(element => {
            linea += `
                <tr>
                    <td class="text-center"><input class="" type="checkbox" id="${element.Categoria}"></td>
                    <td>${element.Categoria}</td>
                </tr>
            `
        });
        return linea;

    }

    async setCategorias() {

        const ROWS = this.getCategoriasRows(await this.getCategorias());
        document.querySelector('#tbl-categorias tbody').innerHTML = ROWS;

    }

}