`use strict`;

import { TipoCambio } from "./classes/tipoCambio.js";
import { Categorias } from "./classes/categorias.js";

document.addEventListener('DOMContentLoaded', () => {
    
    const TC = new TipoCambio()
    TC.getOldTipoCambio()

    const CATEGORIAS = new Categorias();
    CATEGORIAS.setCategorias();

    localStorage.setItem('tcn-correcto', document.querySelector('#tcn').value)
    
})

document.querySelector("#get-diferencial").addEventListener('click', () => {

    document.querySelector('#completing-msg').innerHTML = `de obtener el diferencial`;    
    localStorage.setItem('accion-confirm', 'getDiferencial')      

})

document.querySelector('#apply-diferencial').addEventListener('click', () => {

    document.querySelector('#completing-msg').innerHTML = `De aplicar el diferencial`;    
    localStorage.setItem('accion-confirm', 'applyDiferencial')  

})

document.querySelector('#btn-confirm').addEventListener('click', () => {

    accionesConfirm(localStorage.getItem('accion-confirm'))
    $('#modal-confirmation').modal('hide');

})

document.querySelector('#tcn').addEventListener('keyup', () => validateOnlyNumberAndPoint())

function accionesConfirm(action) {

    const TC = new TipoCambio();
    switch (action) {
        case 'getDiferencial':
            TC.getDiferencial()
        break;
        case 'applyDiferencial':
            TC.applyDiferencial()
        break;
    }

}

function validateOnlyNumberAndPoint() {

    //funcion para validar que solo se pongan numero y un punto
    const TCN = document.querySelector("#tcn");
    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
    
    if(preg.test(TCN.value) === true){
        localStorage.setItem('tcn-correcto', TCN.value);
    } else{
        TCN.value = localStorage.getItem('tcn-correcto');
        const MSN = new Message('warning', 'Solo puedes escribe numeros y punto decimal');
        MSN.showMessage();
    }
    
}


