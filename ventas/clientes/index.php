<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
            <?php include "../../sidebar/index.html"?> 
            <?php include "modales/modal-add-cliente.html"?> 
            <?php include "modales/modal-edit-cliente.html"?> 
            <?php include "modales/modal-estado-cuenta.html"?> 
            <?php include "../../assets/modales/modal-disabled-confirmation.html" ?>
            <?php include "../../assets/modales/modal-activate-confirmation.html" ?>
            <?php include "../../assets/modales/modal-confirmation.html" ?>
            <?php include "../../assets/modales/modal-spinner.html" ?>
       
        
            
            
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                                <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">CLIENTES</span>
                        </div>                        
                    </div>
                    <hr>
                    
                        <div class="row mb-2 text-right">
                            <div class="col-12">
                                <button type="button" class="btn btn-info" id="btn-New" data-toggle="modal" data-target="#modal-add-client" ><span class="material-icons align-middle">add</span> Nuevo Cliente</button>
                            </div> 
                        </div>


                        <div class="row">
                            <div class="col-12 table-responsive">
                            <table id="list-clients" class="table table-hover table-dataTable table-sm" width="100%">
                                <thead>
                                <tr>
                                    <th class="th-sm"></th>
                                    <th class="th-sm">Id</th>
                                    <th class="th-sm">Nombre Completo</th>
                                    <th class="th-sm">Correo electrónico</th>
                                    <th class="th-sm">Dirección</th>
                                    <th class="th-sm">Teléfono</th>
                                    <th class="th-sm">Estatus</th>
                                </tr>
                                </thead>                            
                            <tbody></tbody>
                            </table>
                            </div>
                        </div>



                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>

      
     
</html>