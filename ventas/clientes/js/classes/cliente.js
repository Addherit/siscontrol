import {DataTable} from '/siscontrol/assets/js/classes/dataTable.js';
import { Formatter } from "/siscontrol/assets/js/plugins/currency-format.js";
import { Spinner } from "/siscontrol/assets/js/classes/spinner.js";

const SPINNER = new Spinner();
const FORMATTER = new Formatter();

export class Cliente {
    
    constructor() {
        this.idCliente = ``;
    }

    putClient() {

        const FORM_DATA = new FormData(document.querySelector("#form-new-client"))
        FORM_DATA.append('status', 'Activo')
        fetch('php/insert-new-client.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-add-client').modal('hide');
            this.setClients();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })

    }

    updateClient() {

        const FORM_DATA = new FormData(document.querySelector("#form-edit-client"))     
        FORM_DATA.append('edit-id', this.idCliente)   
        fetch('php/update-cliente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-edit-client').modal('hide');
            this.setClients();
            const MSN = new Message('success', 'Se actualizó la información del cliente correctamente');
            MSN.showMessage();
        })

    }

    disabledClient() {
        
        const ID_CLIENT = this.idCliente        
        const FORM_DATA = new FormData()
        FORM_DATA.append('id_cliente', ID_CLIENT);
        fetch('php/desactivar-cliente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-disabled-confirmation').modal('hide');
            this.setClients();
            const MSN = new Message('success', 'Se desactivó el cliente correctamente');
            MSN.showMessage();
        })

    }

    activateClient() {

        const ID_CLIENT = this.idCliente        
        const FORM_DATA = new FormData()
        FORM_DATA.append('id_cliente', ID_CLIENT);
        fetch('php/activar-cliente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {            
            $('#modal-activate-confirmation').modal('hide');
            this.setClients();
            const MSN = new Message('success', 'Se activó el cliente correctamente');
            MSN.showMessage();
        })

    }

    saveAgente() {

        const FORM_DATA = new FormData()
        const AGENTE = document.querySelector('#slt-agentes');            
        FORM_DATA.append('id',this.idCliente );
        FORM_DATA.append('agente',AGENTE.value );
        fetch('php/update-agente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {
            $('#modal-edit-client').modal('hide');
            this.setClients();
            const MSN = new Message(data.type, data.message);
            MSN.showMessage();
        })
    }

    getClients() {

        return fetch('php/select-clients.php')
        .then(data => data.json())
        .then(data => {
            setTimeout(() => { SPINNER.hideSpinner() }, 1000);
            return data 
        })
        
    }

    configByUser() {

        const ID_type = localStorage.getItem('id_user_type');
        //la impresion del as facturas por dashboard
        let CONFIG_JSON = {};
        console.log(ID_type);
        switch (ID_type) {
            case "1":
                CONFIG_JSON = {
                    Conf: {btnNew: false, btnEdit: false, btnDelete: false, btnUpdate:false}

                }
                
                break;
            case "2":
                CONFIG_JSON = {
                    Conf: {btnNew: true, btnEdit: true, btnDelete: true, btnUpdate:true}
                    
                }
                
                break;

            case "3":
                CONFIG_JSON = {
                    Conf: {btnNew: true, btnEdit: true, btnDelete: true, btnUpdate:true}
                    
                }
                
                break;

            case "4":
                
                break;

            case "5":
                CONFIG_JSON = {
                    Conf: {btnNew: true, btnEdit: true, btnDelete: true, btnUpdate:true}
                    
                }
                
                break;
        
            default:
                break;
        }
        console.log(CONFIG_JSON)
        
        document.querySelector("#btn-New").disabled = CONFIG_JSON['Conf'].btnNew;
        document.querySelector("#btn-edit-client").disabled = CONFIG_JSON['Conf'].btnUpdate;

    }

    async setClients() {

        SPINNER.showSpinner();
        const DATA = await this.getClients();
        const ID_type = localStorage.getItem('id_user_type');
        let lines = ``;
        let BTN_ACTIVATE_DISABLED = "";
        let BTN_ESTADO_CUENTA = '';

        DATA.forEach(client => {
            const STATUS = this.getColorStatus(client.State);
            if(ID_type =="1"){
                BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled(client.State, client.ID_Cliente);
                BTN_ESTADO_CUENTA = '<button type="button" class="btn btn-sm btn-info btn-tbl-modal" title="Estado de cuenta" data-toggle="modal" data-target="#modal-estado-cuenta"><span class="material-icons align-middle">toc</span>Cuenta</button>';
            }else{
                BTN_ACTIVATE_DISABLED = this.getBtnActivateDisabled("Desactivado", client.ID_Cliente);
                if(ID_type =="5"){
                    BTN_ESTADO_CUENTA = '<button type="button" class="btn btn-sm btn-info btn-tbl-modal" title="Estado de cuenta" data-toggle="modal" data-target="#modal-estado-cuenta"><span class="material-icons align-middle">toc</span>Cuenta</button>';
                }
            }
            lines += `
                <tr>
                    <td style="white-space: nowrap">
                        ${BTN_ACTIVATE_DISABLED}
                        <button type="button" class="btn btn-sm btn-warning btn-edit-client" title="Editar Nota de venta" data-toggle="modal" data-target="#modal-edit-client"><span class="material-icons align-middle">edit</span>Editar</button>
                        ${BTN_ESTADO_CUENTA}
                    </td>
                    <td>${client.ID_Cliente}</td>
                    <td style="white-space: nowrap">${client.Nombre_Cte}</td>
                    <td>${client.Email1}</td>
                    <td style="white-space: nowrap">${client.Calle} Int ${client.N_Ext}, Col. ${client.Colonia}</td>
                    <td>${client.Telefono}</td>
                    ${STATUS}
                </tr>
            `
        });

        document.querySelector('#list-clients tbody').innerHTML = lines;
        const dataTable = new DataTable('list-clients');
    
        // Itera todos los botones de eliminado para asignar el evento click.
        this.addEventClickToBtnActivateDisabled();
        this.addEventClickToBtnEditClient();
        dataTable.initDataTable();
        //configuracion
        this.configByUser();
        this.handleConfirmEstado();
    }

    truncateDecimals = function (number, digits) {
        var multiplier = Math.pow(10, digits),
            adjustedNum = number * multiplier,
            truncatedNum = Math[adjustedNum < 0 ? 'ceil' : 'floor'](adjustedNum);

        return truncatedNum / multiplier;

    };

    handleConfirmEstado() {

        const BTNS = document.querySelectorAll('.btn-tbl-modal');
        BTNS.forEach(btn => {
            btn.addEventListener('click', (event) => {
                const TR = event.currentTarget.parentElement.parentElement;
                document.querySelector('#id-estado-cliente').value = TR.children[1].textContent;                
                document.querySelector('#list-cuenta tbody').innerHTML = " ";
            })
        })

    }

    addEstadoDownload() {

        console.log("entro");
        let fi = document.querySelector('#fechaI').value ;
        let ff = document.querySelector('#fechaF').value;
        let id = document.querySelector('#id-estado-cliente').value;

        const FORMDATA = new FormData()
        FORMDATA.append('id_cliente', id)
        FORMDATA.append('fechaI', fi)
        FORMDATA.append('fechaF', ff)
        fetch('php/select-estado.php', { method: 'POST', body: FORMDATA })
        .then(data => data.json())
        .then(data => {

            //let d = document.querySelector('#desccuenta');
            let d = document.querySelector('#list-cuenta tbody');
            if(Object.entries(data).length === 0){
                d.innerHTML += `<tr><td> Sin Datos <td></tr>`;
            }else{
                let desc = "";
                let total = 0.0;
                //falta crear el string

                data.forEach(item =>{
                    desc += `
                        <tr>
                            <td style="white-space: nowrap">${item.Cve_Cte}</td>
                            <td>${item.Fecha_Vta}</td>
                            <td style="white-space: nowrap"> ${FORMATTER.currencyFormat(item.Total)}</td>
                        </tr>
                    `
                    //desc += `<br> ${item.Cve_Cte} ${item.Fecha_Vta}, ${FORMATTER.currencyFormat(item.Total)}`;
                    total += parseFloat(item.Total);
                });
                let det = this.truncateDecimals(total,3) ;
                desc += `<tr><td> Total: ${FORMATTER.currencyFormat(det)} <td></tr>`;
                d.innerHTML =  desc;
                //document.querySelector('#list-clients tbody').innerHTML = lines;
            }

        })
        console.log(fi);
        console.log(ff);
        console.log(id);

    }

    addEventClickToBtnActivateDisabled() {

        document.querySelectorAll('.btn-activate-disabled').forEach(button => {        
            button.addEventListener('click', (event) => {
                const NAME_CLIENT = event.currentTarget.parentElement.parentElement.children[2].textContent;
                document.querySelector('#disabled-name').innerHTML = `el cliente`;
                document.querySelector('#id-to-disabled').innerHTML = NAME_CLIENT;
                document.querySelector('#confirm-name').innerHTML = `el cliente`;
                document.querySelector('#id-to-confirm').innerHTML = NAME_CLIENT;
                this.idCliente = event.currentTarget.parentElement.parentElement.children[1].textContent;
            })
        });

    }

    addEventClickToBtnEditClient() {

        document.querySelectorAll('.btn-edit-client').forEach(button => {        
            button.addEventListener('click', (event) => {
                const NAME_CLIENT = event.currentTarget.parentElement.parentElement.children[2].textContent;
                this.idCliente = event.currentTarget.parentElement.parentElement.children[1].textContent;
                this.setInfoClientById()
            })
        });

    }

    setInfoClientById() {

        const ID_type = localStorage.getItem('id_user_type');
        if(ID_type != 1){
            document.querySelector('#btn-save-agente').disabled = true;            
        }
        const FORMDATA = new FormData()
        FORMDATA.append('id_cliente', this.idCliente)
        fetch('php/select-cliente.php', { method: 'POST', body: FORMDATA })
        .then(data => data.json())
        .then(data => {

            document.querySelector("[name=edit-nombre-completo]").value = data[0].Nombre_Cte
            document.querySelector("[name=edit-email-cliente]").value = data[0].Email1
            document.querySelector("[name=edit-telefono]").value = data[0].Telefono
            document.querySelector("[name=edit-limite-credito]").value = data[0].M_Credito
            document.querySelector("[name=edit-dias-credito]").value = data[0].D_Credito
            document.querySelector("[name=edit-calle]").value = data[0].Calle
            document.querySelector("[name=edit-colonia]").value = data[0].Colonia
            document.querySelector("[name=edit-numero]").value = data[0].N_Ext
            document.querySelector("[name=edit-codigo-postal]").value = data[0].CP
            document.querySelector("[name=edit-contactos]").value = data[0].Contactos
            document.querySelector("[name=edit-razon-social]").value = data[0].Razon_Soc
            document.querySelector("[name=edit-rfc]").value = data[0].RFC
            document.querySelector("[name=edit-email-fiscal]").value = data[0].Email2
            document.querySelector("[name=edit-tipo-pago]").value = data[0].TipoPago
            document.querySelector("[name=edit-cfdi]").value = data[0].Cfdi
            document.querySelector("[name=edit-email-fiscal-alternativo]").value = data[0].Email3
            document.querySelector("#status-user").innerHTML = data[0].State
            document.querySelector("[name=edit-agente]").value = data[0].Nom_Agente
            if (data[0].State != 'Moroso') document.querySelector('#btn-excepcion').disabled = true;            

        })
        this.getAgentes();
    }

    getColorStatus(status) {

        const COLOR_STATUS = {
            Activo: `<td class="text-success">${status}</td>`,
            Inactivo: `<td>${status}</td>`,
            Moroso: `<td class="text-danger">${status}</td>`,
            Excepcion: `<td class="text-info">${status}</td>`
        }

        return COLOR_STATUS[status];

    }



    getBtnActivateDisabled(status, id) {


        if (status == 'Inactivo') {
            return `
            <button type="button" id="${id}" class="btn btn-sm btn-success btn-activate-disabled" title="Activar Cliente" data-toggle="modal" data-target="#modal-activate-confirmation" style="width: 121px">
                <span class="material-icons align-middle">check_circle</span>
                Activar
            </button>`
        } else if(status == 'Activo') {
            return `
            <button type="button" id="${id}" class="btn btn-sm btn-danger btn-activate-disabled" title="Desactivar cliente" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Desactivar
            </button>`
        }else{
            return `
            <button type="button" id="${id}"disabled class="btn btn-sm btn-danger btn-activate-disabled" title="Desactivar cliente" data-toggle="modal" data-target="#modal-disabled-confirmation">
                <span class="material-icons align-middle">cancel</span>
                Desactivar
            </button>`

        }
       
    }

    getAgentes() {
        
        fetch('php/select-agentes.php')
        .then(data => data.json())
        .then(data => {

            let options = `<option value="" selected disabled>Selecciona un agente</option>`;
            data.forEach( item => {
                options += `<option value="${item.Nom_Agente}">${item.Nom_Agente}</option>`;
            });
            document.querySelector("[name=slt-agente]").innerHTML = options;
        
        })

    }

    getCFDIS() {
        
        fetch('php/select-cfdi.php')
        .then(data => data.json())
        .then(data => {

            let options = `<option value="" selected disabled>Selecciona un CFDI</option>`;
            data.forEach( cfdi => {
                options += `<option value="${cfdi.ID_UsoCfdi}">${cfdi.UsoCfdi}</option>`;
            });
            document.querySelector("[name=cfdi]").innerHTML = options;
            document.querySelector("[name=edit-cfdi]").innerHTML = options;
        
        })
    }

    getFormasPago() {

        fetch('php/select-fpago.php')
        .then(data => data.json())
        .then(data => {

            let options = `<option value="" selected disabled>Selecciona tipo pago</option>`;
            data.forEach( fpago => {
                options += `<option value="${fpago.Fpago}">${fpago.Fpago}</option>`;
            });
            document.querySelector("[name=tipo-pago]").innerHTML = options;
            document.querySelector("[name=edit-tipo-pago]").innerHTML = options;
        
        })
    }

    getOptionsDiasCredito() {

        fetch('php/select-diascredito.php')
        .then(data => data.json())
        .then(data => {

            let options = `<option value="" selected disabled>Selecciona tiempo de crédito</option>`;
            data.forEach( fpago => {
                options += `<option value="${fpago.D_Credito}">${fpago.D_Credito}</option>`;
            });
            document.querySelector("[name=dias-credito]").innerHTML = options;
            document.querySelector("[name=edit-dias-credito]").innerHTML = options;
        
        })
    }

    addException() {

        const FORM_DATA = new FormData(document.querySelector("#form-edit-client"))     
        FORM_DATA.append('edit-id', this.idCliente)   
        fetch('php/exception-cliente.php', { method: 'POST', body: FORM_DATA })
        .then(data => data.json())
        .then(data => {

            this.setClients();
            const MSN = new Message('success', 'Se aplicó excepción al cliente');
            MSN.showMessage();
            $('#modal-edit-client').modal('hide');

        })

    }

	exportTableToExcel(tableID) {
        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        var table = tableID;
        var name = 'Estado Cuenta';

        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
            // link.click();
        
	}
}