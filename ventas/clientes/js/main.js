import { Cliente } from "./classes/cliente.js";
import { Usuario } from "./classes/usuario.js";
const CLIENTE = new Cliente();
const USUARIO = new Usuario();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#btn-disabled').addEventListener('click', () => CLIENTE.disabledClient());
document.querySelector('#btn-activate').addEventListener('click', () => CLIENTE.activateClient());
document.querySelector('#btn-new-client').addEventListener('click', () => newClient());
document.querySelector('#btn-edit-client').addEventListener('click', () => editClient());
document.querySelector('#btn-confirm').addEventListener('click', () => confirmAction());
document.querySelector('#btn-excepcion').addEventListener('click', () => CLIENTE.addException());
document.querySelector('#btn-estado-download').addEventListener('click', () => CLIENTE.addEstadoDownload());
document.querySelector('#btn-estado-descarga').addEventListener('click', () => CLIENTE.exportTableToExcel("list-cuenta"));
document.querySelector('#btn-save-agente').addEventListener('click', () => CLIENTE.saveAgente());


function init() {
    
    CLIENTE.setClients(); 
    CLIENTE.getCFDIS();
    CLIENTE.getFormasPago();
    CLIENTE.getOptionsDiasCredito();
    USUARIO.isAdmin();

}

function newClient() {

    document.querySelector('#completing-msg').innerHTML = `de agregar el nuevo cliente`;
    localStorage.setItem('func-todo', 'putClient');
    
}

function editClient() {
    
    document.querySelector('#completing-msg').innerHTML = `editar la información del cliente`;
    localStorage.setItem('func-todo', 'updateClient');

}

function confirmAction() {
    
    actionsConfirm(localStorage.getItem('func-todo'));
    $('#modal-confirmation').modal('hide');

}

function actionsConfirm(action) {
    
    switch (action) {
        case 'putClient':
            CLIENTE.putClient();
            break;
        case 'updateClient':
            CLIENTE.updateClient();
            break;
    }

}