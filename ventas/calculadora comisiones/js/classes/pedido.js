import {Formatter} from "../../../../assets/js/plugins/currency-format.js";
import { Comision } from "./comision.js";

export class Pedido {

    async getPedidos() {

        const ID_USER = document.querySelector('[name=list-agentes]').value;
        const FORMDATA = new FormData()
        FORMDATA.append('id_user', ID_USER)
        FORMDATA.append('fecha_ini', document.querySelector('[name=fecha-inicial]').value)
        FORMDATA.append('fecha_fin', document.querySelector('[name=fecha-final]').value)
        const DATA = await fetch('php/select-estados.php', { method: 'POST', body: FORMDATA });
        return await DATA.json();

    }

    async setPedidos() {

        let lineas = ``;
        const DATA = await this.getPedidos();
        DATA.forEach(pedido => {
            lineas += `
                <tr>
                    <td><button type="button" class="btn btn-warning btn-sm" title="ver detalle de los pedidos con estatus ${pedido.Estatus}">Detalle</button></td>
                    <td>${pedido.Estatus}</td>
                    <td>${pedido.Count}</td>
                </tr>
            `;
            if (pedido.Estatus == 'FACTURADO') { document.querySelector('[name=subtotal-facturado]').value = pedido.Subtotal }
        });

        document.querySelector('#tbl-pedidos tbody').innerHTML = lineas;
        this.addEventRedirect();
        this.getComision(DATA);

    }

    redirectPedidosByEstatusAndAgent(estatus) {
        
        const STATUS = estatus;
        const AGENTE = document.querySelector('[name=list-agentes]').value;
        const FECHA_INI = document.querySelector('[name=fecha-inicial]').value;
        const FECHA_FIN = document.querySelector('[name=fecha-final]').value;        
        window.location.href = `/siscontrol/administrativo/pedidos/index.php?agente=${AGENTE}&status=${STATUS}&date_i=${FECHA_INI}&date_f=${FECHA_FIN}`;
        
    }

    addEventRedirect() {

        const TRS = document.querySelectorAll('#tbl-pedidos tbody tr');
        TRS.forEach(tr => {
            tr.children[0].children[0].addEventListener('click', (event) => {
                const STATUS = event.target.parentElement.parentElement.children[1].textContent;
                this.redirectPedidosByEstatusAndAgent(STATUS);
            })
        });

    }

    async getComision(data) {

        const COMISION = new Comision();
        const DATA = await COMISION.getComision();
        data.forEach(pedidos => {
            if (pedidos.Estatus.toLowerCase() === 'facturado') {
                
                const SUB_FACTURADO = parseFloat(pedidos.Subtotal)
                const FORMATTER = new Formatter();
                const INPUT_SUB_FACT = document.querySelector('[name=subtotal-facturado]');
                const INPUT_DIFERENCIA = document.querySelector('[name=comision]');
                const MIN_PARA_FACTURAR = DATA[0].Categoria;                

                if(SUB_FACTURADO < parseFloat(MIN_PARA_FACTURAR)) {

                    INPUT_SUB_FACT.value = '';
                    INPUT_DIFERENCIA.value = '';
                    const MSN = new Message('warning', 'El subtotal facturado, no es suficiente para cobrar comisiones');
                    MSN.showMessage();

                } else {

                    const DIFERENCIA = (SUB_FACTURADO - parseFloat(MIN_PARA_FACTURAR)) * (DATA[0].Comision / 100);
                    INPUT_SUB_FACT.value = `$ ${FORMATTER.currencyFormat(SUB_FACTURADO)}`;
                    INPUT_DIFERENCIA.value = `$ ${FORMATTER.currencyFormat(DIFERENCIA)}`;

                    const MSN = new Message('success', 'Comisión obtenida');
                    MSN.showMessage();

                }
            }
        })

    }

}