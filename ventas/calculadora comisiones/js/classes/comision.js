export class Comision {

    async getComision() {

        const FORMDATA = new FormData(document.querySelector('#comision-agente'));        
        const DATA = await fetch('php/select-cat-comision.php', { method: 'POST', body: FORMDATA });
        return DATA.json();
        

    }

}