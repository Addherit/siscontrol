export class Agente { 

    async getAgentes() {

        const DATA = await fetch('php/select-agentes.php');
        return await DATA.json();

    }

    async setAgentes() {

        let options = `<option value="" selected disabled>Selecciona un agente</option>`;
        const AGENTES = await this.getAgentes();
        AGENTES.forEach(agente => {
            options += `<option value="${agente.ID_Agente}">${agente.Nom_Agente}</option>`;
            
        });
        
        document.querySelector('[name=list-agentes]').innerHTML = options;

    }
}