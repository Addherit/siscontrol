import { Agente } from "./classes/agente.js";

import { Pedido } from "./classes/pedido.js";


const PEDIDOS = new Pedido();

document.addEventListener('DOMContentLoaded', () => init());
document.querySelector('#comision-agente').addEventListener('submit', (e) => {

    e.preventDefault();        
    PEDIDOS.setPedidos();    

});


function init() {
    
    const AGENTES = new Agente();
    AGENTES.setAgentes();

}