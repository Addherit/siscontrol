<!DOCTYPE html>
<html lang="en">
    <?php include "../../includes/header.html" ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">

    <body>
        
        <div class="page-wrapper legacy-theme sidebar-bg bg2 toggled">
        <?php include "../../assets/modales/modal-delete-confirmation.html" ?>
            <?php include "../../sidebar/index.html"?> 
            
            <!-- page-content  -->
            <main class="page-content pt-2">
              
                <div id="overlay" class="overlay"></div>
                <div class="container-fluid p-5">
                    <div class="row">
                        <div class="form-group col-12">
                            <a id="toggle-sidebar" class="btn btn-secondary rounded-0" href="#" title="Ocultar/Mostrar menú">
                                <i class="fas fa-bars fa-lg"></i>
                            </a>
                            <span class="h1 align-middle" id="title-page">CALCULADORA DE COMISIONES</span>
                        </div>                        
                    </div>
                    <hr>
                    <div class="row pt-4 pb-4 text-center">
                        <div class="col-6 text-center">                            
                            <form id="comision-agente">
                                <div class="row mb-2 pb-4">
                                    <label for="list-agentes" class="form-label">Agentes</label>
                                    <select name="list-agentes" class="form-control form-control-sm"></select>
                                </div>
                                <div class="row mb-2">
                                    <div class="form-row" style="width: 100%">
                                        <div class="form-group col-md-6">
                                            <label for="fecha-inicial">Fecha Inicial</label>
                                            <input type="date" class="form-control form-control-sm" name="fecha-inicial">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="fecha-final">Fecha Final</label>
                                            <input type="date" class="form-control form-control-sm" name="fecha-final">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">
                                    <span class="material-icons align-middle">local_atm</span>
                                    Calcular comisión
                                </button>
                            </form>
                            <br>
                            <hr>
                            <br>
                            <h4>Comisión</h4>
                            <form action="">

                                <div class="row mb-2">
                                    <label for="subtotal-facturado" class="form-label">Subtotal FActurado</label>
                                    <input type="text" name="subtotal-facturado" class="form-control form-control-sm" placeholder="$ 0.00" readonly>
                                </div>
                                <div class="row mb-2">
                                    <label for="comision" class="form-label">Comisión</label>
                                    <input type="text" name="comision" class="form-control form-control-sm" placeholder="$ 0.00" readonly>
                                </div>

                            </form>

                       </div>
                       
        
                        <div class="col-6">
                            <h5>Pedidos</h5>
                            <table class="table table-sm" id="tbl-pedidos">
                                <thead>
                                    <tr>
                                        <th></th>                                        
                                        <th>Estatus</th>
                                        <th>Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                                                
                        
                    </div>                                      
                </div>
            </main>
            <!-- page-content" -->
        </div>        
    </body>

    <?php include "../../includes/footer.html" ?>    
    <script src="js/main.js" type="module"></script>   
     
</html>