# SisControl by Addherit

This project was generated with 
    * HTML 5
    * CSS 3
    * ECMAScript 2016 v7
    * JQuery v3.5.1
    * PHP v8
    
    * Bootstrap v4.6.0
    * Bootswatch 5.0.0-beta1
    * DataTables 1.10.23

## Development server
 
 Navigate to `http://localhost/`